using System.Threading.Tasks;
using StackExchange.Redis;
using System.Threading;
using System;
namespace iVE.API
{
    public class RedisCacheService : ICacheService
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        public RedisCacheService(IConnectionMultiplexer connectionMultipexer)
        {
            _connectionMultiplexer = connectionMultipexer;
        }
        public string GetCache(string key)
        {
            var db = _connectionMultiplexer.GetDatabase();
            return db.StringGet(key);
            //return await db.StringGetAsync(key);
        }
        public void SetCache(string key, string value)
        {
            var db = _connectionMultiplexer.GetDatabase();
            db.StringSet(key, value);
            //db.KeyExpire(key, new TimeSpan(0, 0, 60));
        }
    }
}