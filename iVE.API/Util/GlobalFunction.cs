using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.AccessControl;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MimeKit;
using Newtonsoft.Json;
using OfficeOpenXml;
using Serilog;
using Syncfusion.XlsIO;
using Microsoft.Extensions.Caching.Memory;
using iVE.DAL;

namespace iVE.API
{
    public static class GlobalFunction
    {
        // public static void WriteSystemLog(string message)
        // {
        //     Console.WriteLine(DateTime.Now.ToString() + " - " + message);
        //     var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
        //     var Configuration = appsettingbuilder.Build();
        //     string path = Configuration.GetSection("appSettings:LogFilePath").Value;

        //     if (!Directory.Exists(path))
        //     {
        //         Directory.CreateDirectory(path);
        //         path += DateTime.Now.ToString("yyyy-MM-dd") + "_log.txt";
        //         StreamWriter stwriter = System.IO.File.CreateText(path);
        //         stwriter.WriteLine(DateTime.Now);
        //         stwriter.WriteLine("-------------------Error Log Start----------------");
        //         stwriter.WriteLine("Message:" + message);
        //         stwriter.WriteLine("-------------------End----------------------------");
        //         stwriter.Close();
        //     }
        //     else
        //     {
        //         path += DateTime.Now.ToString("yyyy-MM-dd") + "_log.txt";
        //         if (System.IO.File.Exists(path))
        //         {
        //             using (StreamWriter stwriter = new StreamWriter(path, true))
        //             {
        //                 stwriter.WriteLine(DateTime.Now);
        //                 stwriter.WriteLine("-------------------Error Log Start----------------");
        //                 stwriter.WriteLine("Message:" + message);
        //                 stwriter.WriteLine("-------------------End----------------------------");
        //             }
        //         }
        //         else
        //         {
        //             StreamWriter stwriter = System.IO.File.CreateText(path);
        //             stwriter.WriteLine(DateTime.Now);
        //             stwriter.WriteLine("-------------------Error Log Start----------------");
        //             stwriter.WriteLine("Message:" + message);
        //             stwriter.WriteLine("-------------------End----------------------------");
        //             stwriter.Close();
        //         }
        //     }
        // }

        public static void WriteSystemLog(string logType, string apiName, string logMessage, string userId)
        {
            if (logType == LogType.Info)
            {
                Log.Information("In {APIName} , {LogMessage} by {UserId}", apiName, logMessage, userId);
            }

            if (logType == LogType.Error)
            {
                Log.Error("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Fatal)
            {
                Log.Fatal("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Debug)
            {
                Log.Debug("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Warn)
            {
                Log.Warning("In {APIName} , {LogMessage}", apiName, logMessage);
            }
        }

        public static string UploadImage(IFormFile file, string showId, string uploadPath)
        {
            string fileName = "";

            if (!Directory.Exists(uploadPath))
            {
                Directory.CreateDirectory(uploadPath);
                uploadPath += "\\";
                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                string fileextension = Path.GetExtension(file.FileName);
                filename = showId + fileextension;
                using (FileStream fileStream = System.IO.File.Create(uploadPath + filename))
                {
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                    fileName = filename;
                }
            }
            else
            {
                uploadPath += "\\";
                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                string fileextension = Path.GetExtension(file.FileName);
                filename = showId + fileextension;
                using (FileStream fileStream = System.IO.File.Create(uploadPath + filename))
                {
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                    fileName = filename;
                }
            }
            return fileName;
        }

        public static Claim[] GetClaims(TokenData obj, string role)
        {
            var claims = new Claim[] {
                new Claim (ClaimTypes.Name, obj.UserID),
                new Claim (ClaimTypes.Role, role),
                new Claim ("showId", obj.showId),
                new Claim ("UserID", obj.UserID),
                new Claim ("LoginType", obj.LoginType),
                new Claim ("Userlevelid", obj.Userlevelid),
                new Claim ("TicketExpireDate", obj.TicketExpireDate.ToString ()),
                new Claim (JwtRegisteredClaimNames.Sub, obj.Sub),
                new Claim (JwtRegisteredClaimNames.Jti, obj.Jti),
                new Claim (JwtRegisteredClaimNames.Iat, obj.Iat, ClaimValueTypes.Integer64)
            };
            return claims;
        }
        public static TokenData GetTokenData(JwtSecurityToken tokenS)
        {
            var obj = new TokenData();
            try
            {
                obj.showId = tokenS.Claims.First(claim => claim.Type == "showId").Value;
                obj.UserID = tokenS.Claims.First(claim => claim.Type == "UserID").Value;
                obj.Role = tokenS.Claims.First(claim => claim.Type == "role").Value;
                obj.LoginType = tokenS.Claims.First(claim => claim.Type == "LoginType").Value;
                obj.Userlevelid = tokenS.Claims.First(claim => claim.Type == "Userlevelid").Value;
                obj.Sub = tokenS.Claims.First(claim => claim.Type == "sub").Value;
                string TicketExpire = tokenS.Claims.First(claim => claim.Type == "TicketExpireDate").Value;
                DateTime TicketExpireDate = DateTime.Parse(TicketExpire);
                obj.TicketExpireDate = TicketExpireDate;
            }
            catch (Exception ex)
            {
                WriteSystemLog(LogType.Error, "GlobalFunctions - GetTokenData", ex.Message.ToString(), "");
            }
            return obj;
        }
        public static string GetToken(TokenData obj, string role)
        {
            string tokenString = "";
            try
            {
                var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                var Configuration = appsettingbuilder.Build();

                string secretKey = Configuration.GetSection("TokenAuthentication:SecretKey").Value;
                int expiretimespan = Convert.ToInt32(Configuration.GetSection("TokenAuthentication:TokenExpire").Value);

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(secretKey);
                if (role == "")
                {
                    role = obj.Role;
                }
                var claims = GetClaims(obj, role);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    //Expires = DateTime.UtcNow.AddHours(expiretimespan),
                    Expires = DateTime.UtcNow.AddMinutes(expiretimespan),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                tokenString = tokenHandler.WriteToken(token);
            }
            catch (Exception exe)
            {
                WriteSystemLog(LogType.Error, "GlobalFunctions - GetToken", exe.Message.ToString(), "");
            }
            return tokenString;
        }
        public static string SaveFile(IFormFile file, string exhId, string returnUrl)
        {
            string url;
            string filename = Path.GetFileNameWithoutExtension(file.FileName);
            string fileextension = Path.GetExtension(file.FileName);
            filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;

            string filePath = System.IO.Directory.GetCurrentDirectory() + "\\wwwroot\\Files\\";
            filePath += exhId + "\\";
            // string returnUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
                using (FileStream fileStream = System.IO.File.Create(filePath + filename))
                {
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
            else
            {
                using (FileStream fileStream = System.IO.File.Create(filePath + filename))
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
            url = returnUrl + "/File/" + exhId + "/" + filename;
            return url;
        }
        public static void Deletefile(string url, string confId)
        {
            string materialPath = System.IO.Directory.GetCurrentDirectory() + "\\wwwroot\\Files\\";
            materialPath += confId + "\\";
            string fileName = url.Split('/').Last();
            materialPath += fileName;
            if (System.IO.File.Exists(materialPath))
            {
                System.IO.File.Delete(materialPath);
            }
        }

        public static async Task<bool> SendNotification(IRepositoryWrapper _repositoryWrapper, List<string> userList, tblNotificationTemplate notification, string currentUserId, string image)
        {
            bool pushSent = false;
            try
            {
                string[] deviceTokenList = _repositoryWrapper.Account.FindByCondition(x => userList.Contains(x.a_ID)).Select(x => x.DeviceId).ToArray();

                string[] mDeviceTokenList = _repositoryWrapper.Account.FindByCondition(x => userList.Contains(x.a_ID)).Select(x => x.MobileDeviceId).ToArray();
                string[] deviceTokens = deviceTokenList.Concat(mDeviceTokenList).ToArray();
                var data = new { form_name = notification.Form };
                // Sending devices to firebase server to send notification
                pushSent = await PushNotificationLogic.SendPushNotification(deviceTokens, notification.TitleMessage, notification.BodyMessage, data);
                // saving notification send list
                SaveNotificationSendList(_repositoryWrapper, userList, notification, pushSent);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                WriteSystemLog(LogType.Error, "GlobalFunctions - SendNotification", ex.Message.ToString(), "");
            }
            return pushSent;
        }

        private static void SaveNotificationSendList(IRepositoryWrapper _repositoryWrapper, List<string> userList, tblNotificationTemplate notification, bool pushSent)
        {
            for (int i = 0; i < userList.Count(); i++)
            {
                tblNotificationSendList notiSendModel = new tblNotificationSendList();
                notiSendModel.UserId = userList[i];
                notiSendModel.ViewFlag = false;
                notiSendModel.SendDate = System.DateTime.Now;
                //notiSendModel.Status = Status.Send;
                if (notification != null)
                {
                    notiSendModel.NotificationId = notification.NotificationId;
                }
                if (pushSent)
                    notiSendModel.Status = Status.Send;
                else
                    notiSendModel.Status = Status.ERROR;
                _repositoryWrapper.NotificationSendList.Create(notiSendModel);
            }
            _repositoryWrapper.Save();
        }
        public static void SendEmail(dynamic emailSetting, bool ishtmlBody = false)
        {
            string fromEmail = emailSetting.fromEmail;
            string pass = emailSetting.password;
            string toEmail = emailSetting.toEmail;
            int port = Convert.ToInt32(emailSetting.port);
            bool enableSSL = emailSetting.enableSSL == "true" ? true : false;

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(emailSetting.senderUserName, fromEmail));
            message.To.Add(new MailboxAddress(emailSetting.receiverUserName, toEmail.Trim()));
            message.Subject = emailSetting.subject;
            var bodyBuilder = new BodyBuilder();
            if (ishtmlBody)
            {
                bodyBuilder.HtmlBody = emailSetting.emessage;
                message.Body = bodyBuilder.ToMessageBody();
            }
            else
            {
                message.Body = new TextPart("plain")
                {
                    Text = emailSetting.emessage
                };
            }

            using (var client = new SmtpClient())
            {
                //accept all SSL certificates
                //client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                // client.SslProtocols = System.Security.Authentication.SslProtocols.Tls11;
                client.Connect(emailSetting.host, port, enableSSL);
                client.Authenticate(fromEmail, pass);
                client.Send(message);
                client.Disconnect(true);
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static void SaveAuditLog(IRepositoryWrapper _repositoryWrapper, string logType, string msg, string userId, string altId, string funtionName, string ip)
        {
            tblAudit audit = new tblAudit();
            audit.LogType = logType;
            audit.showId = LoginData.showId;
            audit.Message = msg;
            audit.UserId = userId;
            audit.AlternativeId = altId;
            audit.FunctionName = funtionName;
            audit.LogDateTime = DateTime.Now;
            audit.IPAddress = ip;
            _repositoryWrapper.Audit.Create(audit);
            _repositoryWrapper.Save();
        }

        public static byte[] ExportExcel(dynamic dataList, List<string> columnsHeader, List<string> fieldNames, string heading)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet = package.Workbook.Worksheets.Add(heading);
                using (var cells = worksheet.Cells[1, 1, 1, 15])
                {
                    cells.Style.Font.Bold = true;
                    //cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //cells.Style.Fill.BackgroundColor.SetColor(Color.Green);
                }
                //First add the headers
                for (int i = 0; i < columnsHeader.Count; i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnsHeader[i];
                }

                //Add values
                var j = 2;
                var count = 1;
                foreach (var item in dataList)
                {
                    string jstr = Newtonsoft.Json.JsonConvert.SerializeObject(item);
                    dynamic jobj = Newtonsoft.Json.JsonConvert.DeserializeObject(jstr);

                    int unicode = 65;
                    foreach (var field in fieldNames)
                    {
                        if (jobj.GetValue(field) != null)
                        {
                            string colindex = ((char)unicode).ToString();
                            worksheet.Cells[colindex + j].Value = jobj.GetValue(field).ToString();
                        }
                        unicode++;
                    }

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }

            return result;
        }
        public static void CacheTryGetValueSet(string entryKey, dynamic cacheData, IMemoryCache _cache)
        {
            object cacheEntry;
            // Look for cache key.
            //if (!_cache.TryGetValue(entryKey, out cacheEntry))
            //{
                string expiredtimeString = GetAppSettingValue("MemoryCache:ExpiredSeconds");
                double expiredSeconds = Convert.ToDouble(expiredtimeString);
                string isSlidingString = GetAppSettingValue("MemoryCache:IsSliding");
                Boolean isSliding = Convert.ToBoolean(isSlidingString);
                // Key not in cache, so get data.
                cacheEntry = cacheData;

                if (isSliding)
                {
                    // Set cache options.
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        // Keep in cache for this time, reset time if accessed.
                        .SetSlidingExpiration(TimeSpan.FromSeconds(expiredSeconds));

                    // Save data in cache.
                    _cache.Set(entryKey, cacheEntry, cacheEntryOptions);
                }
                else
                {
                    // Set cache options.
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        // Keep in cache for this time, reset time if accessed.
                        .SetAbsoluteExpiration(TimeSpan.FromSeconds(expiredSeconds));

                    // Save data in cache.
                    _cache.Set(entryKey, cacheEntry, cacheEntryOptions);
                }
            //}
        }

        public static dynamic CacheGet(string entryKey, IMemoryCache _cache)
        {
            var cacheEntry = _cache.Get<dynamic>(entryKey);
            return cacheEntry;
        }

        public static dynamic CacheGetOrCreate(string entryKey, dynamic cacheData, IMemoryCache _cache)
        {
            string expiredtimeString = GetAppSettingValue("MemoryCache:ExpiredSeconds");
            double expiredSeconds = Convert.ToDouble(expiredtimeString);
            var cacheEntry = _cache.GetOrCreate(entryKey, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromSeconds(expiredSeconds);
                return cacheData;
            });

            return cacheEntry;
        }

        public async static Task<dynamic> CacheGetOrCreateAsynchronous(string entryKey, object cacheData, IMemoryCache _cache)
        {
            string expiredtimeString = GetAppSettingValue("MemoryCache:ExpiredSeconds");
            double expiredSeconds = Convert.ToDouble(expiredtimeString);
            var cacheEntry = await
            _cache.GetOrCreateAsync(entryKey, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromSeconds(expiredSeconds);
                return Task.FromResult(cacheData);
            });

            return cacheEntry;
        }

        public static void CacheRemove(string entryKey, IMemoryCache _cache)
        {
            _cache.Remove(entryKey);
        }

        public static string GetAppSettingValue(string appKey)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string rtnVal = Configuration.GetSection(appKey).Value;
            return rtnVal;
        }

        public static bool isRequireCreateCache()
        {
            string isOpenString = GetAppSettingValue("MemoryCache:Status");
            Boolean isOpen = Convert.ToBoolean(isOpenString);
            return isOpen;
        }
    }
}