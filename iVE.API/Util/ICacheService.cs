using System.Threading.Tasks;
namespace iVE.API
{
    public interface ICacheService
    {

        string GetCache(string key);
        void SetCache(string key, string value);
    }
}