using iVE.BLL.Implementation;
using iVE.BLL.Interface;
using iVE.DAL.Repository.Implementation;
using iVE.DAL.Repository.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using iVE.BLL;
namespace iVE.API {
    public static class ServiceExtensions {
        public static void ConfigureRepositoryWrapper (this IServiceCollection services) {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper> ();
        }

        //For All business logic repository
        public static void ConfigureBLLRepositoryWrapper (this IServiceCollection services) {
            services.AddScoped<IContactPersonLogic, ContactPersonLogic> ();
            services.AddScoped<IExhibitorLogic, ExhibitorLogic> ();
            services.AddScoped<IProductLogic, ProductLogic> ();
            services.AddScoped<ISponsorLogic, SponsorLogic> ();
            services.AddScoped<IBoothLogic, BoothLogic> ();
            services.AddScoped<IBoothTemplateLogic, BoothTemplateLogic> ();
            services.AddScoped<IChatLogic, ChatLogic> ();
            services.AddScoped<ISponsorCategoryLogic, SponsorCategoryLogic> ();
            services.AddScoped<IShowConfigLogic, ShowConfigLogic> ();
            services.AddScoped<IAccountLogic, AccountLogic>();
            services.AddScoped<IFavouriteLogic, FavouriteLogic>();
            services.AddScoped<IVisitorRecommendationLogic, VisitorRecommendationLogic>();
            services.AddScoped<IProductTypeLogic, ProductTypeLogic> ();
            services.AddScoped<IAppointmentLogic, AppointmentLogic> ();
            services.AddScoped<ICountryLogic, CountryLogic> ();
            services.AddScoped<IFeedbackBoardLogic, FeedbackBoardLogic>();
            services.AddScoped<ILeaderBoardLogic, LeaderBoardLogic>();
            services.AddScoped<IMCQScoreBoardLogic, MCQScoreBoardLogic>();
            services.AddScoped<ITemplateConfigLogic, TemplateConfigLogic>();
            services.AddScoped<IFileStorageLogic, FileStorageLogic>();
            services.AddScoped<IStorageStrategy, AwsStorageStrategy>();
            services.AddScoped<IStorageStrategy, FileStorageStrategy>();
            services.AddScoped<IShowLogic, ShowLogic>();
            services.AddScoped<IAdminLogic,AdminLogic>();
            services.AddScoped<IChatConfigurationLogic,ChatConfigurationLogic>();
            services.AddScoped<IExhibitorTypeLogic,ExhibitorTypeLogic>();
            services.AddScoped<IExhibitorCategoryLogic,ExhibitorCategoryLogic>();
        }

        public static void ConfigureDALRepositoryWrapper (this IServiceCollection services) { }

        public static void ConfigureCors (this IServiceCollection services) {
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string url = Configuration.GetSection ("appSettings:SingnalUrl").Value;

            var corsBuilder = new CorsPolicyBuilder ();
            corsBuilder.AllowAnyHeader ();
            corsBuilder.AllowAnyMethod ();
            corsBuilder.AllowAnyOrigin (); // For anyone access.
            //corsBuilder.AllowCredentials();

            var corsBuilder2 = new CorsPolicyBuilder ();
            corsBuilder2.WithOrigins (url);
            corsBuilder2.AllowAnyHeader ();
            corsBuilder2.AllowAnyMethod ();
            corsBuilder2.AllowCredentials ();
            corsBuilder2.SetIsOriginAllowed ((host) => true);
            services.AddCors (options => {
                options.AddPolicy ("CorsAllowAllPolicy", corsBuilder.Build ());
                options.AddPolicy ("CorsPolicy", corsBuilder2.Build ());
            });
        }

        public static void ConfigureIISIntegration (this IServiceCollection services) {
            services.Configure<IISOptions> (options => {
                options.AutomaticAuthentication = false;
            });
            services.Configure<IISOptions> (options => {
                options.ForwardClientCertificate = false;
            });
            services.Configure<IISServerOptions> (options => {
                options.AllowSynchronousIO = true;
            });
        }

    }

}