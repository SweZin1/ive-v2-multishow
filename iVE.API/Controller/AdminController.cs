using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.IO;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Microsoft.Extensions.Caching.Memory;
using System.Threading.Tasks;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class AdminController : BaseController
    {
        private IAdminLogic _admin;
        private IMemoryCache _memoryCache;
        private readonly IFileStorageLogic _fileStorage;
        public AdminController(IAdminLogic admin, IMemoryCache memoryCache, IFileStorageLogic fileStorage)
        {
            _admin = admin;
            _memoryCache = memoryCache;
            _fileStorage = fileStorage;
        }

        [Authorize (Roles = "Super Admin")]
        [HttpGet("{Id}")]
        public dynamic GetAdmin(string Id)
        {
            dynamic objResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    int id = Convert.ToInt32(Id);
                    dynamic result = _admin.GetAdminBySuperAdmin(id);
                    objResponse = new { data = result, success = true, message = "Successfully Retrieved" };
                }
                else
                {
                    objResponse = new { data = false, success = false, message = "Can't retrieve Admin Data!" };
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "AdminController - GetAdmin", "Id : " + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AdminController - GetAdmin", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost("{showId}/List")]
        public dynamic GetAdminList([FromBody] JObject param, string showId)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _admin.GetAdminListBySuperAdmin(obj, showId);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AdminController - GetAdminList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpDelete("{Id}")]
        public dynamic DeleteAdmin(string Id)
        {
            dynamic objResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    int id = Convert.ToInt32(Id);
                    dynamic result = _admin.DeleteAdminBySuperAdmin(id);
                    objResponse = new { success = result, message = result ? "Successfully Deleted" : "Can't Delete Admin" };
                }
                else
                {
                    objResponse = new { success = false, message = "Can't retrieve Admin Data!" };
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "AdminController - DeleteAdmin", "Id : " + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AdminController - DeleteAdmin", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost]
        public dynamic AddOrUpdateAdmin([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _admin.AddOrUpdateAdminBySuperAdmin(obj);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AdminController - AddOrUpdateAdmin", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to add or update" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost("ChangePassword")]
        public dynamic ChangePassword([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                bool result = _admin.ChangeAdminPassword(obj);
                objResponse = new { success = result, message = result ? "Successfully Updated" : "Can't Change Password" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AdminController - ChangePassword", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to update." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost("File/{ID}")]
        public async Task<dynamic> AdminProfileUpload(IFormFile file, string ID)
        {
            dynamic objResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(ID))
                {
                    if (file != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileextension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
                        string url = await _fileStorage.UploadFile(file, "admin", ID, filename, StorageStrategy.Aws);
                        int Id = Convert.ToInt32(ID);
                        _admin.UpdateAdminProfile(Id, url);
                        objResponse = new { success = true, message = "Successfully Uploaded" };
                    }
                    else
                    {
                        objResponse = new { success = false, message = "File is empty" };
                    }
                }
                else
                {
                    objResponse = new { success = false, message = "Id is empty" };
                }

                GlobalFunction.WriteSystemLog(LogType.Info, "AdminController - AdminProfileUpload", "Id : " + ID, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AdminController - AdminProfileUpload", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { success = false, message = "failed to upload" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}