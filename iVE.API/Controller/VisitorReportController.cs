using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using iVE.API;
using iVE.DAL;
using iVE.BLL.Interface;
using iVE.DAL.Util;
using iVE.DAL.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System;
using Serilog;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using iVE.DAL.Models;

namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class VisitorReportController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        private IAccountLogic _accountLogic;
        public VisitorReportController(IRepositoryWrapper rw, IAccountLogic accountLogic)
        {
            _repositoryWrapper = rw;
            _accountLogic = accountLogic;
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("Reports/VisitorCountryGroup")]
        public dynamic GetVisitorsByCountry()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _accountLogic.GetVisitorByCountry();
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorReportController - GetVisitorsByCountry", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Reports/ActiveUsers")]
        public dynamic GetActiveLoginUsers([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            try
            {
                dynamic data = _accountLogic.GetActiveLoginUsers(obj,false);
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, data);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorReportController - GetActiveLoginUsers", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("ExcelExport/ActiveUsers")]
        public dynamic ExportActiveUserList([FromBody] JObject param)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                var objResult = _accountLogic.GetActiveLoginUsers(obj,true);
                List<string> Header = new List<string> { "Name", "Designation", "Company", "Country" };
                List<string> fieldName = new List<string> { "name", "designation", "company", "country" };

                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "ActiveUserList");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"ActiveUserList_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorReportController - ExportActiveUserList", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

    }
}