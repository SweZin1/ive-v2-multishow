using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class ExhibitorCategoryController: BaseController
    {
        private IExhibitorCategoryLogic _exhibitorCategoryLogic;
        public ExhibitorCategoryController(IExhibitorCategoryLogic exhCategory)
        {
            _exhibitorCategoryLogic = exhCategory;
        }

        [HttpPost]
        public dynamic GetExhibitorCategories ([FromBody] JObject param) {
            dynamic objResponse = null;
            dynamic obj = param;
            try {
                dynamic data = _exhibitorCategoryLogic.getExhibitorCategories(obj);
                objResponse = new { data = data, success = true, message = "Successfully retrieved." };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorCategoryController - GetExhibitorCategories", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet]
        public dynamic GetExhibitorCategories () {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorCategoryLogic.getExhibitorCategories();
                objResponse = new { data = data, success = true, message = "Successfully retrieved." };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorCategoryController - GetExhibitorCategories", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{Id}")]
        public dynamic UpdateOrCreateExhibitorType (string Id,[FromBody] JObject param) {
            dynamic objResponse = null;
            dynamic obj = param;
            try {
                objResponse = _exhibitorCategoryLogic.createorUpdateExhibitorCategory(Id,obj);
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorCategoryController - UpdateOrCreateExhibitorType", "Update exhibitor category.", _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorCategoryController - UpdateOrCreateExhibitorType", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{Id}")]
        public dynamic GetExhibitorCategoryDetails (string Id) {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorCategoryLogic.getExhibitorCategoryDetails(Id);
                return StatusCode (StatusCodes.Status200OK, data);
            } catch (Exception ex) {
                objResponse = new { data = "", success = false, message = "Failed to retrieve." };
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorCategoryController - GetExhibitorCategoryDetails", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete ("{Id}")]
        public dynamic DeleteExhibitorCategory (string Id) {
            dynamic objResponse = null;
            try {
                objResponse = _exhibitorCategoryLogic.deleteExhibitorCategory(Id);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = false, message = "Failed to delete." };
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorCategoryController - DeleteExhibitorCategory", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}