using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System.Dynamic;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class ShowConfigController:BaseController
    {
        private IShowConfigLogic _showConfigLogic;
        public ShowConfigController(IShowConfigLogic showConfigLogic)
        {
            _showConfigLogic = showConfigLogic;
        }

        [HttpGet]
        public dynamic GetShowConfig()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _showConfigLogic.GetShowConfig();
                objResponse = new { data = data, success = true, message = "Successfully retrieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowConfigController - GetShowConfig", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retrieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost]
        public dynamic CreateOrUpdateShowConfig([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            try
            {
                objResponse = _showConfigLogic.CreateOrUpdateShowConfig(obj);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowConfigController - CreateOrUpdateShowConfig", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

    }
}