using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System.Dynamic;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class SponsorCategoryController:BaseController
    {
        private ISponsorCategoryLogic _sponsorCategoryLogic;
        public SponsorCategoryController(ISponsorCategoryLogic cl)
        {
            _sponsorCategoryLogic = cl;
        }

        [HttpGet]
        public dynamic GetSponsorCategories () {
            dynamic objResponse = null;
            try {
                dynamic data = _sponsorCategoryLogic.GetSponsorCategories();
                objResponse = new { data = data, success = true, message = "Successfully retrieved." };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "SponsorCategoryController - GetSponsorCategories", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{id}")]
        public dynamic UpdateOrCreateSponsorCategory (string id,[FromBody] JObject param) {
            dynamic objResponse = null;
            dynamic obj = param;
            try {
                objResponse = _sponsorCategoryLogic.UpdateOrCreateSponsorCategory(id,obj);
                GlobalFunction.WriteSystemLog (LogType.Info, "SponsorCategoryController - UpdateOrCreateSponsorCategory", "Update sponsor category.", _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "SponsorCategoryController - UpdateOrCreateSponsorCategory", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{id}")]
        public dynamic GetSponsorCategory (string id) {
            dynamic objResponse = null;
            try {
                dynamic data = _sponsorCategoryLogic.GetSponsorCategory(id);
                objResponse = new { data = data, success = true, message = "Successfully retrieved." };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = false, message = "Failed to retrieve." };
                GlobalFunction.WriteSystemLog (LogType.Error, "SponsorCategoryController - GetSponsorCategory", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete ("{id}")]
        public dynamic DeleteSponsorCategory (string id) {
            dynamic objResponse = null;
            try {
                objResponse = _sponsorCategoryLogic.DeleteSponsorCategory(id);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = false, message = "Failed to delete." };
                GlobalFunction.WriteSystemLog (LogType.Error, "SponsorCategoryController - DeleteSponsorCategory", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}