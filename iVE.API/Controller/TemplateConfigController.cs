using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.IO;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Microsoft.Extensions.Caching.Memory;
using System.Threading.Tasks;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class TemplateConfigController : BaseController
    {
        private ITemplateConfigLogic _templateConfig;
        private IMemoryCache _memoryCache;
        private readonly IFileStorageLogic _fileStorage;
        public TemplateConfigController(ITemplateConfigLogic templateConfig, IMemoryCache memoryCache,IFileStorageLogic fileStorage)
        {
            _templateConfig = templateConfig;
            _memoryCache = memoryCache;
            _fileStorage = fileStorage;
        }

        [HttpGet("{Id}")]
        public dynamic GetTemplateConfig (string Id) {
            dynamic objResponse = null;
            try {
                if(!string.IsNullOrEmpty(Id))
                {
                    int id = Convert.ToInt32(Id);
                    dynamic result = _templateConfig.GetTemplateConfig(id);
                    objResponse = new { data = result, success = true, message = "Successfully Retrieved" };
                }
                else{
                    objResponse = new { data = false, success = false, message = "Please check Template Id!" };
                }
                GlobalFunction.WriteSystemLog (LogType.Info, "TemplateConfigController - GetTemplateConfig", "Id : " + Id, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "TemplateConfigController - GetTemplateConfig", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete("{Id}")]
        public dynamic DeleteTemplateConfig (string Id) {
            dynamic objResponse = null;
            try {
                if(!string.IsNullOrEmpty(Id))
                {
                    int id = Convert.ToInt32(Id);
                    dynamic result = _templateConfig.DeleteTemplateConfig(id);
                    objResponse = new { data = result, success = true, message = "Successfully Deleted" };
                }else{
                    objResponse = new { data = false, success = false, message = "Please check Template Id!" };
                }
                GlobalFunction.WriteSystemLog (LogType.Info, "TemplateConfigController - DeleteTemplateConfig", "Id : " + Id, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "TemplateConfigController - DeleteTemplateConfig", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost]
        public dynamic AddOrUpdateTemplateConfig([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _templateConfig.AddOrUpdateTemplateConfig(obj);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "TemplateConfigController - AddOrUpdateTemplateConfig", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to add or update" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }

        [HttpPost("Config")]
        public dynamic GetTemplate([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _templateConfig.GetTemplate(obj);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "TemplateConfigController - GetTemplateConfigListByTemplateType", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("TemplateConfigList")]
        public dynamic GetTemplateConfigListByTemplateType([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _templateConfig.GetTemplateConfigList(obj);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "TemplateConfigController - GetTemplateConfigListByTemplateType", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{Id}/Status/{Status}")]
        public dynamic UpdateStatus(string Id, string Status)
        {
            dynamic objResponse = null;
            try
            {
                if(!string.IsNullOrEmpty(Id))
                {
                    int id = Convert.ToInt32(Id);
                    dynamic data = _templateConfig.ChangeStatus(id, Status);
                    objResponse = new { data = data, success = true, message = "Successfully Updated" };
                }
                else{
                    objResponse = new { data = false, success = false, message = "Please check Template Id!" };
                }
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "TemplateConfigController - GetTemplateConfigListByTemplateType", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("File/{ID}/MediaType/{MediaType}/FileType/{FileType}")]
        public async Task<dynamic> TemplateFileUpload (IFormFile file,string ID,string MediaType,string FileType) {
            dynamic objResponse = null;
            string url = "";
            try {
                if(!string.IsNullOrEmpty(ID))
                {
                    if(file != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileextension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
                        url = await _fileStorage.UploadFile(file,FileType,ID,filename,StorageStrategy.Aws);
                        if(FileType != "boothconfig" && FileType != "lobbyconfig")
                        {
                            int Id = Convert.ToInt32(ID);
                            _templateConfig.SaveUrl(Id, url,MediaType);
                        }
                        objResponse = new { data = url, success = true, message = "Successfully upload" };
                    }
                    else
                    {
                         objResponse = new { data = url, success = false, message = "File is empty" };
                    }
                    
                }else{
                    objResponse = new { data = url, success = false, message = "Id is empty" };
                }
                
                GlobalFunction.WriteSystemLog (LogType.Info, "TemplateConfigController - TemplateFileUpload", "Id : " + ID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "TemplateConfigController - TemplateFileUpload", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}