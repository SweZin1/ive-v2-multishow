using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.IO;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Microsoft.Extensions.Caching.Memory;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class ShowController : BaseController
    {
        private IShowLogic _show;
        private IMemoryCache _memoryCache;
        private readonly IFileStorageLogic _fileStorage;
        public ShowController(IShowLogic show, IMemoryCache memoryCache, IFileStorageLogic fileStorage)
        {
            _show = show;
            _memoryCache = memoryCache;
            _fileStorage = fileStorage;
        }

        [Authorize (Roles = "Super Admin")]
        [HttpGet("{Id}")]
        public dynamic GetShow(string Id)
        {
            dynamic objResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    dynamic result = _show.GetShowBySuperAdmin(Id);
                    objResponse = new { data = result, success = true, message = "Successfully Retrieved" };
                }
                else
                {
                    objResponse = new { data = false, success = false, message = "Can't retrieve Show Data!" };
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "ShowController - GetShow", "Id : " + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - GetShow", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost("List")]
        public dynamic GetShowList([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _show.GetShowListBySuperAdmin(obj);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - GetShowList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpDelete("{Id}")]
        public dynamic DeleteShow(string Id)
        {
            dynamic objResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    dynamic result = _show.DeleteShowBySuperAdmin(Id);
                    objResponse = new { success = result, message = result ? "Successfully Deleted" : "Can't Delete Show" };
                }
                else
                {
                    objResponse = new { success = false, message = "Can't retrieve Show Data!" };
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "ShowController - DeleteShow", "Id : " + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - DeleteShow", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost]
        public dynamic AddOrUpdateShow([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _show.AddOrUpdateShowBySuperAdmin(obj);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - AddOrUpdateShow", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to add or update" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost("Status")]
        public dynamic UpdateStatus([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            try
            {
                _show.ChangeStatusForShow(obj);
                objResponse = new { success = true, message = "Successfully Updated" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - UpdateStatus", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpGet("{ID}/Layout")]
        public async Task<dynamic> GetLayout(string ID)
        {
            GlobalFunction.WriteSystemLog(LogType.Info,"show", "ID : " + ID, "");
            dynamic objResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(ID))
                {
                    var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                    var configuration = appsettingbuilder.Build();
                    string bucketPath = configuration.GetSection("AWSConfigs:bucketPath").Value;
                    string filename = _show.GetThemeConfigFileName(ID);
                    string filePath = bucketPath + "show";
                    GlobalFunction.WriteSystemLog(LogType.Info,"show", "filename : " + filename, "");
                    GlobalFunction.WriteSystemLog(LogType.Info,"show", "filePath : " + filePath, "");
                    string resultString = await _fileStorage.ReadFile(filePath, filename, StorageStrategy.Aws);
                    var result = JsonConvert.DeserializeObject(resultString);
                    GlobalFunction.WriteSystemLog(LogType.Info,"show", "Result : " + result, "");
                    objResponse = new { data = result, success = true, message = "Successfully Uploaded" };   
                }
                else
                {
                    objResponse = new {  success = false, message = "Id is empty" };
                }

                GlobalFunction.WriteSystemLog(LogType.Info, "ShowController - GetLayout", "Id : " + ID, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - GetLayout", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { success = false, message = "failed to retrieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost("Layout")]
        public dynamic ChangeLayout([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            GlobalFunction.WriteSystemLog(LogType.Info,"show", "obj : " + obj, "");
            try
            {
                string ID = obj.id;
                if (!string.IsNullOrEmpty(ID))
                {
                    var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                    var configuration = appsettingbuilder.Build();
                    string bucketPath = configuration.GetSection("AWSConfigs:bucketPath").Value;
                    string mainUrl = configuration.GetSection("AWSConfigs:mainUrl").Value;
                    string folderPath = configuration.GetSection("AWSConfigs:folderPath").Value;
                    string filename = ID + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".json";
                    
                    string filePath = bucketPath + "show";
                    dynamic layout = obj.layout.ToString();
                    MemoryStream layoutStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(layout));
                    GlobalFunction.WriteSystemLog(LogType.Info, "show","filename : " + filename, "");
                    GlobalFunction.WriteSystemLog(LogType.Info,"show", "filePath : " + filePath, "");
                    _fileStorage.WriteFile(layoutStream, filePath, filename, StorageStrategy.Aws);
                    string url = mainUrl + folderPath + "show/" + filename;
                    GlobalFunction.WriteSystemLog(LogType.Info,"show", "url : " + url, "");
                    GlobalFunction.WriteSystemLog(LogType.Info,"show", "ID : " + ID, "");
                    _show.UpdateShowConfigUrl(ID,url);
                    objResponse = new { success = true, message = "Successfully Changed" };
                }
                else
                {
                    objResponse = new { success = false, message = "Id is empty" };
                }

                GlobalFunction.WriteSystemLog(LogType.Info, "ShowController - ChangeLayout", "Id : " + ID, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - ChangeLayout", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { success = false, message = "Fail to change Layout" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        // Organizer
        [Authorize (Roles = "Organizer")]
        [HttpPost("Configure")]
        public dynamic ConfigureShow([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _show.UpdateShowByOrganizer(obj);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - ConfigureShow", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to configure" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpGet("Configure")]
        public dynamic GetShowDataByOrganizer()
        {
            dynamic objResponse = null;
            try
            {
                dynamic result = _show.GetShowByOrganizer();
                objResponse = new { data = result, success = true, message = "Successfully Retrieved" };
                GlobalFunction.WriteSystemLog(LogType.Info, "ShowController - GetShowDataByOrganizer", "Id : " + _tokenData.showId, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ShowController - GetShowDataByOrganizer", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer,Super Admin")]
        [HttpPost ("File/{ID}")]
        public async Task<dynamic> ShowFileUpload (IFormFile file,string ID) {
            dynamic objResponse = null;
            string url = "";
            try {
                if(!string.IsNullOrEmpty(ID))
                {
                    if(file != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileextension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
                        url = await _fileStorage.UploadFile(file,"show",ID,filename,StorageStrategy.Aws);
                        objResponse = new { data = url, success = true, message = "Successfully upload" };
                    }
                    else
                    {
                         objResponse = new { data = url, success = false, message = "File is empty" };
                    }
                    
                }else{
                    objResponse = new { data = url, success = false, message = "Id is empty" };
                }
                
                GlobalFunction.WriteSystemLog (LogType.Info, "ShowController - ShowFileUpload", "Id : " + ID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ShowController - ShowFileUpload", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}