using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.IO;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Microsoft.Extensions.Caching.Memory;
using System.Threading.Tasks;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class ChatConfigurationController : BaseController
    {
        private IChatConfigurationLogic _chat;
        private IMemoryCache _memoryCache;
        private readonly IFileStorageLogic _fileStorage;
        public ChatConfigurationController(IChatConfigurationLogic chat, IMemoryCache memoryCache, IFileStorageLogic fileStorage)
        {
            _chat = chat;
            _memoryCache = memoryCache;
            _fileStorage = fileStorage;
        }

        [Authorize (Roles = "Super Admin")]
        [HttpGet("{Id}")]
        public dynamic GetChatConfiguration(string Id)
        {
            dynamic objResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    int id = Convert.ToInt32(Id);
                    dynamic result = _chat.GetChatConfig(id);
                    objResponse = new { data = result, success = true, message = "Successfully Retrieved" };
                }
                else
                {
                    objResponse = new { data = false, success = false, message = "Can't retrieve ChatConfiguration Data!" };
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "ChatConfigurationController - GetChatConfiguration", "Id : " + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ChatConfigurationController - GetChatConfiguration", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpGet]
        public dynamic GetChatConfiguration()
        {
            dynamic objResponse = null;
            try
            {
                dynamic result = _chat.GetChatConfigs();
                objResponse = new { data = result, success = true, message = "Successfully Retrieved" };
                GlobalFunction.WriteSystemLog(LogType.Info, "ChatConfigurationController - GetChatConfiguration", "", _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ChatConfigurationController - GetChatConfiguration", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost("List")]
        public dynamic GetChatConfigurationList([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _chat.GetChatConfigList(obj);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ChatConfigurationController - GetChatConfigurationList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpDelete("{Id}")]
        public dynamic DeleteChatConfiguration(string Id)
        {
            dynamic objResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    int id = Convert.ToInt32(Id);
                    dynamic result = _chat.DeleteChatConfig(id);
                    objResponse = new { success = result, message = result ? "Successfully Deleted" : "Can't Delete ChatConfiguration" };
                }
                else
                {
                    objResponse = new { success = false, message = "Can't retrieve ChatConfiguration Data!" };
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "ChatConfigurationController - DeleteChatConfiguration", "Id : " + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ChatConfigurationController - DeleteChatConfiguration", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Super Admin")]
        [HttpPost]
        public dynamic AddOrUpdateChatConfiguration([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _chat.AddOrUpdateChatConfig(obj);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ChatConfigurationController - AddOrUpdateChatConfiguration", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to add or update" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }

    }
}