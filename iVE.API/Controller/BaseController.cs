using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using iVE.DAL.Models;
using iVE.DAL;
namespace iVE.API.Controllers
{
    [Route("api/v2/[controller]")]
    public class BaseController : Controller
    {
        public TokenData _tokenData = new TokenData();
        public string _ipaddress = "";
        public string _clienturl = "";
         public static string showId { get; set; }
        public static string showPrefix { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            _ipaddress = context.HttpContext.Connection.RemoteIpAddress.ToString();
            //_ipaddress = "127.0.0.1";
            _clienturl = context.HttpContext.Request.Headers["Referer"];
            setDefaultDataFromToken();
        }

        public void setDefaultDataFromToken()
        {
            try
            {
                string access_token = "";
                var hdtoken = Request.Headers["Authorization"];
                if (hdtoken.Count > 0)
                {
                    access_token = hdtoken[0];
                    access_token = access_token.Replace("Bearer ", "");
                    var handler = new JwtSecurityTokenHandler();
                    var tokenS = handler.ReadToken(access_token) as JwtSecurityToken;
                    _tokenData = GlobalFunction.GetTokenData(tokenS);
                    LoginData.showId = _tokenData.showId;
                    LoginData.userId = _tokenData.showId;
                    showId = _tokenData.showId;
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "BaseController", ex.Message.ToString(), _tokenData.UserID);
            }
        }
        // public IActionResult VCard (VCard vCard) {
        //     return new vCardActionResult (vCard);
        // }
    }
}