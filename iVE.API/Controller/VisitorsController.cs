using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Threading.Tasks;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System.IO;
using System.Data;

namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class VisitorsController : BaseController
    {
        private IAccountLogic _accountLogic;
        private IVisitorRecommendationLogic _visitorRecLogic;
        private IProductLogic _productLogic;
        private IRepositoryWrapper _repositoryWrapper;
        private IFavouriteLogic _favouriteLogic;
        private IMemoryCache _memoryCache;
        private readonly IFileStorageLogic _fileStorageLogic;
        public VisitorsController(IAccountLogic accountLogic, IVisitorRecommendationLogic visitorRecLogic, IProductLogic productLogic, IRepositoryWrapper repositoryWrapper
        , IFavouriteLogic favouriteLogic,IMemoryCache memoryCache,IFileStorageLogic fileStorageLogic)
        {
            _accountLogic = accountLogic;
            _visitorRecLogic = visitorRecLogic;
            _productLogic = productLogic;
            _favouriteLogic = favouriteLogic;
            _repositoryWrapper = repositoryWrapper;
            _memoryCache = memoryCache;
            _fileStorageLogic = fileStorageLogic;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost]
        public dynamic GetVisitors([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _accountLogic.GetVisitor(obj);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetVisitors", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }

        [HttpGet("{Id}")]
        public dynamic GetVisitorsById(string Id)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _accountLogic.GetVisitorById(Id);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetVisitorsById", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{Id}/Visitors")]
        public dynamic GetVisitedExhListByVisitor(string Id)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _accountLogic.GetVisitedExhListByVisitor(Id);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetVisitedExhListByVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{Id}/Favourite/Exhibitors")]
        public dynamic GetFavouriteExhibitorsByVisitor(string Id)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _accountLogic.GetFavouriteExhibitorsByVisitorId(Id);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetFavouriteExhibitorsByVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{Id}/Favourite/Products")]
        public dynamic GetFavouriteProductsByVisitor(string Id)
        {
            dynamic objResponse = null;
            try
            {
                
                dynamic data = _accountLogic.GetFavouriteProductsByVisitorId(Id);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetFavouriteProductsByVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        #region VisitedExhibitor
        [HttpPost("{VID}/VisitedExhibitor")]
        public dynamic GetVisitedExhibitorByVisitor([FromBody] JObject param, string VID)
        {
            //Get all the exhibitors visited by visitor
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _accountLogic.GetVisitedExhibitor(obj, VID);
                return StatusCode(StatusCodes.Status200OK, data);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetVisitedExhibitorByVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion
        [HttpPost("{VID}/Favourite/Exhibitors/{Id}")]
        public dynamic AddUserFavouriteByVisitor(string vId, string Id)
        {
            dynamic objResponse = null;
            try
            {
                _accountLogic.AddUsersFavouriteByVisitor(vId, Id);
                objResponse = new { data = "", success = true, message = "Successfully updated" };
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - AddUserFavouriteByVisitor", "UserId:" + vId + ", FavItemId:" + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - AddUserFavouriteByVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete("{VID}/Favourite/Exhibitors/{Id}")]
        public dynamic RemoveUserFavouriteByVisitor(string vId, string Id)
        {
            dynamic objResponse = null;
            try
            {
                _accountLogic.RemoveUsersFavourite(vId, Id);
                objResponse = new { data = "", success = true, message = "Successfully deleted" };
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - RemoveUserFavouriteByVisitor", "UserId:" + vId + ", FavItemId:" + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - RemoveUserFavouriteByVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{VID}/Exhibitor/{ExhID}/Products")]
        public dynamic GetAllExhibitorProductsByVisitorId([FromBody] JObject param, string vId, string ExhID)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                objResponse = _accountLogic.GetAllExhibitorProductsByVisitorId(obj, ExhID, vId);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetAllExhibitorProductsByVisitorId", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{VID}/Recommendations")]
        public dynamic GetAllRecommendedExhibitorsByVisitor(string vId)
        {
            dynamic objResponse = null;
            try
            {
                objResponse = _visitorRecLogic.GetAllRecommendedExhibitorsByVisitor(vId);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetAllRecommendedExhibitorsByVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        [HttpPost("{VID}/Recommendations")]
        public dynamic AddVisitorRecommendation([FromBody] JObject param, string vId)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                string data = _visitorRecLogic.AddVisitorRecommendation(obj, vId);
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - AddVisitorRecommendation","UserId:" + vId, _tokenData.UserID);
                objResponse = new { data = data, success = true, message = "Successfully updated" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - AddVisitorRecommendation", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to update" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Booth/{ProductID}")]
        public dynamic GetProductByID(string ProductID)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _productLogic.GetProductInfoById(ProductID);
                string logMessage = "Visitor Viewed Product";
                string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.ViewProduct, logMessage, _tokenData.UserID, ProductID, "GetProductByID", ip);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetProductByID", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{VID}/Exhibitor/{ExhId}/FavouriteStatus")]
        public dynamic GetFavouriteExhStatusByVisitor(string vId, string ExhId)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _accountLogic.GetFavouriteExhStatusByVisitor(vId, ExhId);
                string logMessage = "Visitor Visited Exhibitor";
                string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.VisitExhibitor, logMessage, vId, ExhId, "GetFavouriteExhStatusByVisitor", ip);
                
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetFavouriteExhStatusByVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{ID}")]
        public dynamic CreateOrUpdateVisitor([FromBody] JObject param, string ID)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            try
            {
                objResponse = _accountLogic.CreateOrUpdateVisitor(obj, ID);
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - CreateOrUpdateVisitor", "UserId:" + ID , _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - CreateOrUpdateVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpDelete("{Id}")]
        public dynamic DeleteVisitor(string Id)
        {
            dynamic objResponse = null;
            string msg = "Successfully deleted.";
            try
            {
                _accountLogic.DeleteVisitorByVisitorId(Id);
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - DeleteVisitor", msg + ", UserId:" + Id, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - DeleteVisitor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{UserId}/Favourite/Conference/{Id}")]
        public dynamic AddUserFavouriteToConference(string UserId, string Id)
        {
            dynamic objResponse = null;
            try
            {
                _favouriteLogic.UpdateUsersFavourite(UserId, Id, FavouriteItem.Conference);
                objResponse = new { data = "", success = true, message = "Successfully updated" };
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - AddUserFavouriteToConference", "FavItem:" + FavouriteItem.Conference + ", UserId:" + UserId, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - AddUserFavouriteToConference", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Exhibitor/{ExhId}/ExitedExhibitionHall")]
        public dynamic UpdateLogForVisitorExitedExhibitionHall(string ExhId)
        {
            dynamic objResponse = null;
            string userID = _tokenData.UserID;
            try
            {
                string logMessage = "Visitor Exited Exhibitor";
                string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.ExitExhibitor, logMessage, userID, ExhId, "UpdateLogForVisitorExitedExhibitionHall", ip);
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - UpdateLogForVisitorExitedExhibitionHall","LogType:"+AuditLogType.ExitExhibitor+", LogMsg:"+ logMessage + ", UserId:" + userID+", AlternativeId:"+ ExhId , _tokenData.UserID);
                objResponse = new { data = userID, success = true, message = "Successfully updated." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - UpdateLogForVisitorExitedExhibitionHall", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to update" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{Module}/{ChatType}/{AlternativeId}/{IsStarted}/UpdateLog")]
        public dynamic UpdateLogForChatRoom(string Module, int ChatType, string AlternativeId, string IsStarted)
        {
            dynamic objResponse = null;
            string userID = _tokenData.UserID;
            string logMessage = "";
            string alternativeID = null;
            string logType = string.Empty;
            try
            {
                //Module ===> Exhibitor = Exh, NetworkLounge = NL, HelpDesk = HD
                //ChatType ===> 1 = Group Chat, 2 = One-to-One Chat
                //IsStarted ===> 1 = Started, 0 = Ended
                string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                alternativeID = AlternativeId;

                // ****** Exhibitor ChatRoom *******//
                if (Module == "Exh" && ChatType == 1)
                {
                    //Group Chat
                    logMessage = "Join Exhibitor Group Chat";
                    logType = AuditLogType.Started_JoinExhibitorGroupChat;
                    if (IsStarted == "0")
                    {
                        logType = AuditLogType.Ended_JoinExhibitorGroupChat;
                    }
                }
                if (Module == "Exh" && ChatType == 2)
                {
                    //One-to-One Chat
                    logMessage = "Chat with Exhibitor";
                    logType = AuditLogType.Started_ChatWithExhibitor;
                    if (IsStarted == "0")
                    {
                        logType = AuditLogType.Ended_ChatWithExhibitor;
                    }
                }
                if (Module == "NL")
                {
                    //Network Lounge Group Chat
                    logMessage = "Join Network Lounge Group Chat";
                    logType = AuditLogType.Started_JoinNetworkLogGroupChat;
                    if (IsStarted == "0")
                    {
                        logType = AuditLogType.Ended_JoinNetworkLogGroupChat;
                    }
                }
                if (Module == "HelpDesk")
                {
                    //Help desk chat
                    logMessage = "Join Help Desk Chat";
                    logType = AuditLogType.Started_JoinHelpDeskChat;
                    if (IsStarted == "0")
                    {
                        logType = AuditLogType.Ended_JoinHelpDeskChat;
                    }
                }
                if (Module == "ConnChat")
                {
                    //One to One connection chat
                    logMessage = "Join One-to-One Chat";
                    logType = AuditLogType.Started_JoinCONNECTIONChat;
                    if (IsStarted == "0")
                    {
                        logType = AuditLogType.Ended_JoinCONNECTIONChat;
                    }
                }

                GlobalFunction.SaveAuditLog(_repositoryWrapper, logType, logMessage, userID, alternativeID, "UpdateLogForChatRoom", ip);
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - UpdateLogForChatRoom", "LogType:"+logType+", LogMsg:"+ logMessage + ", UserId:" + userID+", AlternativeId:"+ alternativeID, _tokenData.UserID);
                objResponse = new { data = userID + "_" + logType, success = true, message = "Successfully updated." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - UpdateLogForChatRoom", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to update" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete("DeleteCache")]
        public dynamic ClearCache([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic paraObj = param;
            string key = paraObj.key;
            string msg = "Successfully removed.";
            dynamic data = null;
            try
            {
                if (_memoryCache.TryGetValue(key, out data))
                {
                    GlobalFunction.CacheRemove(key,_memoryCache);
                }
                objResponse = new { data = key, success = true, message = msg };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - ClearCache", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("RetrieveCache")]
        public dynamic RetrieveFromCache([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic paraObj = param;
            string key = paraObj.key;
            string msg = "Successfully retrieved.";
            dynamic data = null;
            bool result = true;
            try
            {
                if (!_memoryCache.TryGetValue(key, out data))
                {
                    data = null;
                }
                if(data == null){
                    msg = "There is no data to retrieve.";
                    result = false;
                }
                objResponse = new { data = data, success = result, message = msg };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - RetrieveFromCache", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retrieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("{ID}/File")]
        public async Task<dynamic> UploadProfilePicture (IFormFile file, string ID) {
            dynamic objResponse = null;
            string msg = "Successfully uploaded.";
            try {
                string url = "";
                if(string.IsNullOrEmpty(ID)) return new { data = url, success = false, message = "Id is empty." };
                if (file == null) return new { data = url, success = false, message = "File is empty." };

                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                string fileextension = Path.GetExtension(file.FileName);
                filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
                url = await _fileStorageLogic.UploadFile(file, "ProfilePicture", ID, filename, StorageStrategy.Aws);
                _accountLogic.updateProfilePicUrl(ID,url);

                objResponse = new { data = url, success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "VisitorsController - UploadProfilePicture", msg + ", ID:" + ID, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "VisitorsController - UploadProfilePicture", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Salutation")]        
        public dynamic GetSalutation()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _accountLogic.GetSalutation();
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "VisitorsController - GetSalutation", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("DownloadExcel")]
        public dynamic DownloadExcelTemplate () {
            try {
                byte[] fileContents = _accountLogic.DownloadExcelTemplate();
                return File (
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"AccountSetupTemplate_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "VisitorsController - DownloadExcelTemplate", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost ("UploadExcel")]
        public dynamic UploadExcel (IFormFile file) {
            dynamic objResponse = null;
            try {
                objResponse = _accountLogic.UploadExcelFile (file);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "VisitorsController - UploadExcel", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("SaveExcel")]
        public dynamic SaveExcelData ([FromBody] JObject param) {
            dynamic objResponse = null;
            dynamic result = null;
            dynamic obj = param;
            try {
                if (obj == null) {
                    objResponse = new { data = "", success = true, message = "There is no data to save." };
                    return StatusCode (StatusCodes.Status200OK, objResponse);
                }
                result = _accountLogic.SaveExcelData(obj);
                return StatusCode (StatusCodes.Status200OK, result);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "VisitorsController - SaveExcelData", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to save." };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("TempAccounts")]
        public dynamic GetTempAccounts ([FromBody] JObject param) {
            dynamic objResponse = null;
            dynamic obj = param;
            try {
                objResponse = _accountLogic.getTempAccounts (obj);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "VisitorsController - GetTempAccounts", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retrieve." };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpPost ("ChangePassword")]
        public dynamic ChangePassword ([FromBody] JObject param) {
            dynamic obj = param;
            dynamic objResponse = null;
            try {

                objResponse = _accountLogic.changePassword (obj);
                string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                GlobalFunction.SaveAuditLog (_repositoryWrapper, AuditLogType.ChangedContactPersonPwd, "Changed contact person password", _tokenData.UserID, "", "ChangeContactPersonPassword", ip);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "VisitorsController - ChangePassword", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to update." };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

    }
}