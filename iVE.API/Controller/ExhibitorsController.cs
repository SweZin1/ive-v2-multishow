using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.Intrinsics.X86;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Syncfusion.XlsIO;

namespace iVE.API.Controllers {
    [Authorize]
    [ApiController]
    [Route ("api/v2/[controller]")]
    public class ExhibitorsController : BaseController {
        private IContactPersonLogic _contactPersonLogic;
        private IExhibitorLogic _exhibitorLogic;
        private IProductLogic _productLogic;
        private IBoothLogic _boothLogic;
        private IChatLogic _chatLogic;
        private IRepositoryWrapper _repositoryWrapper;
        private IProductTypeLogic _productTypeLogic;
        private IFavouriteLogic _favouriteLogic;
        private readonly IHubContext<SignalHub> _hubContext;
        private IMemoryCache _memoryCache;
        private readonly IFileStorageLogic _fileStorage;
        string returnUrl;
        public ExhibitorsController (IContactPersonLogic contactPersonLogic, IExhibitorLogic exhibitorLogic,
            IProductLogic productLogic, IBoothLogic boothLogic, IChatLogic chatLogic, IRepositoryWrapper RW,
            IProductTypeLogic productTypeLogic, IFavouriteLogic favouriteLogic, IHubContext<SignalHub> hubContext, IMemoryCache memoryCache,IFileStorageLogic fileStorage)
        {
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            returnUrl = Configuration.GetSection ("appSettings:ProfilePicUrl").Value;

            _contactPersonLogic = contactPersonLogic;
            _exhibitorLogic = exhibitorLogic;
            _productLogic = productLogic;
            _boothLogic = boothLogic;
            _chatLogic = chatLogic;
            _repositoryWrapper = RW;
            _productTypeLogic = productTypeLogic;
            _favouriteLogic = favouriteLogic;
            _hubContext = hubContext;
            _memoryCache = memoryCache;
            _fileStorage = fileStorage;
        }


        [HttpGet ("Halls")]
        public dynamic GetHallList () {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.GetHallList ();
                string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetHallList", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        
        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpGet ("ExhibitorTypes")]
        public dynamic GetExhibitorTypeList () {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.GetExhibitorTypeList ();
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetExhibitorTypeList", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        
        // Exhibitor
        #region  Exhibitor
        //[Authorize(Roles = "Organizer,Visitor")]
        [HttpPost]
        public dynamic GetExhibitionList ([FromBody] JObject param) {
            dynamic obj = param;
            dynamic objResponse = null;
            dynamic data = null;
            try {
                if (GlobalFunction.isRequireCreateCache ()) {
                    
                    dynamic exhList = new ExpandedObjectFromApi (ENUM_Cache.ExhibitionList);
                    string key = exhList.key;
                    if (!_memoryCache.TryGetValue (key, out data)) {
                        data = _exhibitorLogic.GetAllExhibitionList (obj);
                        GlobalFunction.CacheTryGetValueSet (key, data, _memoryCache);
                    }
                } else {
                    data = _exhibitorLogic.GetAllExhibitionList (obj);
                }
                return StatusCode (StatusCodes.Status200OK, data);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetExhibitionList", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("Organization")]
        public dynamic GetExhibitionListForOrganization ([FromBody] JObject param) {
            dynamic obj = param;
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.GetAllExhibitionListForOrganization (obj);
                return StatusCode (StatusCodes.Status200OK, data);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetExhibitionListForOrganization", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpPost ("{ID}")]
        public dynamic UpdateExhibitorById ([FromBody] JObject param, string ID) {
            dynamic obj = param;
            dynamic objResponse = null;
            string message = null;
            try {
                message = _exhibitorLogic.CreateOrUpdateExhibior (obj, ID, ref ID);
                if (message != "") {
                    objResponse = new { data = "false", success = false, message = message };
                } else {
                    ClearExhibitingCompanyCache(ID);
                    message = "Successfully updated.";
                    objResponse = new { data = "true", success = true, message = message };
                }
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UpdateExhibitorById", message + ", ExhId:" + ID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UpdateExhibitorById", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to updated" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("Organizational/{ID}")]
        public dynamic UpdateExhibitorByIdForOrg ([FromBody] JObject param, string ID) {
            dynamic obj = param;
            dynamic objResponse = null;
            string message = null;
            try {
                message = _exhibitorLogic.CreateOrUpdateExhibiorOrg (obj, ID, ref ID);
                if (message != "") {
                    objResponse = new { data = "false", success = false, message = message };
                } else {
                    ClearExhibitingCompanyCache(ID);
                    message = "Successfully updated.";
                    objResponse = new { data = "true", success = true, message = message };
                }
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - UpdateExhibitorByIdForOrg", message + ", ExhId:" + ID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UpdateExhibitorByIdForOrg", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to updated" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}")]
        public dynamic GetExhibitorById (string ID) {
            dynamic objResponse = null;
            dynamic data = null;
            try {
                if (GlobalFunction.isRequireCreateCache ()) {
                    dynamic exhCompany = new ExpandedObjectFromApi (ENUM_Cache.ExhibitingCompany);
                    string key = exhCompany.key + ID;
                    if (!_memoryCache.TryGetValue (key, out data)) {
                        data = _exhibitorLogic.GetExhibitorById (ID);
                        GlobalFunction.CacheTryGetValueSet (key, data, _memoryCache);
                    }
                } else {
                    data = _exhibitorLogic.GetExhibitorById (ID);
                }
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetExhibitorById", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("{ID}/File")]
        public async Task<dynamic> UploadFileForExhibitor (IFormFile file, string ID) {
            dynamic objResponse = null;
            try {
                string url = "";
                if(!string.IsNullOrEmpty(ID))
                {
                    if(file != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileextension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
                        url = await _fileStorage.UploadFile(file,"Exhibitor",ID,filename,StorageStrategy.Aws);
                        objResponse = new { data = url, success = true, message = "Successfully upload" };
                    }
                    else
                    {
                         objResponse = new { data = url, success = false, message = "File is empty" };
                    }
                    
                }else{
                    objResponse = new { data = url, success = false, message = "Id is empty" };
                }
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - UploadFileForExhibitor", "ExhId:" + ID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UploadFileForExhibitor", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{type}/ChatActiveStatus")]
        public dynamic ChangeExhibitorChatStatus (int type) {
            dynamic objResponse = null;
            try {
                bool data = _exhibitorLogic.ChangeExhibitorChatStatus (_tokenData.UserID, type);
                objResponse = new { data = data, success = true, message = "Successfully change" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - ChangeExhibitorChatStatus", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to change" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpPost ("{ID}/ChangeStatus/{Status}")]
        public dynamic UpdateExhibitorStatusByExhibitorId (string ID, string Status) {
            dynamic objResponse = null;
            try {
                _exhibitorLogic.UpdateExhibitorStatus (ID, Status);
                ClearExhibitingCompanyCache(ID);
                string templateId = _repositoryWrapper.Template.FindByCondition(x => x.showId == showId && x.ExhID == ID).Select(x => x.TemplateID).FirstOrDefault();
                ClearBoothTemplateCache(templateId);
                objResponse = new { data = "", success = true, message = "Successfully updated" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UpdateExhibitorStatusByExhibitorId", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to Updated" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        #endregion 
        // Contact Person
        #region  Person
        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpPost ("{ID}/Contacts/{ContactID}")]
        public dynamic AddNewContactPerson ([FromBody] JObject param, string ID, string ContactID) {
            dynamic obj = param;
            dynamic objResponse = null;
            try {
                
                objResponse = _contactPersonLogic.AddOrUpdateContactPerson (obj, ID, ContactID);
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - AddNewContactPerson", "ExhId:" + ID + ", ContactPersonId:" + ContactID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - AddNewContactPerson", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Contacts")]
        public dynamic GetAllContacPerson (string ID) {
            dynamic objResponse = null;
            try {
                dynamic data = _contactPersonLogic.GetAllContactPersonByExhId (ID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetAllContacPerson", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpGet ("{ID}/Contacts/{ContactID}")]
        public dynamic GetContacPersonDetails (string ID, string ContactID) {
            dynamic objResponse = null;
            try {
                dynamic data = _contactPersonLogic.GetContactPersonById (ID, ContactID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetContacPersonDetails", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Contacts/{type}/type")]
        public dynamic GetMainContactPerson (string type) {
            dynamic objResponse = null;
            try {
                dynamic data = _contactPersonLogic.GetContactPersonByType (type);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };

                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetMainContactPerson", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpDelete ("{ID}/Contacts/{ContactID}")]
        public dynamic DeleteContactPersonByExhId (string ID, string ContactID) {
            dynamic objResponse = null;
            string msg = "Successfully Deleted.";
            try {
                _contactPersonLogic.DeleteContactPersonByExhId (ID, ContactID);
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - DeleteContactPersonByExhId", msg + ", ExhId:" + ID + ", ContactPersonId:" + ContactID, _tokenData.UserID);

                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - DeleteContactPersonByExhId", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        /*8-7-2020*/
        //[Authorize(Roles = "Organizer")]
        [HttpGet ("{ID}/ContactsWithStatus")]
        public dynamic GetAllContacPersonWithStatus (string ID) {
            dynamic objResponse = null;
            try {
                dynamic data = _contactPersonLogic.GetAllContactPersonsWithStatusByExhId (ID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetAllContacPersonWithStatus", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("{ID}/Contacts/{type}/GroupStatus")]
        public dynamic ChangeExhibitorGroupChatStatus (string ID, int type) {
            dynamic objResponse = null;
            try {
                dynamic data = _contactPersonLogic.ChangeExhibitorGroupChatStatus (ID, type);
                ClearExhibitingCompanyCache(ID);
                objResponse = new { data = data, success = true, message = "Successfully change" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - ChangeExhibitorGroupChatStatus", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to change" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpGet ("{ID}/Contacts/CheckBadgeEntitlement")]
        public dynamic CheckBadgeEntitlement (string ID) {
            dynamic objResponse = null;
            bool result = true;
            try {
                result = _contactPersonLogic.CheckBadgeEntitlement (ID);
                if (!result) {
                    objResponse = new { data = "false", success = false, message = "Can't create. Exceeded the max count for contact person." };
                } else {
                    objResponse = new { data = "true", success = true, message = "" };
                }
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - CheckBadgeEntitlement", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        #endregion
        // Products 
        #region Products

        [HttpGet ("ProductType")]
        public dynamic GetProductTypeByID () {
            dynamic objResponse = null;
            try {
                dynamic data = _productTypeLogic.GetProductTypeList ();
                string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetProductTypeByID", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpPost ("{ID}/Products")]
        public dynamic GetAllProduct (string ID, [FromBody] JObject param) {
            dynamic obj = param;
            dynamic objResponse = null;
            try {
                dynamic data = _productLogic.GetAllProductList (ID, obj);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetAllProduct", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Products")]
        public dynamic GetAllProduct (string ID) {
            dynamic objResponse = null;
            try {
                dynamic data = _productLogic.GetAllProductList (ID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetAllProduct", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Products/{ProductID}")]
        public dynamic GetProductByID (string ID, string ProductID) {
            dynamic objResponse = null;
            try {
                dynamic data = _productLogic.GetProductDetailsById (ID, ProductID);
                string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetProductByID", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpPost ("{ID}/Products/{ProductID}")]
        public dynamic CreateOrUpdateProduct ([FromBody] JObject param, string ID, string ProductID) {
            dynamic objResponse = null;
            dynamic obj = param;
            string msg = "Successfully updated.";
            bool result = true;
            try {
                _productLogic.CreateOrUpdateProductByExhId (obj, ID, ProductID, ref ProductID);
                objResponse = new { data = ProductID, success = result, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - CreateOrUpdateProduct", msg + ", ProductId:" + ProductID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - CreateOrUpdateProduct", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpDelete ("{ID}/Products/{ProductID}")]
        public dynamic DeleteProduct (string ID, string ProductID) {
            dynamic objResponse = null;
            string msg = "Successfully deleted.";
            try {
                _productLogic.DeleteProductByProductId (ID, ProductID);
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - DeleteProduct", msg + ", ProductId:" + ProductID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - DeleteProduct", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete." };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        
        [Authorize (Roles = "Exhibitor")]
        [HttpPost ("Product/{ProductID}/File")]
        public async Task<dynamic> UploadFileForProduct (IFormFile file, string ProductID) {
            dynamic objResponse = null;
            string msg = "Successfully uploaded.";
            try {
                string url = "";
                if(!string.IsNullOrEmpty(ProductID))
                {
                    if(file != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileextension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
                        url = await _fileStorage.UploadFile(file,"Product",ProductID,filename,StorageStrategy.Aws);
                        _productLogic.SaveProductUrl (ProductID, url);
                        objResponse = new { data = url, success = true, message = "Successfully upload" };
                    }
                    else
                    {
                         objResponse = new { data = url, success = false, message = "File is empty" };
                    }
                    
                }else{
                    objResponse = new { data = url, success = false, message = "Id is empty" };
                }
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - UploadFileForProduct", msg + ", ProductId:" + ProductID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UploadFileForProduct", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Products/{ProductID}/Commants")]
        public dynamic GetCommantListByProductId (string ID, string ProductID) {
            dynamic objResponse = null;
            try {
                dynamic data = _productLogic.GetProductCommantListByProductId (ID, ProductID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetCommantListByProductId", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{ID}/Products/ViewedCount")]
        public dynamic GetProductViewedCountByExhibitorId (string ID) {
            dynamic objResponse = null;
            try {
                dynamic resultObj = _productLogic.GetProductViewedCountByExhibitorId (ID);
                objResponse = new { data = resultObj, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetProductViewedCountByExhibitorId", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retereieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Products/{ProductID}/Favourite")]
        public dynamic AddFavouriteProduct (string ID, string ProductID) {
            dynamic objResponse = null;
            try {
                //dynamic data = _productLogic.AddToMyFavourite(_tokenData.UserID, ProductID);
                dynamic data = _productLogic.AddToMyFavourite (ID, ProductID);
                objResponse = new { data = data, success = true, message = "Successfully updated" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - AddFavouriteProduct", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to update" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete ("{ID}/Products/{ProductID}/Favourite")]
        public dynamic RemoveFavouriteProduct (string ID, string ProductID) {
            dynamic objResponse = null;
            try {
                dynamic data = _productLogic.RemoveFavProduct (ID, ProductID);
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - RemoveFavouriteProduct", "UserId:" + ID + ", FavItemId:" + ProductID, _tokenData.UserID);
                objResponse = new { data = data, success = true, message = "Successfully updated" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - RemoveFavouriteProduct", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to update" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("Products/{ProductID}")]
        public dynamic GetProductURL (string ProductID) {
            dynamic objResponse = null;
            try {
                string productUrl = _productLogic.GetProductURL (ProductID);
                //string logMessage = "Visitor Viewed Product";
                //string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                //GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.ViewProduct, logMessage, _tokenData.UserID, ProductID, "GetProductURL", ip);
                objResponse = new { data = productUrl, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetProductURL", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        // Booths 
        #region  Booths

        [HttpGet ("ExhibitorBoothTemplate")]
        public dynamic GetExhibitorBoothTemplates () {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.GetExhBoothTemplates ();
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetExhibitorBoothTemplates", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Booth")]
        public dynamic GetBoothList (string ID) {
            dynamic objResponse = null;
            try {
                dynamic data = _boothLogic.GetAllBooth (ID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetBoothList", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retereieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Booth/{BoothID}")]
        public dynamic GetBoothByBoothId (string ID, string BoothID) {
            dynamic objResponse = null;
            try {
                dynamic data = _boothLogic.GetBoothById (ID, BoothID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetBoothByBoothId", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retereieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("Booth/{TemplateId}")]
        public dynamic GetBoothByTemplateId (string TemplateId) {
            dynamic objResponse = null;
            dynamic resultObj = null;
            try {
                if (GlobalFunction.isRequireCreateCache ()) {
                    dynamic exhBooth = new ExpandedObjectFromApi (ENUM_Cache.ExhibitionBooth);
                    string key = exhBooth.key + showId +TemplateId;
                    if (!_memoryCache.TryGetValue (key, out resultObj)) {
                        resultObj = _boothLogic.GetBoothbyTemplateId (TemplateId);
                        GlobalFunction.CacheTryGetValueSet (key, resultObj, _memoryCache);
                    }
                } else {
                    resultObj = _boothLogic.GetBoothbyTemplateId (TemplateId);
                }
                dynamic expendObj = new ExpandedObjectFromApi (resultObj);
                string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                objResponse = new { data = resultObj, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetBoothByTemplateId", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retereieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpPost ("{ID}/Booth")]
        public dynamic CreateOrUpdateBoothByBoothId ([FromBody] JObject param, string ID) {
            dynamic objResponse = null;
            dynamic obj = param;
            string BoothID = "";
            string msg = "Successfully created.";
            try {
                BoothID = _boothLogic.CreateOrUpdateBoothById (ID, obj);
                ClearExhibitingCompanyCache(ID);
                ClearBoothTemplateCache(BoothID);
                objResponse = new { data = BoothID, success = true, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - CreateOrUpdateBoothByBoothId", msg + ", BoothId:" + BoothID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - CreateOrUpdateBoothByBoothId", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpPost ("Booth/{BoothTemplateId}/Config")]
        public dynamic UpdateBoothConfig ([FromBody] JObject param,string BoothTemplateId) {
            dynamic objResponse = null;
            dynamic obj = param;
            string msg = "Successfully updated.";
            try {
                _boothLogic.UpdateBoothFileConfig (BoothTemplateId,obj);
                ClearBoothTemplateCache(BoothTemplateId);
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - UpdateBoothConfig", msg + ", BoothId:" + BoothTemplateId, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UpdateBoothConfig", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to Updated" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer,Exhibitor")]
        [HttpPost ("{ID}/Booth/{BoothID}/File")]
        public async Task<dynamic> UploadFileForBooth (IFormFile file, string ID, string BoothID) {
            dynamic objResponse = null;
            string msg = "Successfully upload.";
            try {
                string url = "";
                if(!string.IsNullOrEmpty(ID))
                {
                    if(file != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileextension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
                        url = await _fileStorage.UploadFile(file,"Exhibitor",ID,filename,StorageStrategy.Aws);
                        objResponse = new { data = url, success = true, message = msg };

                    }else
                    {
                         objResponse = new { data = url, success = false, message = "File is empty" };
                    }
                    
                }else{
                    objResponse = new { data = url, success = false, message = "Id is empty" };
                }
                
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - UploadFileForBooth", msg + ", BoothId:" + BoothID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UploadFileForBooth", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("BoothTemplateZip/{ExhId}")]
        public async Task<dynamic> GetZipFile (string ExhId) {
            var zipStream = new MemoryStream ();
            var exhObj = _repositoryWrapper.Exhibitor.FindByCondition (x => x.showId == showId && x.ExhID == ExhId).FirstOrDefault ();
            string fileName = "demo";
            if (exhObj != null) {
                fileName = exhObj.CompanyName;
            }
            byte[] result = await _boothLogic.GetBoothTemplateZipFile (ExhId);
            return File (result, "application/zip", fileName + ".zip");
        }

        [HttpPost ("File/{ExhId}")]
        public async Task<dynamic> UpdataeBoothImage (IFormFile file,string ExhId) {
            dynamic objResponse = null;
            string url = "";
            try {
                if(!string.IsNullOrEmpty(ExhId))
                {
                    if(file != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileextension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
                        url = await _fileStorage.UploadFile(file,"booth",ExhId,filename,StorageStrategy.Aws);
                        dynamic obj = new System.Dynamic.ExpandoObject();
                        obj.url = url;
                        _boothLogic.UpdateTemplateImageByExhID(obj,ExhId);
                        objResponse = new { data = url, success = true, message = "Successfully upload" };
                    }
                    else
                    {
                         objResponse = new { data = url, success = false, message = "File is empty" };
                    }
                    
                }else{
                    objResponse = new { data = url, success = false, message = "Id is empty" };
                }
                
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - UpdataeBoothImage", "Id : " + ExhId, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UpdataeBoothImage", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        #region ChatContactGroupMember (8-7-2020 th)
        //[Authorize (Roles = "Exhibitor")]
        [HttpPost ("{ID}/GroupMember/{UID}")]
        public dynamic AddChatContactGroupMember (string ID, string UID) {
            dynamic objResponse = null;
            string msg = "Successfully added.";
            try {
                _exhibitorLogic.AddChatContactMember (ID, UID);
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - RemoveChatContactGroupMember", msg + ", ExhId:" + ID + ", ContactMemId:" + UID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - AddChatContactGroupMember", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to add" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        //[Authorize (Roles = "Exhibitor")]
        [HttpDelete ("{ID}/GroupMember/{UID}")]
        public dynamic RemoveChatContactGroupMember (string ID, string UID) {
            dynamic objResponse = null;
            string msg = "Successfully removed.";
            try {
                _exhibitorLogic.RemoveChatContactMember (ID, UID);
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - RemoveChatContactGroupMember", msg + ", ExhId:" + ID + ", ContactMemId:" + UID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - RemoveChatContactGroupMember", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to remove" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor,Visitor")]
        [HttpGet ("{ID}/GroupMembers")]
        public dynamic GetChatContactGroupMembers (string ID) {
            dynamic objResponse = null;
            try {
                dynamic resultObj = _exhibitorLogic.GetChatContactMembers (ID);
                objResponse = new { data = resultObj, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetChatContactGroupMembers", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retereieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/Chat")]
        public dynamic JoinChatWithContactPerson (string ID) {
            dynamic objResponse = null;
            string msg = "Successfully joined.";
            try {
                dynamic data = _chatLogic.JoinChatWithContactPerson (ID, _tokenData.UserID);
                objResponse = new { data = data, success = true, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - JoinChatWithContactPerson", msg + ", ExhId:" + ID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - JoinChatWithContactPerson", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to join" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        #region Update Template Image (8-7-2020 th)
        //[Authorize(Roles = "Organizer")]
        [HttpPost ("{ID}/Booth/TemplateImage")]
        public dynamic UpdateTemplateImage ([FromBody] JObject param, string ID) {
            dynamic objResponse = null;
            dynamic obj = param;
            string msg = "Successfully updated.";
            try {
                _boothLogic.UpdateTemplateImageByExhID (obj, ID);
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - UpdateTemplateImage", msg + "ExhId:" + ID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UpdateTemplateImage", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to Updated" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        #endregion

        #region  ExcelExport
        [HttpPost ("ExcelExport")]
        public dynamic ExhibitorsExport ([FromBody] JObject param) {
            byte[] fileContents;
            dynamic obj = param;
            try {
                var objResult = _exhibitorLogic.GetExhibitorList (obj);
                List<string> Header = new List<string> {
                    "ExhID",
                    "NavigationID",
                    "Company Name",
                    "Description",
                    "Status",
                    "Country",
                    "Exhibitor Type",
                    "Exhibitor Category",
                    "Sequence No",
                    "Booth No",
                    "Hall No",
                    "Contact Person",
                    "Email",
                    "Telephone",
                    "Fax"
                };
                List<string> fieldName = new List<string> {
                    "ExhID",
                    "TemplateID",
                    "CompanyName",
                    "Description",
                    "Status",
                    "Country",
                    "ExhType",
                    "ExhCategory",
                    "SequenceNo",
                    "BoothNo",
                    "HallNo",
                    "a_fullname",
                    "a_email",
                    "a_Tel",
                    "Fax"
                };

                fileContents = GlobalFunction.ExportExcel (objResult, Header, fieldName, "Exhibitors");
                if (fileContents == null || fileContents.Length == 0) {
                    return NotFound ();
                }

                return File (
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Exhibitors_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );

            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - ExhibitorsExport", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpPost ("{ID}/ExcelExport/VisitedVisitors/{FavStatus}")]
        public dynamic ExcelExportForVisiterVisitors ([FromBody] JObject param, string ID, string FavStatus) {
            byte[] fileContents;
            dynamic obj = param;
            try {
                dynamic objResult = _exhibitorLogic.GetVisitedVisitor (obj, ID, FavStatus, returnUrl, true);
                List<string> Header = new List<string> { "id", "Name ", "Designation", "Company", "Address", "Country", "Photo", "Visited Count", "Last Visited Date", "Favourite Status" };
                List<string> fieldName = new List<string> { "id", "fullName", "designation", "company", "address", "country", "photo", "VisitedCount", "lastVisitedDate", "favStatus" };

                fileContents = GlobalFunction.ExportExcel (objResult, Header, fieldName, "Exhibitor_VisitedVisitors");
                if (fileContents == null || fileContents.Length == 0) {
                    return NotFound ();
                }
                return File (
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Exhibitor_VisitedVisitors_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - ExcelExportForVisiterVisitors", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError);
            }
        }

        #endregion

        #region  Exhibitor Dashboard
        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{ExhId}/CountryDashboard")]
        public dynamic VisitedAnalysisByCountry (string ExhId) {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.VisitedAnalysisByCountry (ExhId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - VisitedAnalysisByCountry", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{ExhId}/DayDashboard")]
        public dynamic VisitedAnalysisByDay (string ExhId) {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.VisitedAnalysisByDay (ExhId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - VisitedAnalysisByDay", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{ExhId}/Dashboard")]
        public dynamic VisitedAnalysis (string ExhId) {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.VisitedAnalysis (ExhId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - VisitedAnalysis", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{ExhId}/CountryDashboard/ViewedProduct")]
        public dynamic ViewedExhProductByCountry (string ExhId) {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.ViewedExhProductByCountry (ExhId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - ViewedExhProductByCountry", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{ExhId}/Dashboard/FavouriteProduct")]
        public dynamic FavouriteExhProduct (string ExhId) {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.FavouriteExhProduct (ExhId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - FavouriteExhProduct", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{ExhId}/Dashboard/ExhibitorStatus")]
        public dynamic GetExhibitorDashboardStatus (string ExhId) {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.GetExhibitorDashboardStatus (ExhId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetExhibitorDashboardStatus", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        #endregion

        #region Exhibitor Mass Upload
        [HttpGet ("DownloadExhibitorTemplate")]
        public dynamic DownloadExhibitorTemplate () {
            try {
                //byte[] fileContents = iVE.API.GlobalFunction.DownloadExcelTemplate ("ExhibitorUpload");
                byte[] fileContents = null;
                return File (
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"ExhibitorTemplate_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - DownloadExhibitorTemplate", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost ("UploadExhibitorImport")]
        public dynamic UploadExhibitorImport (IFormFile file) {
            dynamic objResponse = null;
            string msg = "";
            try {
                DataTable dtResult = new DataTable ();
                //dtResult = iVE.API.GlobalFunction.UploadExcelFile (file);
                //////dynamic returnData = _exhibitorLogic.SaveExhibitorUpload(dtResult, ref msg);/*not use(direct save)*/
                dynamic returnData = dtResult;
                msg = "Successfull imported";
                objResponse = new { data = returnData, success = true, message = msg };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - UploadExhibitorImport", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("SaveExhibitorImport")]
        public dynamic SaveExhibitorImport () {
            //dynamic obj = param;
            dynamic objResponse = null;
            string msg = "";
            try {
                string tmpJson = "";
                using (var reader = new StreamReader (Request.Body)) {
                    var body = reader.ReadToEnd ();
                    tmpJson = body;

                }
                if (tmpJson == null) {
                    msg = "Data is null";
                    objResponse = new { data = "", success = true, message = msg };
                    return StatusCode (StatusCodes.Status200OK, objResponse);
                } else {
                    DataTable dtResult = JsonConvert.DeserializeObject<DataTable> (tmpJson);
                    dynamic returnData = _exhibitorLogic.SaveExhibitorUpload (dtResult, ref msg);
                    objResponse = new { data = returnData, success = true, message = msg };
                    return StatusCode (StatusCodes.Status200OK, objResponse);
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - SaveExhibitorImport", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to upload" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        [HttpGet ("VisitedVisitor/SendNotification/{UserId}")]
        public async Task<dynamic> SendNotifications (string UserId) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Can't Send Notification";
            try {
                var requestTemplate = _repositoryWrapper.NotificationTemplate.FindByCondition (x => x.showId == showId && x.TitleMessage == NotificationType.NotifyVisitedVisitor).SingleOrDefault ();
                string msg = requestTemplate.BodyMessage;
                StringBuilder emessage = new StringBuilder (msg);

                string titleMessage = requestTemplate.TitleMessage;

                var user = _repositoryWrapper.Account.FindByCondition (x => x.showId == showId && x.a_ID == _tokenData.UserID && x.deleteFlag == false).FirstOrDefault ();
                string bodyMessage = emessage.Replace ("<UserName>", user.a_fullname).ToString ();
                emessage.Replace ("<UserName>", user.a_fullname);
                _repositoryWrapper.NotificationSendList.SaveNotificationSendList (UserId, NotificationStauts.Send, titleMessage, bodyMessage);

                result = true;
                message = "Sent Notification Successfully.";
                await _hubContext.Clients.All.SendAsync ("NotificationCount", "Check Your Notification Count");
                objResponse = new { data = "", success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - SendNotifications", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("VisitedVisitor/NotificationData")]
        public dynamic GetNotificationData () {
            dynamic objResponse = null;
            dynamic data = null;
            try {
                var user = _repositoryWrapper.Account.FindByCondition (x => x.showId == showId && x.a_ID == _tokenData.UserID && x.deleteFlag == false).FirstOrDefault ();
                if (user != null) {
                    string userName = user.a_fullname;
                    if (!String.IsNullOrEmpty (user.ExhId)) {
                        string exId = user.ExhId;
                        var exh = _repositoryWrapper.Exhibitor.FindByCondition (x => x.showId == showId && x.ExhID == exId).FirstOrDefault ();
                        if (exh != null) {
                            if (!String.IsNullOrEmpty (exh.CompanyName)) {
                                userName += "( " + exh.CompanyName + " )";
                            }
                        }
                    }
                    data = _exhibitorLogic.GetNotificationData (userName);
                }
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetNotificationData", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("{exhId}/ChatStart")]
        public dynamic VisitorChat (string exhId) {
            dynamic objResponse = null;
            dynamic data = null;
            try {

                //remove emailStatus from ShowConfig table
                string emailStatus = "1";
                if (emailStatus == "1") {
                    bool result = VisitorVsited (exhId);
                    if (result) {
                        objResponse = new { data = data, success = true, message = "Successfully send email" };
                    } else {
                        objResponse = new { data = data, success = false, message = "Failed to  send email" };
                    }
                } else {
                    objResponse = new { data = data, success = false, message = "Failed to  send email.Email status is closed" };
                }

                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - VisitorChat", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to send email" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Exhibitor")]
        [HttpGet ("{ExhId}/Favourite/Visitors/{Id}")]
        public dynamic AddUserFavouriteByExhibitor (string ExhId, string Id) {
            dynamic objResponse = null;
            try {
                _favouriteLogic.UpdateUsersFavourite (ExhId, Id, FavouriteItem.Visitor);
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorsController - AddUserFavouriteByExhibitor", "UserId:" + ExhId + ", FavItem:" + FavouriteItem.Visitor + ", FavItemId:" + Id, _tokenData.UserID);
                objResponse = new { data = "", success = true, message = "Successfully updated" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - AddUserFavouriteByExhibitor", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("{ID}/VisitedVisitor/{FavStatus}")]
        public dynamic GetVisitedVisitorByExhibitor ([FromBody] JObject param, string ID, string FavStatus) {
            dynamic obj = param;
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorLogic.GetVisitedVisitor (obj, ID, FavStatus, returnUrl, false);
                objResponse = data;
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetVisitedVisitorByExhibitor", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        #region  Exhibition
        [HttpPost ("ExhibitionDirectory")]
        public dynamic GetExhibitionDirectoryLsit ([FromBody] JObject param) {
            dynamic obj = param;
            dynamic objResponse = null;
            dynamic data = null;
            try {
                data = _exhibitorLogic.GetAllExhibitionList (obj);
                return StatusCode (StatusCodes.Status200OK, data);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetExhibitionDirectoryLsit", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("ExhibitionList")]
        public dynamic GetExhibitionList() {
            dynamic objResponse = null;
            dynamic data = null;
            try {
                data = _exhibitorLogic.GetAllExhibitorList ();
                return StatusCode (StatusCodes.Status200OK, data);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorsController - GetExhibitionList", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        #region Private Methods
        private void ClearBoothTemplateCache(string templateId){
            dynamic data = null;
            dynamic keyObj = new ExpandedObjectFromApi (ENUM_Cache.ExhibitionBooth);
            string key = keyObj.key + templateId;
            if (_memoryCache.TryGetValue(key, out data))
            {
                GlobalFunction.CacheRemove(key, _memoryCache);
            }
        }
        private void ClearExhibitingCompanyCache(string exhId){
            dynamic exhListObj = null;
            dynamic exhComObj = null;
            dynamic exhList = new ExpandedObjectFromApi (ENUM_Cache.ExhibitionList);
            dynamic exhCompany = new ExpandedObjectFromApi (ENUM_Cache.ExhibitingCompany);
            string key = exhList.key;
            string exhKey = exhCompany.key + exhId;
            
            if (_memoryCache.TryGetValue(key, out exhListObj))
            {
                GlobalFunction.CacheRemove(key, _memoryCache);
            }
             if (_memoryCache.TryGetValue(exhKey, out exhComObj))
            {
                GlobalFunction.CacheRemove(exhKey, _memoryCache);
            }
        }
        private bool VisitorVsited (string exhId) {
            bool result = false;
            try {
                tbl_Account exhDetails = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == exhId && x.deleteFlag == false).FirstOrDefault ();
                if (exhDetails != null) {
                    // getting email template
                    var emailConfig = _repositoryWrapper.EmailConfig.FindByCondition (x => x.em_Title == EmailTitle.ChatRequest).FirstOrDefault ();
                    if (emailConfig != null) {
                        dynamic emailSetting = new System.Dynamic.ExpandoObject ();

                        string websiteLink = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.WebSiteLink && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        // string showName = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowName && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        emailSetting.fromEmail = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.SMTPUserName && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        emailSetting.password = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.SMTPPassword && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        emailSetting.senderUserName = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowAdminName && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        emailSetting.port = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.SMTPPort && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        emailSetting.host = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.SMTPHost && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        emailSetting.enableSSL = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.EnableSSL && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        emailSetting.subject = emailConfig.em_Subject;
                        emailSetting.receiverUserName = exhDetails.a_fullname;
                        string bodyMsg = replaceBodyMessage (emailConfig.em_Content);
                        string bmsg = GetEmailBody (bodyMsg, exhDetails.a_fullname, websiteLink);
                        emailSetting.emessage = bmsg;
                        emailSetting.toEmail = exhDetails.a_email;
                        GlobalFunction.SendEmail (emailSetting, true);
                        result = true;
                    }
                }
            } catch {

            }
            // getting exhibitor's data 

            return result;
        }

        private string GetEmailBody (string message, string userName, string websitelink) {
            //message = message.Remove("\n",)
            StringBuilder emessage = new StringBuilder (message);
            emessage.Replace ("<UserName>", userName);
            emessage.Replace ("<websitelink>", websitelink);

            //emessage.Replace("\n",Environment.NewLine);
            return emessage.ToString ();
        }

        private string replaceBodyMessage (string message) {
            message = message.Replace ("\\n", "\n");

            return message;
        }

        #endregion

    }
}