using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v2/[controller]")]
    public class ExhibitorTypeController: BaseController
    {
        private IExhibitorTypeLogic _exhibitorTypeLogic;
        public ExhibitorTypeController(IExhibitorTypeLogic exhType)
        {
            _exhibitorTypeLogic = exhType;
        }

        [HttpPost]
        public dynamic GetExhibitorTypes ([FromBody] JObject param) {
            dynamic objResponse = null;
            dynamic obj = param;
            try {
                objResponse = _exhibitorTypeLogic.getExhibitorTypes(obj);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorTypeController - GetExhibitorTypes", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet]
        public dynamic GetExhibitorTypes () {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorTypeLogic.getExhibitorTypes();
                objResponse = new { data = data, success = true, message = "Successfully retrieved." };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorTypeController - GetExhibitorTypes", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{Id}")]
        public dynamic UpdateOrCreateExhibitorType (string Id,[FromBody] JObject param) {
            dynamic objResponse = null;
            dynamic obj = param;
            try {
                objResponse = _exhibitorTypeLogic.createorUpdateExhibitorType(Id,obj);
                GlobalFunction.WriteSystemLog (LogType.Info, "ExhibitorTypeController - UpdateOrCreateExhibitorType", "Update exhibitor type.", _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorTypeController - UpdateOrCreateExhibitorType", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{Id}")]
        public dynamic GetExhibitorTypeDetails (string Id) {
            dynamic objResponse = null;
            try {
                dynamic data = _exhibitorTypeLogic.getExhibitorTypeDetails(Id);
                return StatusCode (StatusCodes.Status200OK, data);
            } catch (Exception ex) {
                objResponse = new { data = "", success = false, message = "Failed to retrieve." };
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorTypeController - GetExhibitorTypeDetails", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete ("{Id}")]
        public dynamic DeleteExhibitorType (string Id) {
            dynamic objResponse = null;
            try {
                objResponse = _exhibitorTypeLogic.deleteExhibitorType(Id);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = false, message = "Failed to delete." };
                GlobalFunction.WriteSystemLog (LogType.Error, "ExhibitorTypeController - DeleteExhibitorType", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}