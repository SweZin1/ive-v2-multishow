using System.Security.AccessControl;
using iVE.DAL.Models;
using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore;
using System;
namespace iVE.DAL
{
    public class AppDB : DbContext
    {
        public AppDB(DbContextOptions<AppDB> options) : base(options) { }
        public DbSet<SiteRunNumber> SiteRunNumbers { get; set; }
        public DbSet<tbl_Account> Accounts { get; set; }
        public DbSet<tbl_Administrator> Administrators { get; set; }
        public DbSet<tbl_UsersFavourite> UsersFavourites { get; set; }
        public DbSet<tblShowConfig> ShowConfigs { get; set; }
        public DbSet<tblNotificationTemplate> NotificationTemplates { get; set; }
        public DbSet<tblNotificationSendList> NotificationSendLists { get; set; }
        public DbSet<tblAudit> Audits { get; set; }
        public DbSet<tblHelpDesk> HelpDesks { get; set; }
        public DbSet<tblHelpChatLog> HelpChatLogs { get; set; }
        public DbSet<tblExhibitor> Exhibitors { get; set; }
        public DbSet<tblProduct> Products { get; set; }
        public DbSet<tblBoothTemplate> BoothTemplates { get; set; }
        public DbSet<tblTemplate> Templates { get; set; }
        public DbSet<tblProductCommant> ProductCommants { get; set; }
        public DbSet<tblSponsor> Sponsors { get; set; }
        public DbSet<tblSponsorCategory> SponsorCategories { get; set; }
        public DbSet<tblVisitorRecommendations> VisitorRecommendations { get; set; }
        public DbSet<tblVisitorRecommendationLog> VisitorRecommendationLog { get; set; }
        public DbSet<tblExhibitorProductType> ExhibitorProductTypes { get; set; }
        public DbSet<tblProductTypeMaster> ProductTypeMaster { get; set; }
        public DbSet<tbl_App_TimeSlot> AppTimeSlot { get; set; }
        public DbSet<tbl_Appointments> Appointments { get; set; }
        public DbSet<tbl_App_MySchedule> Schedules { get; set; }
        public DbSet<tbl_App_Request> AppRequest { get; set; }
        public DbSet<tbl_Day> Days { get; set; }
        public DbSet<tbl_Country> Countries { get; set; }
        public DbSet<tblHall> Halls { get; set; }
        public DbSet<tblExhibitorType> ExhibitorTypes { get; set; }
        public DbSet<tblBooth> Booths { get; set; }
        public DbSet<tblExhibitorCategory> ExhibitorCategories { get; set; }
        public DbSet<tblFeedbackboard> Feedbackboards { get; set; }
        public DbSet<tblUsersComment> UsersComments { get; set; }
        public DbSet<tblUsersCommentAction> UsersCommentActions { get; set; }
        public DbSet<tblLeaderBoard> LeaderBoards { get; set; }
        public DbSet<tblLeaderBoardEntitlePoints> LeaderBoardEntitlePoints { get; set; }
        public DbSet<tblAccountLoginBlocker> AccountLoginBlockers { get; set; }
        public DbSet<tblShowSponsors> ShowSponsors { get; set; }
        public DbSet<tblMCQScoreBoard> MCQScoreBoards { get; set; }
        public DbSet<tblConfigEmail> ConfigEmails { get; set; }
        public DbSet<tblChatMessageDetails> ChatMessageDetails { get; set; }
        public DbSet<tbl_TemplateConfig> tbl_TemplateConfigs { get; set; }
        public DbSet<tbl_Salutation> Salutations { get; set; }
        public DbSet<TempAccount> tempAccounts { get; set; }
        public DbSet<tblShow> Shows { get; set; }
        public DbSet<tblChatConfiguration> ChatConfigurations { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<SiteRunNumber>()
                .HasKey(c => new { c.prefix });
            modelBuilder.Entity<tbl_Account>()
                .HasKey(c => new { c.a_ID });
            modelBuilder.Entity<tbl_Administrator>()
                .HasKey(c => new { c.admin_id });
            modelBuilder.Entity<tbl_UsersFavourite>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tblShowConfig>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tblNotificationTemplate>()
                .HasKey(c => new { c.NotificationId });
            modelBuilder.Entity<tblNotificationSendList>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tblAudit>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tblHelpDesk>()
                .HasKey(c => new { c.IssueID });
            modelBuilder.Entity<tblHelpChatLog>()
                .HasKey(c => new { c.HelpChatLogID });
            modelBuilder.Entity<tblExhibitor>()
                .HasKey(c => new { c.ExhID });
            modelBuilder.Entity<tblProduct>()
                .HasKey(c => new { c.p_ID });
            modelBuilder.Entity<tblBoothTemplate>()
                .HasKey(c => new { c.BoothID });
            modelBuilder.Entity<tblTemplate>()
                .HasKey(c => new { c.TemplateID });
            modelBuilder.Entity<tblProductCommant>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tblSponsor>()
                .HasKey(c => new { c.s_ID });
            modelBuilder.Entity<tblSponsorCategory>()
                .HasKey(c => new { c.sponsorCat_ID });
            modelBuilder.Entity<tblVisitorRecommendations>()
                .HasKey(c => new { c.VisitorID, c.ExhID });
            modelBuilder.Entity<tblVisitorRecommendationLog>()
                .HasKey(c => new { c.VisitorID, c.ExhID });
            modelBuilder.Entity<tblExhibitorProductType>()
            .HasKey(c => new { c.ID });
            modelBuilder.Entity<tblProductTypeMaster>()
            .HasKey(c => new { c.ProductID });
            modelBuilder.Entity<tbl_App_TimeSlot>()
            .HasKey(c => new { c.TimeSlotID });
            modelBuilder.Entity<tbl_Appointments>()
            .HasKey(c => new { c.AppointmentID });
            modelBuilder.Entity<tbl_App_MySchedule>()
            .HasKey(c => new { c.OwnerID, c.AppointmentID });
            modelBuilder.Entity<tbl_App_Request>()
            .HasKey(c => new { c.ID });
            modelBuilder.Entity<tbl_Day>()
                .HasKey(c => new { c.d_ID });
            modelBuilder.Entity<tbl_Country>()
                .HasKey(c => new { c.ISO3Digit });
            modelBuilder.Entity<tblHall>()
                .HasKey(c => new { c.HallId });
            modelBuilder.Entity<tblExhibitorType>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<tblBooth>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<tblExhibitorCategory>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<tblFeedbackboard>()
                .HasKey(c => new { c.FebId });
            modelBuilder.Entity<tblUsersComment>()
                .HasKey(c => new { c.CommentID });
            modelBuilder.Entity<tblUsersCommentAction>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tblLeaderBoard>()
                .HasKey(c => new { c.ParticipantID });
            modelBuilder.Entity<tblLeaderBoardEntitlePoints>()
                .HasKey(c => new { c.PointKey });
            modelBuilder.Entity<tblAccountLoginBlocker>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<tblShowSponsors>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<tblMCQScoreBoard>()
                .HasKey(c => new { c.ParticipantID });
            modelBuilder.Entity<tblConfigEmail>()
                .HasKey(c => new { c.em_id });
            modelBuilder.Entity<tblChatMessageDetails>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tbl_TemplateConfig>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tbl_Salutation>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tblShow>()
                .HasKey(c => new { c.showId });
            modelBuilder.Entity<TempAccount>()
                .HasKey(c => new { c.Id });
            modelBuilder.Entity<tblChatConfiguration>()
                .HasKey(c => new { c.Id });
        }
    }
}