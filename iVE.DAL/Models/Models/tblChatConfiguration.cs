using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblChatConfiguration")]
    public class tblChatConfiguration
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string chatAppId { get; set; }
        public string region { get; set; }
        public string apiKey { get; set; }
        public string widget { get; set; }
        public string adminWidget { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime? updatedDate { get; set; }
    }
}