using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tbl_Salutation")]
    public class tbl_Salutation
    {
        public int Id { get; set; }
        public string sal_Name { get; set; }
        public int? sal_Seq { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime? updatedDate { get; set; }
    }
}