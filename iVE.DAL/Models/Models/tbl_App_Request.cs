using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tbl_App_Request")]
    public class tbl_App_Request
    {
        public int ID { get; set; }
        public string showId { get; set; }
        public string RequestFrom { get; set; }
        public string RequestTo { get; set; }
        public string TimeSlotID { get; set; }
        public string Status { get; set; }
        public string Action { get; set; }
        public string BookingType { get; set; }
        public DateTime RequestDateTime { get; set; }
        public DateTime? ActionTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool deleteFlag { get; set; }
    }
}