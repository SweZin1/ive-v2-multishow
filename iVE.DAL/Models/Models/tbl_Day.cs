using System.ComponentModel.DataAnnotations.Schema;
using System;
namespace iVE.DAL.Models
{
    [Table("tblDay")]
    public class tbl_Day
    {
        public string d_ID { get; set; }
        public string d_key { get; set; }
        public DateTime d_date { get; set; }
        public string d_year { get; set; }
        public string d_desc { get; set; }
        public bool? deleteFlag { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string showId { get; set; }
    }
}