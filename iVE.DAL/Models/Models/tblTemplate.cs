using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models {
    [Table ("tblTemplate")]
    public class tblTemplate {
        public string TemplateID { get; set; }
        public string showId { get; set; }
        public string ExhID { get; set; }
        public string Image { get; set; }
        public string BoothID { get; set; }
        public string TemplateConfig { get; set; }
    }
}