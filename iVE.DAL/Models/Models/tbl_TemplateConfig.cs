using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tbl_TemplateConfig")]
    public class tbl_TemplateConfig
    {
        public int Id { get; set; }
        public string showId { get; set; }
        public string title { get; set; }
        public string templateType { get; set; }
        public string url { get; set; }
        public string videoUrl { get; set; }
        public string mediaType { get; set; }
        public string blockUserType { get; set; }
        public string category { get; set; }
        public int sequenceNo { get; set; }
        public string configData { get; set; }
        public bool deleteFlag { get; set; }
        public string status { get; set; }
    }
}