using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models {
    [Table ("tblExhibitor")]
    public class tblExhibitor {
        public string showId { get; set; }
        public string ExhID { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
        public string ExhType { get; set; }
        public string ExhCategory { get; set; }
        public string HallNo { get; set; }
        public string BoothNo { get; set; }
        public string BoothTemplate { get; set; }
        public string Logo { get; set; }
        public string BoothBanner { get; set; }
        public string SocialMedia { get; set; }
        public string CompanyVideo { get; set; }
        public string CompanyDocument { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ChatContactMembers { get; set; }
        public int? SequenceNo { get; set; }
        public int? additionalBadges { get; set; }
        public int? GroupChatStatus { get; set; }

    }
}