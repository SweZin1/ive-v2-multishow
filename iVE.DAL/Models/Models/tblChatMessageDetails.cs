using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tbl_ChatMessageDetails")]
    public class tblChatMessageDetails
    {
        public int Id { get; set; }
        public string messageId { get; set; }
        public string conversationId { get; set; }
        public string receiverType { get; set; }
        public string category { get; set; }
        public string type { get; set; }
        public string text { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string senderDetail { get; set; }
        public string receiverDetail { get; set; }
        public Int64? sentAt { get; set; }
        public Int64? deliveredAt { get; set; }
        public Int64? readAt { get; set; }
        public Int64? updatedAt { get; set; }
        public string senderName { get; set; }
        public string receiverName { get; set; }
        public DateTime? sentDate { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? updatedDate { get; set; }
    }
}