using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("Administrator")]
    public class tbl_Administrator
    {
        public string showId { get; set; }
        public int admin_id { get; set; }
        public string admin_username { get; set; }
        public string admin_password { get; set; }
        public string admin_name { get; set; }
        public string admin_role { get; set; }
        public string admin_remark { get; set; }
        public bool? admin_isdeleted { get; set; }
        public string admin_profilepic { get; set; }
        public DateTime? admin_createddate { get; set; }
        public string ChatID { get; set; }
        public string admin_email { get; set; }
        public string admin_mobile { get; set; }
        public bool? IsSuperAdmin { get; set; }
        public DateTime? admin_updateddate { get; set; }

    }
}