using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tbl_Appointments")]
    public class tbl_Appointments
    {
        public string AppointmentID { get; set; }
        public string showId { get; set; }
        public string TimeSlotID { get; set; }
        public string AppType { get; set; }
        public string BookingType { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public DateTime ConfirmDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool deleteFlag { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}