using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
namespace iVE.DAL.Models {
    [Table ("tblSponsorCategory")]
    public class tblSponsorCategory {
        public string showId { get; set; }
        public string sponsorCat_ID { get; set; }
        public string sponsorCat_name { get; set; }
        public int? sponsorCat_seq { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime? updatedDate { get; set; }
        public DateTime? createdDate { get; set; }

    }
}