using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblShow")]
    public class tblShow
    {
        public string showId { get; set; }
        public string showName { get; set; }
        public string showTitle { get; set; }
        public string showPrefix { get; set; }
        public string logo { get; set; }
        public DateTime showStartDate { get; set; }
        public DateTime showEndDate { get; set; }
        public string country { get; set; }
        public string category { get; set; }
        public string route { get; set; }
        public string timeZone { get; set; }
        public string RegLink { get; set; }
        public string showConfigUrl { get; set; }
        public string conferenceTemplate { get; set; }
        public string conferenceStaticUrl { get; set; }
        public int? chatId { get; set; }
        public string Status { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime updatedDate { get; set; }
    }
}