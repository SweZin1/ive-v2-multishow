using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblLeaderBoardEntitlePoints")]
    public class tblLeaderBoardEntitlePoints
    {
        public int PointKey { get; set; }
        public string LeaderBoardCat { get; set; }
        public string showId { get; set; }
        public int Points { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool deleteFlag { get; set; }
    }
}