using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models {
    [Table ("tblAudit")]
    public class tblAudit {
        public string showId { get; set; }
        public int Id { get; set; }
        public string LogType { get; set; }
        public string Message { get; set; }
        public DateTime LogDateTime { get; set; }
        public string UserId { get; set; }
        public string AlternativeId { get; set; }
        public string IPAddress { get; set; }
        public string FunctionName { get; set; }

    }
}