using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblMCQScoreBoard")]
    public class tblMCQScoreBoard
    {
        public string ParticipantID { get; set; }
        public int TotalPoints { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool deleteFlag { get; set; }
    }
}