using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models {
    [Table ("tblProductCommant")]
    public class tblProductCommant {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ExhId { get; set; }
        public string ProductId { get; set; }
        public string Command { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int Status { get; set; }
    }
}