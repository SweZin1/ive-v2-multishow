using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblHall")]
    public class tblHall
    {
        public string HallId { get; set; }
        public string showId { get; set; }
        public string HallName { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}