using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblBooth")]
    public class tblBooth
    {
        public string ID { get; set; }
        public string BoothNo { get; set; }
        public string HallId { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}