using System.Collections.Generic;
namespace iVE.DAL.Models {
    public class Panelist {

        public List<Panelists> panelists { get; set; }
    }

    public class Panelists {
        public string name { get; set; }
        public string email { get; set; }
    }
}