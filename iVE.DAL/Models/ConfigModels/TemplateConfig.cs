namespace iVE.DAL.Models {
    public class TemplateConfig {
        public string name { get; set; }
        public string file_type { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public string click_url_action { get; set; }
        public string refId { get; set; }
    }
}