using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table ("tbl_TempAccount")]
    public class TempAccount
    {
        [DisplayName("ShowId")]
        public string showId { get; set; }
        public int Id { get; set; }

        [DisplayName("Full Name")]
        public string a_fullname { get; set; }

        [DisplayName("Account Type")]
        public string a_type { get; set; }

        [DisplayName("First Name")]
        public string a_fname { get; set; }
        [DisplayName("Last Name")]
        public string a_lname { get; set; }
        [DisplayName("Designation")]
        public string a_designation { get; set; }
        [DisplayName("Email")]
        public string a_email { get; set; }
        [DisplayName("Company")]
        public string a_company { get; set; }
        [DisplayName("Address")]
        public string a_addressOfc { get; set; }
        [DisplayName("Country")]
        public string a_country { get; set; }
        [DisplayName("Mobile")]
        public string a_Mobile { get; set; }
        [DisplayName("LoginName")]
        public string a_LoginName { get; set; }
        [DisplayName("Password")]
        public string a_Password { get; set; }
        [DisplayName("Speaker Bio")]
        public string biography { get; set; }
        [DisplayName("RegID")]
        public string RegPortalID { get; set; }
        [DisplayName("Signed Date")]
        public string Signed_Date { get; set; }
        public string remark { get; set; }
        public bool status { get; set; }
        public int? totalCount { get; set; }
        public string countryCode { get; set; }
        public string templateId { get; set; }
    }
}