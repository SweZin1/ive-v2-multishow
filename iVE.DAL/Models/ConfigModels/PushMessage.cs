using Newtonsoft.Json.Linq;
namespace iVE.DAL.Models
{
    public class PushMessage
    {
        public string[] to { get; set; }
        public Notification notification { get; set; }
    }
}