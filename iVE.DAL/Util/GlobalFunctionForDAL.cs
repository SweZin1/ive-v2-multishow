using Microsoft.Extensions.Configuration;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using iVE.DAL.Models;
namespace iVE.DAL.Util
{
    public static class GlobalFunctionForDAL
    {
        public static string GenerateSalthKey(string password)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string salt = Configuration.GetSection("SaltKey").Value;
            return DataAccess.SaltedHash.ComputeHash(salt, password);
        }

        public static void WriteSystemLog(string logType, string apiName, string logMessage, string userId)
        {
            if (logType == LogType.Info)
            {
                Log.Information("In {APIName} , {LogMessage} by {UserId}", apiName, logMessage, userId);
            }

            if (logType == LogType.Error)
            {
                Log.Error("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Fatal)
            {
                Log.Fatal("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Debug)
            {
                Log.Debug("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Warn)
            {
                Log.Warning("In {APIName} , {LogMessage}", apiName, logMessage);
            }
        }

    }
}