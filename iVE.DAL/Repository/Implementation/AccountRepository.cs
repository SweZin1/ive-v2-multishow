using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using System.Globalization;

using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation
{
    public class AccountRepository : RepositoryBase<tbl_Account>, IAccountRepository
    {
        private string showId = LoginData.showId;
        public AccountRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
        public void SaveNewContactPerson(dynamic obj, string exhId, string contactId)
        {
            try
            {
                tbl_Account contactPerson = new tbl_Account();
                contactPerson.a_ID = contactId;
                contactPerson.a_fullname = obj.name;
                contactPerson.ExhId = exhId;
                contactPerson.showId = showId;
                contactPerson.a_type = obj.type;
                contactPerson.a_addressHome = obj.address;
                contactPerson.a_email = obj.email;
                contactPerson.a_designation = obj.designation;
                contactPerson.a_Tel = obj.tel;
                contactPerson.a_LoginName = obj.loginName;
                string password = obj.password;
                contactPerson.a_Password = GlobalFunctionForDAL.GenerateSalthKey(password);

                contactPerson.a_Mobile = obj.mobile;
                contactPerson.deleteFlag = false;
                contactPerson.lang = 1;
                contactPerson.UpdatedDate = DateTime.Now;
                contactPerson.Fax = obj.fax;
                contactPerson.LoginBlocker = obj.LoginBlocker == null && obj.LoginBlocker == "" ? null : obj.LoginBlocker;
                contactPerson.ChatActiveStatus = 1;
                contactPerson.UserLevelId = obj.userLevelId;
                RepositoryContext.Accounts.Add(contactPerson);
                RepositoryContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }

        }

        public void UpdateContactPerson(dynamic obj, tbl_Account contactPerson)
        {
            contactPerson.showId = showId;
            contactPerson.a_fullname = obj.name;
            contactPerson.a_addressHome = obj.address;
            contactPerson.a_email = obj.email;
            contactPerson.a_Tel = obj.tel;
            contactPerson.a_LoginName = obj.loginName;
            contactPerson.a_designation = obj.designation;
            // string password = obj.password;
            // contactPerson.a_Password = GlobalFunctionForDAL.GenerateSalthKey(password);
            contactPerson.Fax = obj.fax;
            contactPerson.a_Mobile = obj.mobile;
            contactPerson.LoginBlocker = obj.LoginBlocker == null && obj.LoginBlocker == "" ? null : obj.LoginBlocker;
            contactPerson.ModifiedDate = DateTime.Now;
            contactPerson.UserLevelId = obj.userLevelId;
            RepositoryContext.Accounts.Update(contactPerson);
            RepositoryContext.SaveChanges();
        }


        public void DeleteContactPerson(string exhId, string contactId)
        {
            tbl_Account contactPerson = (from c in RepositoryContext.Accounts
                                         where c.showId == showId && c.deleteFlag == false && c.a_ID == contactId && c.ExhId == exhId
                                         select c).FirstOrDefault();
            if (contactPerson != null)
            {
                contactPerson.deleteFlag = true;
                RepositoryContext.Accounts.Update(contactPerson);
                RepositoryContext.SaveChanges();
            }
        }

        public dynamic GetVisitorById(string Id)
        {
            var main = (from c in RepositoryContext.Accounts
                        where c.showId == showId && c.deleteFlag == false
                        && c.a_ID == Id
                        select c).FirstOrDefault();
            return main;

        }

        public dynamic GetVisitedExhListByVisitorId(string Id)
        {
            var main = (from c in RepositoryContext.Audits
                        join b in RepositoryContext.Exhibitors on c.AlternativeId equals b.ExhID
                        orderby b.CompanyName
                        where c.showId == showId && b.showId == showId
                        && c.UserId == Id && c.LogType == AuditLogType.ViewPosterDescription
                        && c.showId == showId
                        select new { b, c.UserId }).Distinct().ToList();
            return main;
        }

        public dynamic GetFavouriteExhibitorsByVisitorId(string Id)
        {
            var main = (from uf in RepositoryContext.UsersFavourites
                        join ex in RepositoryContext.Exhibitors on uf.FavItemId equals ex.ExhID
                        orderby ex.CompanyName
                        where ex.Status == ExhibitorStatus.Published

                        && uf.Status == 1 && uf.UserId == Id
                        && uf.ItemType == FavouriteItem.Exhibitor
                        && uf.showId == showId && ex.showId == showId
                        select new
                        {
                            uf.UserId,
                            ex.ExhID,
                            ex.CompanyName,
                            ex.Description,
                            ex.ExhType,
                            ex.ExhCategory,
                            ex.Country,
                            ex.HallNo,
                            ex.BoothNo,
                            ex.BoothTemplate
                        }).AsNoTracking().Distinct().ToList();
            return main;
        }
        public dynamic GetFavouriteProductsByVisitorId(string Id)
        {
            var main = (from uf in RepositoryContext.UsersFavourites
                        join p in RepositoryContext.Products on uf.FavItemId equals p.p_ID
                        join exh in RepositoryContext.Exhibitors on p.ExhID equals exh.ExhID
                        orderby p.p_name
                        where uf.showId == showId && p.showId == showId
                        && p.deleteFlag == false && uf.Status == 1 && exh.Status == ExhibitorStatus.Published
                        && uf.ItemType == FavouriteItem.Product && uf.UserId == Id
                        select new
                        {
                            exh.CompanyName,
                            uf.UserId,
                            p.ExhID,
                            p.p_ID,
                            p.p_name,
                            p.Description
                        }).AsNoTracking().Distinct().ToList();
            return main;
        }

        #region Visitor Set Up
        public void SaveNewVisitor(dynamic obj, string visitorId)
        {
            try
            {
                tbl_Account visitorObj = new tbl_Account();
                visitorObj.showId = showId;
                visitorObj.a_ID = visitorId;
                visitorObj.a_fullname = obj.name;
                visitorObj.a_sal = obj.salutation;
                visitorObj.a_fname = obj.firstName;
                visitorObj.a_lname = obj.lastName;
                visitorObj.a_designation = obj.designation;
                visitorObj.a_type = obj.userType;
                visitorObj.a_addressHome = obj.addressHome;
                visitorObj.a_addressOfc = obj.addressOffice;
                visitorObj.a_company = obj.company;
                visitorObj.a_country = obj.country;
                visitorObj.a_email = obj.email;
                visitorObj.a_Tel = obj.tel;
                visitorObj.a_LoginName = obj.loginName;
                visitorObj.Biography = obj.biography;
                string password = obj.password;
                visitorObj.a_Password = GlobalFunctionForDAL.GenerateSalthKey(password);
                visitorObj.a_Mobile = obj.mobile;
                visitorObj.UserLevelId = obj.userLevelId;
                visitorObj.deleteFlag = false;
                visitorObj.lang = 1;
                visitorObj.UpdatedDate = DateTime.Now;
                visitorObj.Fax = obj.fax;

                RepositoryContext.Accounts.Add(visitorObj);
                RepositoryContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }
        public void UpdateVisitor(dynamic obj, tbl_Account visitorObj)
        {
            string usertype = obj.userType;
            visitorObj.showId = showId;
            visitorObj.a_fullname = obj.name;
            visitorObj.a_fname = obj.firstName;
            visitorObj.a_lname = obj.lastName;
            visitorObj.a_designation = obj.designation;
            visitorObj.a_sal = obj.salutation;
            visitorObj.a_LoginName = obj.loginName;
            visitorObj.a_addressHome = obj.addressHome;
            visitorObj.a_addressOfc = obj.addressOffice;
            visitorObj.a_email = obj.email;
            visitorObj.a_Tel = obj.tel;
            visitorObj.a_Mobile = obj.mobile;
            visitorObj.a_company = obj.company;
            visitorObj.Fax = obj.fax;
            visitorObj.a_country = obj.country;
            visitorObj.Biography = obj.biography;
            visitorObj.a_type = obj.userType;
            visitorObj.UserLevelId = obj.userLevelId;
            visitorObj.ModifiedDate = DateTime.Now;
            if (!string.IsNullOrEmpty(usertype) && (usertype == UserType.Speaker || usertype == UserType.Delegate
            || usertype == UserType.Visitor || usertype == UserType.FreeDelegate))
            {
                visitorObj.a_type = usertype;
            }
            RepositoryContext.Accounts.Update(visitorObj);
            RepositoryContext.SaveChanges();
        }
        public void DeleteVisitor(tbl_Account visitorObj)
        {
            visitorObj.deleteFlag = true;
            visitorObj.ModifiedDate = DateTime.Now;
            RepositoryContext.Accounts.Update(visitorObj);
            RepositoryContext.SaveChanges();
        }
        #endregion        

        #region VisitedExhibitor
        public dynamic GetVisitedExhibitor(dynamic obj, string sortField, string sortBy, string userId)
        {
            dynamic result;
            var mainQuery = (from e in RepositoryContext.Exhibitors
                             join cs in (from c in RepositoryContext.Countries
                                         where c.deleteFlag == false
                                         select new { c.ISO3Digit, c.CountryName }) on e.Country equals cs.ISO3Digit into cls
                             from ctl in cls.DefaultIfEmpty()
                             join au in RepositoryContext.Audits on e.ExhID equals au.AlternativeId
                             where e.showId == showId && au.showId == showId && au.UserId == userId && au.showId == showId
                             && au.LogType == AuditLogType.VisitExhibitor
                             && e.Status == ExhibitorStatus.Published
                             select new
                             {
                                 e.ExhID,
                                 e.CompanyName,
                                 description = e.Description,
                                 country = ctl.CountryName
                             }).AsNoTracking().Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForExhibitorlist(mainQuery, obj);
                }
            }
            var objTotal = mainQuery.AsNoTracking().Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.AsNoTracking().OrderBy(sortList);
            mainQuery = PaginationSessionForExhibitorlist(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();
            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }
        private dynamic FilterSessionForExhibitorlist(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhID = default(string),
                CompanyName = default(string),
                description = default(string),
                country = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "CompanyName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.CompanyName.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationSessionForExhibitorlist(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhID = default(string),
                CompanyName = default(string),
                description = default(string),
                country = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.ExhID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.ExhID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }
        #endregion

        /*8-7-2020*/
        public dynamic GetAllContactPersonsWithStatusByExhId(string exhId)
        {
            var main = (from a in RepositoryContext.Accounts
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName })
                        on a.a_country equals ct.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where a.showId == showId && a.deleteFlag == false && a.ExhId == exhId
                        select new
                        {
                            a_ID = a.a_ID,
                            ExhId = a.ExhId,
                            a_fullname = a.a_fullname,
                            a_type = a.a_type,
                            a_sal = a.a_sal,
                            a_fname = a.a_fname,
                            a_lname = a.a_lname,
                            a_designation = a.a_designation,
                            a_email = a.a_email,
                            a_company = a.a_company,
                            a_addressOfc = a.a_addressOfc,
                            a_addressHome = a.a_addressHome,
                            a_country = ctl.CountryName,
                            a_Tel = a.a_Tel,
                            a_Mobile = a.a_Mobile,
                            a_LoginName = a.a_LoginName,
                            a_Password = a.a_Password,
                            a_profilepic = a.a_profilepic,
                            a_isActivated = a.a_isActivated,
                            lang = a.lang,
                            deleteFlag = a.deleteFlag,
                            UpdatedDate = a.UpdatedDate,
                            a_AttendDay = a.a_AttendDay,
                            NFCID = a.NFCID,
                            RegPortalID = a.RegPortalID,
                            ExhID_iScan = a.ExhID_iScan,
                            UserType = a.UserType,
                            AdminId = a.AdminId,
                            Biography = a.Biography,
                            ProfilePic = a.ProfilePic,
                            VCard = a.VCard,
                            ShareVCard = a.ShareVCard,
                            DeviceId = a.DeviceId,
                            MobileDeviceId = a.MobileDeviceId,
                            Fax = a.Fax,
                            Status = (from e in RepositoryContext.Exhibitors where e.showId == showId && e.ChatContactMembers.Contains(a.a_ID) && a.deleteFlag == false select a).Count() > 0 ? true : false,
                        }).AsNoTracking().ToList();
            return main;
        }

        public dynamic GetVisitor(dynamic obj, string sortField, string sortBy, bool isDefaultSort)
        {
            dynamic result;
            var mainQuery = (from c in RepositoryContext.Accounts
                             join ct in (from cs in RepositoryContext.Countries
                                         where cs.deleteFlag == false
                                         select new { cs.ISO3Digit, cs.CountryName }) on c.a_country equals ct.ISO3Digit into cl
                             from ctl in cl.DefaultIfEmpty()
                             where c.deleteFlag == false && c.showId == showId
                             && (c.a_type == UserType.Visitor || c.a_type == UserType.Delegate
                             || c.a_type == UserType.Speaker || c.a_type == UserType.FreeDelegate)
                             select new
                             {
                                 fullName = c.a_fullname,
                                 //email = c.a_email,
                                 designation = c.a_designation,
                                 mobile = c.a_Mobile,
                                 address = c.a_addressHome,
                                 country = ctl.CountryName,
                                 id = c.a_ID,
                                 userType = c.a_type == UserType.Visitor ? "Visitor" :
                                 c.a_type == UserType.Delegate ? "Delegate" :
                                 c.a_type == UserType.FreeDelegate ? "Free Delegate" :
                                 c.a_type == UserType.Speaker ? "Speaker" : "",
                             }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForVistiorlist(mainQuery, obj);
                }
            }

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);

            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForVisitorlist(mainQuery, obj);
            var objResult = mainQuery.AsNoTracking().ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        private dynamic FilterSessionForVistiorlist(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                fullName = default(string),
                //email = default(string),
                designation = default(string),
                mobile = default(string),
                address = default(string),
                country = default(string),
                id = default(string),
                userType = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "fullName")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.fullName.ToLower().Contains(Name));
                            }
                        }
                        if (filterName == "userType")
                        {
                            string value = filterValue;
                            if (!String.IsNullOrEmpty(value))
                            {
                                value = value.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.userType.ToLower() == value);
                            }
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForVisitorlist(dynamic mainQuery, dynamic obj)
        {

            var tmpQuery = Enumerable.Repeat(new
            {
                fullName = default(string),
                //email = default(string),
                designation = default(string),
                mobile = default(string),
                address = default(string),
                country = default(string),
                id = default(string),
                userType = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public dynamic GetAllExhibitorProductsByVisitorId(dynamic obj, string sortField, string sortBy, bool isDefaultSort, string exhId, string vId)
        {
            dynamic result;
            var main = (from ex in RepositoryContext.Exhibitors
                        join p in RepositoryContext.Products on ex.ExhID equals p.ExhID
                        where p.showId == showId && ex.showId == showId
                        && p.deleteFlag == false && ex.ExhID == exhId
                        select new
                        {
                            p.p_ID,
                            p.p_name,
                            p.Description,
                            p_seq = p.sororder
                        }).Distinct();

            var visitor = (from v in RepositoryContext.Accounts
                           where v.showId == showId && v.deleteFlag == false && v.a_ID == vId
                           select
                           new
                           {
                               v.a_ID
                           }).Distinct();

            var mainQuery = (from productlist in main
                             from visitorlist in visitor
                             select new
                             {
                                 productlist.p_ID,
                                 productlist.p_name,
                                 productlist.Description,
                                 productlist.p_seq,
                                 Status = (from uf in RepositoryContext.UsersFavourites
                                           where uf.showId == showId && uf.UserId == visitorlist.a_ID 
                                           && uf.FavItemId == productlist.p_ID && uf.ItemType == FavouriteItem.Product
                                           && uf.Status == 1
                                           select uf).Count() > 0 ? "1" : "0",
                                 visitorlist.a_ID

                             }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForExhibitorProducts(mainQuery, obj);
                }
            }
            var sortList = new System.Collections.Generic.List<SortModel>();

            var seqNoSort = new SortModel();
            seqNoSort.ColId = "p_seq";
            seqNoSort.Sort = "asc";
            sortList.Add(seqNoSort);

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;
            sortList.Add(objSort);

            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForExhibitorProducts(mainQuery, obj);
            var objResult = mainQuery.ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        private dynamic FilterSessionForExhibitorProducts(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                P_ID = default(string),
                p_name = default(string),
                Description = default(string),
                p_seq = default(int?),
                Status = default(string),
                a_ID = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "p_name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.p_name.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForExhibitorProducts(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                p_ID = default(string),
                p_name = default(string),
                Description = default(string),
                p_seq = default(int?),
                Status = default(string),
                a_ID = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.p_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.p_ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public dynamic GetFavouriteExhStatusByVisitor(string vId, string exhId)
        {
            var main = (from uf in RepositoryContext.UsersFavourites
                        where uf.showId == showId && uf.UserId == vId 
                        && uf.FavItemId == exhId && uf.ItemType == FavouriteItem.Exhibitor
                        select new { uf.Status }).AsNoTracking().SingleOrDefault();
            return main;
        }
        public void ChangePassword(string pwd, tbl_Account contactPerson)
        {
            string password = pwd;
            contactPerson.a_Password = GlobalFunctionForDAL.GenerateSalthKey(password);
            contactPerson.ModifiedDate = DateTime.Now;
            RepositoryContext.Accounts.Update(contactPerson);
            RepositoryContext.SaveChanges();
        }

        public dynamic GetVisitorsByCountry()
        {
            var main = (from v in RepositoryContext.Accounts
                        join vc in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on v.a_country equals vc.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where v.showId == showId && v.deleteFlag == false && v.a_type == UserType.Visitor
                        select new
                        {
                            v.a_ID,
                            Country = ctl.CountryName.ToUpper()
                        }).Distinct().ToList().GroupBy(x => x.Country, (key, g) => new { name = key, value = g.Count() });
            return main;
        }

        public bool CheckBadgeEntitlement(string exhId)
        {
            bool result = true; int totalEntitleBadges = 0; int entBadges = 0; int addBadges = 0;
            int contactPersonCount = (from c in RepositoryContext.Accounts
                                      where c.showId == showId && c.deleteFlag == false && c.ExhId == exhId && c.a_type == ContactPersonType.ContactPerson
                                      select c.a_ID).Count();

            var additional = (from c in RepositoryContext.Exhibitors
                              where c.showId == showId && c.ExhID == exhId
                              select new
                              {
                                  add = (c.additionalBadges == null && c.additionalBadges == 0) ? 0 : c.additionalBadges
                              }).FirstOrDefault();

            var obj = (from e in RepositoryContext.Exhibitors
                       join t in RepositoryContext.ExhibitorTypes on e.ExhType equals t.ID.ToString()
                       where e.showId == showId && t.showId == showId && e.ExhID == exhId && t.deleteFlag == false
                       select new
                       {
                           ent = (t.entitleBadges == null && t.entitleBadges == 0) ? 0 : t.entitleBadges
                       }).FirstOrDefault();
            if (obj != null)
            {
                entBadges = Convert.ToInt16(obj.ent);
            }
            if (additional != null)
            {
                addBadges = Convert.ToInt16(additional.add);
            }
            totalEntitleBadges = entBadges + addBadges;
            if (contactPersonCount >= totalEntitleBadges)
            {
                result = false;
            }
            return result;
        }

        public dynamic GetActiveLoginUsers(dynamic obj, bool isexport)
        {
            dynamic result = null;
            string sortField = null; string sortBy = "";

            var main = (from au in RepositoryContext.Audits
                        join a in RepositoryContext.Accounts on au.UserId equals a.a_ID
                        join vc in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on a.a_country equals vc.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where au.showId == showId && a.showId == showId && a.deleteFlag == false
                        select new
                        {
                            name = a.a_fullname,
                            designation = a.a_designation,
                            company = a.a_company,
                            country = ctl.CountryName.ToUpper(),
                            createdDate = au.LogDateTime

                        }).Distinct();

            if (obj.filter.filters != null)
            {
                main = FilterSessionForActiveLoginUsers(main, obj);
            }

            var mainQuery = (from d in main select new { d.name, d.designation, d.company, d.country }).Distinct();

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "name";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            if (isexport)
            {
                return mainQuery;
            }
            mainQuery = PaginationSessionForActiveLoginUsers(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();
            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved." };
            return result;
        }

        private dynamic FilterSessionForActiveLoginUsers(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                name = default(string),
                designation = default(string),
                company = default(string),
                country = default(string),
                createdDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                DateTime paraDate = new DateTime();
                string fromtime = "";
                string totime = "";

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "Name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.name.ToLower().Contains(Name));

                        }
                    }
                }

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (filterName == "fromDate" && !string.IsNullOrEmpty(filterValue))
                    {
                        paraDate = Convert.ToDateTime(filterValue);
                    }
                    else if (filterName == "fromTime" && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterValue.Length == 7)
                        {
                            filterValue = "0" + filterValue;
                        }
                        fromtime = filterValue;
                    }
                    else if (filterName == "toTime" && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterValue.Length == 7)
                        {
                            filterValue = "0" + filterValue;
                        }
                        totime = filterValue;
                    }
                }

                TimeSpan ftspan;
                ftspan = DateTime.ParseExact(fromtime, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                TimeSpan ttspan;
                ttspan = DateTime.ParseExact(totime, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;

                DateTime fromDate = paraDate.Add(ftspan);
                DateTime toDate = paraDate.Add(ttspan);

                tmpQuery = tmpQuery.Where(x => x.createdDate >= fromDate && x.createdDate <= toDate);
                //.Select(x => new {x.company,x.name,x.designation}).Distinct();    
                //tmpQuery = tmpQuery.Select(x => new { x.company,x.name,x.designation}).Distinct();        
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForActiveLoginUsers(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                name = default(string),
                designation = default(string),
                company = default(string),
                country = default(string),
                //createdDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.name);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.name) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public dynamic GetContactPersonDetail(string exhId, string contactId)
        {
            var exhibitor = (from e in RepositoryContext.Exhibitors
                             where e.showId == showId && e.ExhID == exhId
                             select e).FirstOrDefault();
            bool addToRepresentative = false;
            if (exhibitor != null)
            {
                string ChatContactMembers = exhibitor.ChatContactMembers == null ? "" : exhibitor.ChatContactMembers;
                addToRepresentative = ChatContactMembers.Contains(contactId);
            }

            var main = (from a in RepositoryContext.Accounts
                        where a.showId == showId && a.deleteFlag == false && a.ExhId == exhId && a.a_ID == contactId
                        select new
                        {
                            a.ExhId,
                            a.a_ID,
                            a.a_fullname,
                            a.a_designation,
                            a.a_LoginName,
                            a.a_email,
                            a.a_Tel,
                            a.Fax,
                            a.a_Mobile,
                            a.a_type,
                            a.a_country,
                            a.a_company,
                            a.a_addressHome,
                            a.UserLevelId,
                            a.ChatActiveStatus,
                            a.ChatID,
                            addToRepresentative = addToRepresentative
                        }).AsNoTracking().FirstOrDefault();
            return main;
        }

        public dynamic GetContactPersonListbyExhId(string exhId)
        {
            var main = (from a in RepositoryContext.Accounts
                        where a.showId == showId && a.deleteFlag == false && a.ExhId == exhId
                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            a.a_LoginName,
                            a.a_company,
                            a.a_country,
                            a.a_email,
                            a.a_designation,
                            a.a_Tel,
                            a.Fax,
                            a.a_Mobile,
                            a.a_type,
                            a.a_addressHome,
                            a.ChatActiveStatus,
                            a.ChatID
                        }).AsNoTracking().ToList();
            return main;
        }

        public dynamic GetDays()
        {
            var main = (from d in RepositoryContext.Days
                        where d.deleteFlag == false
                        select d.d_date).Distinct().ToList();
            return main;
        }

        public void updateProfilePicUrl(tbl_Account account, string url)
        {
            account.ProfilePic = url;
            account.ModifiedDate = DateTime.Now;
            RepositoryContext.Accounts.Update(account);
            RepositoryContext.SaveChanges();
        }

        public dynamic GetSalutation()
        {
            dynamic main = null;
            main = (from sal in RepositoryContext.Salutations
                    where sal.deleteFlag == false
                    select new { sal.Id, sal.sal_Name }).AsNoTracking().Distinct().ToList();
            return main;
        }

        public void UpdateTempAccount(List<TempAccount> lst)
        {
            using (var transaction = RepositoryContext.Database.BeginTransaction())
            {
                try
                {
                    RepositoryContext.tempAccounts.AddRange(lst);
                    RepositoryContext.SaveChanges();
                    RepositoryContext.Database.ExecuteSqlRaw("updateTempAccounts");
                    transaction.Commit();
                }
                catch (Exception)
                {
                    //if something is wrong, all calls are rolled back
                    transaction.Rollback();
                }
            }

        }

        public dynamic getTempAccounts(dynamic obj)
        {
            //dynamic main = null; 
            int? totalCount = 0;
            string filterName = ""; string filterValue = "";
            string sortCol = null; string sortBy = "";
            int pageNo = 0; int pageSize = 10;

            //Sort
            if (obj != null && obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortBy = sortBy.ToUpper();
                sortCol = sort.field.Value;
            }
            //Filter 
            if (obj != null && obj.filter != null)
            {
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    filterName = obj.filter.filters[i].field;
                    filterValue = obj.filter.filters[i].value;
                }
            }
            if (obj != null)
            {
                //Pagination
                pageNo = obj.skip;
                pageSize = obj.take;
            }
            var spParams = new object[] { filterName, filterValue, pageNo, pageSize, sortCol, sortBy };
            List<TempAccount> main = RepositoryContext.tempAccounts.FromSqlRaw<TempAccount>("getTempAccounts {0},{1},{2},{3},{4},{5}"
            , spParams).AsNoTracking().ToList();

            int validCount = 0; int unValidCount = 0; string templateId = string.Empty;
            if (main.Count > 0)
            {
                totalCount = main[0].totalCount;
                templateId = main[0].templateId;
                validCount = main.Where(x => x.status == true).Select(x => x.Id).Distinct().Count();
                unValidCount = main.Where(x => x.status == false).Select(x => x.Id).Distinct().Count();
                return new
                {
                    data = main,
                    dataFoundRowsCount = totalCount,
                    ValidCount = validCount,
                    UnValidCount = unValidCount,
                    success = true,
                    templateId = templateId,
                    message = "Successfully retrieved."
                };
            }
            return new { data = main, dataFoundRowsCount = totalCount, success = true, message = "No record to display." }; ;
        }

        public bool UpdateAccounts(List<tbl_Account> lst)
        {
            using (var transaction = RepositoryContext.Database.BeginTransaction())
            {
                try
                {
                    RepositoryContext.Accounts.AddRange(lst);
                    RepositoryContext.SaveChanges();
                    //RepositoryContext.Database.ExecuteSqlRaw("truncate table [tbl_TempAccount]");
                    transaction.Commit();
                    return true;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }




    }
}