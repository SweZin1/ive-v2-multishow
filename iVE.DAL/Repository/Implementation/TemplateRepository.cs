using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.CompilerServices;
using System;
namespace iVE.DAL.Repository.Implementation
{
    public class TemplateRepository : RepositoryBase<tblTemplate>, ITemplateRepository
    {
        private string showId = LoginData.showId;
        public TemplateRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetAllBooth(string exhId)
        {
            var main = (from t in RepositoryContext.Templates
                        join e in RepositoryContext.Exhibitors on t.ExhID equals e.ExhID
                        where t.showId == showId && e.showId == showId && t.ExhID == exhId
                        select new
                        {
                            t.Image,
                            templateConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>>(t.TemplateConfig),
                            t.TemplateID,
                            t.BoothID,
                            contactPersonStatus = e.ChatContactMembers != null && e.ChatContactMembers != "" ? true : false
                        }).ToList();
            return main;
        }

        public dynamic GetBoothById(string exhId, string boothId)
        {
            var main = (from tem in RepositoryContext.Templates
                        where tem.showId == showId && tem.ExhID == exhId && tem.TemplateID == boothId
                        select new
                        {
                            tem.Image,
                            tem.ExhID,
                            tem.TemplateConfig//templateConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>>(tem.TemplateConfig),

                        }).FirstOrDefault();
            return main;
        }

        public dynamic GetBoothByTemplateId(string templateId)
        {
            var main = (from tem in RepositoryContext.Templates
                        join e in RepositoryContext.Exhibitors on tem.ExhID equals e.ExhID
                        where tem.showId == showId && tem.TemplateID == templateId
                        select new
                        {
                            tem.BoothID,
                            tem.ExhID,
                            tem.Image,
                            tem.TemplateConfig,//templateConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>>(tem.TemplateConfig),
                            contactPersonStatus = e.ChatContactMembers != null && e.ChatContactMembers != "" ? true : false,
                            e.Status
                        }).AsNoTracking().FirstOrDefault();
            return main;
        }

        public dynamic UpdateBoothById(tblTemplate template, dynamic obj)
        {

            template.BoothID = obj.Id;
            int boothTemplateId = Convert.ToInt32(obj.Id);
            tbl_TemplateConfig temConfig = RepositoryContext.tbl_TemplateConfigs.Where(x => x.Id == boothTemplateId).FirstOrDefault();
            if (temConfig != null)
            {
                template.Image = temConfig.url;
                template.TemplateConfig = temConfig.configData;
            }
            RepositoryContext.Templates.Update(template);
            RepositoryContext.SaveChanges();
            return template.TemplateID;
        }
        public dynamic CreateBoothById(dynamic obj, string templateId, string exhId)
        {
            int boothTemplateId = Convert.ToInt32(obj.Id);
            tblTemplate template = new tblTemplate();
            template.TemplateID = templateId;
            template.ExhID = exhId;
            template.showId = showId;
            template.BoothID = obj.Id;

            tbl_TemplateConfig temConfig = RepositoryContext.tbl_TemplateConfigs.Where(x => x.showId == showId && x.Id == boothTemplateId).FirstOrDefault();
            if (temConfig != null)
            {
                template.Image = temConfig.url;
                template.TemplateConfig = temConfig.configData;
            }

            RepositoryContext.Templates.Add(template);
            RepositoryContext.SaveChanges();
            return template.TemplateID;
        }
        public void UpdateBoothConfigByName(string boothTemplateId, dynamic obj)
        {
            tblTemplate template = RepositoryContext.Templates.Where(x => x.showId == showId && x.TemplateID == boothTemplateId).Select(x => x).FirstOrDefault();
            if (template != null)
            {
                template.TemplateConfig = obj.configData;
                RepositoryContext.Templates.Update(template);
                RepositoryContext.SaveChanges();
            }
        }
        public void UpdateTemplateImageByExhID(string exhId, tblTemplate template, dynamic obj)
        {
            template.Image = obj.url;
            RepositoryContext.Templates.Update(template);
            RepositoryContext.SaveChanges();
        }
    }
}