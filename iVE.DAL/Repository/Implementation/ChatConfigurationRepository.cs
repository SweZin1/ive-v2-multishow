using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using iVE.DAL.Util;
using System;
using System.Collections.Generic;

namespace iVE.DAL.Repository.Implementation
{
    public class ChatConfigurationRepository : RepositoryBase<tblChatConfiguration>, IChatConfigurationRepository
    {
        private string showId = LoginData.showId;
        public ChatConfigurationRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetChatConfig(int id)
        {
            return (from s in RepositoryContext.ChatConfigurations
                    where s.Id == id && s.deleteFlag == false
                    select new
                    {
                        id = s.Id,
                        name = s.name,
                        chatAppId = s.chatAppId,
                        region = s.region,
                        apiKey = s.apiKey,
                        widget = s.widget,
                        adminWidget = s.adminWidget
                    }).FirstOrDefault();
        }

        public dynamic GetChatConfigList(dynamic obj)
        {
            string sortField = null; string sortBy = "";

            var main = (from s in RepositoryContext.ChatConfigurations
                        where s.deleteFlag == false
                        select new
                        {
                            id = s.Id,
                            name = s.name,
                            chatAppId = s.chatAppId,
                            region = s.region,
                            updatedDate = s.updatedDate
                        }).AsNoTracking();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForChatConfigList(main, obj);
                }
            }


            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "updatedDate";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            var objTotal = main.Count();
            main = PaginationSessionForChatConfigList(main, obj);

            var objResult = main.AsNoTracking().ToList();
            return new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved." };
        }

        public dynamic GetChatConfigs()
        {

            var main = (from s in RepositoryContext.ChatConfigurations
                        where s.deleteFlag == false
                        select new
                        {
                            id = s.Id,
                            name = s.name
                        }).AsNoTracking();

            return main.AsNoTracking().ToList();
        }

        private dynamic FilterSessionForChatConfigList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                name = default(string),
                chatAppId = default(string),
                region = default(string),
                updatedDate = default(DateTime?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "ChatAppId")
                    {
                        string ChatAppId = filterValue;
                        if (!String.IsNullOrEmpty(ChatAppId))
                        {
                            ChatAppId = ChatAppId.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.chatAppId.ToLower().Contains(ChatAppId));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForChatConfigList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                name = default(string),
                chatAppId = default(string),
                region = default(string),
                updatedDate = default(DateTime?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, int> pageDictionary = new Dictionary<int, int>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<int> PaginationFilterArray = new List<int> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public int AddChatConfig(dynamic obj)
        {
            tblChatConfiguration newChatObj = new tblChatConfiguration();
            newChatObj.chatAppId = obj.chatAppId;
            newChatObj.name = obj.name;
            newChatObj.region = obj.region;
            newChatObj.apiKey = obj.apiKey;
            newChatObj.widget = obj.widget;
            newChatObj.adminWidget = obj.adminWidget;
            newChatObj.deleteFlag = false;
            newChatObj.createdDate = System.DateTime.Now;
            newChatObj.updatedDate = System.DateTime.Now;
            RepositoryContext.ChatConfigurations.Add(newChatObj);
            RepositoryContext.SaveChanges();
            return newChatObj.Id;
        }

        public int UpdateChatConfig(dynamic obj)
        {
            int id = obj.id;
            tblChatConfiguration updateChatObj = RepositoryContext.ChatConfigurations.Find(id);
            updateChatObj.chatAppId = obj.chatAppId;
            updateChatObj.name = obj.name;
            updateChatObj.region = obj.region;
            updateChatObj.apiKey = obj.apiKey;
            updateChatObj.widget = obj.widget;
            updateChatObj.adminWidget = obj.adminWidget;
            updateChatObj.updatedDate = System.DateTime.Now;
            RepositoryContext.ChatConfigurations.Update(updateChatObj);
            RepositoryContext.SaveChanges();
            return id;
        }

        public bool DeleteChatConfig(int id)
        {
            tblChatConfiguration updateChatObj = RepositoryContext.ChatConfigurations.Find(id);
            updateChatObj.deleteFlag = true;
            updateChatObj.updatedDate = System.DateTime.Now;
            RepositoryContext.ChatConfigurations.Update(updateChatObj);
            RepositoryContext.SaveChanges();
            return true;
        }

        public int CheckDuplicateForChatConfig(dynamic obj, bool isNew)
        {
            int result = 0;
            string appId = obj.chatAppId;
            string name = obj.name;
            if (isNew)
            {
                var appIdQuery = (from s in RepositoryContext.ChatConfigurations where s.chatAppId == appId && s.deleteFlag == false select s);
                if (appIdQuery.Count() > 0)
                {
                    result = 1;
                    return result;
                }

                var nameQuery = (from s in RepositoryContext.ChatConfigurations where s.name == name && s.deleteFlag == false select s);
                if (nameQuery.Count() > 0)
                {
                    result = 2;
                    return result;
                }
            }
            else
            {
                int id = obj.id;
                var appIdQuery = (from s in RepositoryContext.ChatConfigurations where s.chatAppId == appId && s.deleteFlag == false && s.Id != id select s);
                if (appIdQuery.Count() > 0)
                {
                    result = 1;
                    return result;
                }
                var nameQuery = (from s in RepositoryContext.ChatConfigurations where s.name == name && s.deleteFlag == false && s.Id != id select s);
                if (nameQuery.Count() > 0)
                {
                    result = 2;
                    return result;
                }
            }
            return result;
        }
    }
}