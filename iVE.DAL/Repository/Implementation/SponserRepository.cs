using System.Linq;
using System;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation
{
    public class SponserRepository : RepositoryBase<tblSponsor>, ISponserRepository
    {
        private string showId = LoginData.showId;
        public SponserRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetSponsors()
        {
            var main = (from s in RepositoryContext.Sponsors
                        where s.showId == showId && s.deleteFlag == false
                        select new
                        {
                            ID = s.s_ID,
                            SponsorName = s.s_name,
                            Content = s.s_content,
                            SponsorLogo = s.s_logo,
                            SequenceNo = s.s_seq,
                            SponsorCategoryID = s.s_category,
                            SponsorUrl = s.s_url,
                            TemplateId = s.templateId,
                            s.s_exhID,
                            SponsorCategory = (from sc in RepositoryContext.SponsorCategories where sc.sponsorCat_ID == s.s_category && sc.deleteFlag == false select sc.sponsorCat_name).FirstOrDefault(),
                        }).AsNoTracking()
                        .ToList()
                        .OrderBy(x => x.SequenceNo);
            return main;
        }

        public dynamic GetSponsorById(string Id)
        {
            var main = (from s in RepositoryContext.Sponsors
                        where s.deleteFlag == false && s.s_ID == Id
                        select new
                        {
                            s.s_ID,
                            s.s_category,
                            s.s_content,
                            s.s_exhID,
                            s.s_logo,
                            s.s_name,
                            s.s_seq,
                            s_showId = s.showId,
                            s.s_showInCate,
                            s.s_url,
                            s.templateId,
                            s.menuID,
                            s.lang,
                            //s.deleteFlag,
                            //s.UpdatedDate
                        }).AsNoTracking().FirstOrDefault();
            return main;
        }


        public void SaveNewSponsor(dynamic obj, tblSponsor sponsor)
        {
            string url = null; string temID = null; string showId = null;
            string showInCate = null;
            dynamic sc = obj.showInCate;
            showId = obj.showId;
            if (!string.IsNullOrEmpty(Convert.ToString(obj.targeturl)))
            {
                url = obj.targeturl;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(obj.templateId)))
            {
                temID = obj.templateId;
            }
            if (string.IsNullOrEmpty(showId))
            {
                showId = null;
            }
            if (sc != null)
            {
                showInCate = string.Join(",", sc);
            }
            sponsor.showId = showId;
            sponsor.menuID = obj.menuID;
            sponsor.s_name = obj.name;
            sponsor.s_content = obj.content;
            sponsor.s_logo = obj.url;
            sponsor.s_category = obj.category;
            sponsor.s_seq = obj.seq;
            sponsor.lang = 1;
            sponsor.s_url = url;
            sponsor.showId = showId;
            sponsor.s_showInCate = showInCate;
            sponsor.templateId = temID;
            sponsor.deleteFlag = false;
            sponsor.UpdatedDate = DateTime.Now;

            RepositoryContext.Sponsors.Add(sponsor);
            RepositoryContext.SaveChanges();
        }
        public void UpdateSponsor(dynamic obj, tblSponsor sponsor)
        {
            string url = null; string temID = null;
            string showId = null;
            string showInCate = null;
            dynamic sc = obj.showInCate;
            showId = obj.showId;
            if (!string.IsNullOrEmpty(Convert.ToString(obj.targeturl)))
            {
                url = obj.targeturl;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(obj.templateId)))
            {
                temID = obj.templateId;
            }
            if (sc != null)
            {

                showInCate = string.Join(",", sc);
            }
            if (string.IsNullOrEmpty(showId))
            {
                showId = null;
            }
            sponsor.menuID = obj.menuID;
            sponsor.s_name = obj.name;
            sponsor.s_content = obj.content;
            sponsor.s_logo = obj.url;
            sponsor.s_category = obj.category;
            sponsor.s_seq = obj.seq;
            sponsor.s_url = url;
            sponsor.templateId = temID;
            sponsor.s_exhID = obj.exhID;
            sponsor.showId = showId;
            sponsor.s_showInCate = showInCate;
            sponsor.UpdatedDate = DateTime.Now;
            RepositoryContext.Sponsors.Update(sponsor);
            RepositoryContext.SaveChanges();

        }
        public void DeleteSponsor(tblSponsor sponsor)
        {
            sponsor.deleteFlag = true;
            sponsor.UpdatedDate = DateTime.Now;
            RepositoryContext.Sponsors.Update(sponsor);
            RepositoryContext.SaveChanges();
        }
        public dynamic GetSponsorList(dynamic obj)
        {
            dynamic result;
            string sortField = null; string sortBy = "";
            var mainQuery = (from s in RepositoryContext.Sponsors
                             join sc in RepositoryContext.SponsorCategories on s.s_category equals sc.sponsorCat_ID
                             where s.showId == showId && sc.showId == showId
                             && s.deleteFlag == false && sc.deleteFlag == false
                             select new
                             {
                                 ID = s.s_ID,
                                 SponsorName = s.s_name,
                                 SponsorCategory = sc.sponsorCat_name,
                                 Content = s.s_content,
                                 SponsorLogo = s.s_logo,
                                 SponsorUrl = s.s_url,
                                 TemplateID = s.templateId,
                                 Cat_seq = sc.sponsorCat_seq,
                                 Sp_seq = s.s_seq
                             }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForSponsorList(mainQuery, obj);
                }
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "Cat_seq";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var sortList = new System.Collections.Generic.List<SortModel>();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;
            sortList.Add(objSort);

            var objSort01 = new SortModel();
            objSort01.ColId = "Sp_seq";
            objSort01.Sort = "asc";
            sortList.Add(objSort01);

            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForSponsorList(mainQuery, obj);

            var objResult = mainQuery.ToList();
            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetSponsorListByCateGrouping(dynamic obj)
        {
            string paraVal = obj.category;//d_desc from tblDay
            var mainQuery = (from s in RepositoryContext.Sponsors
                             join sc in RepositoryContext.SponsorCategories on s.s_category equals sc.sponsorCat_ID
                             join sd in (from d in RepositoryContext.Days
                                         where d.showId == showId && d.deleteFlag == false
                                         select new { d.d_ID, d.d_date, d.d_desc, d.showId }) on s.showId equals sd.showId
                             into sl
                             from slist in sl.DefaultIfEmpty()

                             where s.showId == showId
                             && s.deleteFlag == false && sc.deleteFlag == false
                             && (paraVal == null || paraVal == "" || slist.d_ID == paraVal)
                             select new
                             {
                                 ID = s.s_ID,
                                 SponsorName = s.s_name,
                                 SponsorCategory = sc.sponsorCat_name,
                                 Content = s.s_content,
                                 SponsorLogo = s.s_logo,
                                 SponsorUrl = s.s_url,
                                 TemplateID = s.templateId,
                                 Cat_seq = sc.sponsorCat_seq,
                                 Sp_seq = s.s_seq,
                                 showId = s.showId,
                                 s.s_showInCate,
                             }).AsNoTracking().Distinct().ToList().OrderBy(x => x.Cat_seq).ThenBy(s => s.Sp_seq)
                        .GroupBy(x => x.SponsorCategory)
                        .Select(b => new
                        {
                            sponsorCategory = b.Key,
                            sponsorList = b.Select(bn => new
                            {
                                bn.SponsorName,
                                bn.Content,
                                bn.SponsorLogo,
                                bn.SponsorUrl,
                                bn.TemplateID,
                                bn.Sp_seq,
                                bn.s_showInCate,
                                bn.showId
                            }).ToList()
                        });
            return mainQuery;
        }

        public dynamic GetSponsorDetails(string Name)
        {
            var main = (from s in RepositoryContext.Sponsors
                        where s.showId == showId && s.deleteFlag == false && s.s_name.ToLower() == Name.ToLower()
                        select new
                        {
                            ID = s.s_ID,
                            SponsorName = s.s_name,
                            Content = s.s_content,
                            SponsorLogo = s.s_logo,
                            SequenceNo = s.s_seq,
                            SponsorCategoryID = s.s_category,
                            SponsorUrl = s.s_url,
                            TemplateId = s.templateId,
                            s.s_exhID,
                            s.showId,
                            s.s_showInCate,
                            SponsorCategory = (from sc in RepositoryContext.SponsorCategories where sc.sponsorCat_ID == s.s_category && sc.deleteFlag == false select sc.sponsorCat_name).FirstOrDefault(),
                        }).ToList();
            return main;
        }

        #region  Filter Session
        private dynamic FilterSessionForSponsorList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ID = default(string),
                SponsorName = default(string),
                SponsorCategory = default(string),
                Content = default(string),
                SponsorLogo = default(string),
                SponsorUrl = default(string),
                TemplateID = default(string),
                Cat_seq = default(int?),
                Sp_seq = default(int?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "SponsorName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.SponsorName.ToLower().Contains(Name));

                        }
                    }
                    if (filterName == "SponsorCategory")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.SponsorCategory.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        #endregion

        #region  Pagination Session
        private dynamic PaginationSessionForSponsorList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ID = default(string),
                SponsorName = default(string),
                SponsorCategory = default(string),
                Content = default(string),
                SponsorLogo = default(string),
                SponsorUrl = default(string),
                TemplateID = default(string),
                Cat_seq = default(int?),
                Sp_seq = default(int?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.SponsorName);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.SponsorName) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        #endregion
    }
}