using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation {
    public class ShowConfigRepository : RepositoryBase<tblShowConfig>, IShowConfigRepository {
        public ShowConfigRepository (AppDB repositoryContext) : base (repositoryContext) {

        }

        public dynamic GetShowConfig () {
            var main = (from s in RepositoryContext.ShowConfigs
                where s.Status == 1
                select new {
                Id = s.Id,
                Name = s.Name,
                Value = s.Value,
                Status = s.Status,
            }).AsNoTracking().ToList ();
            return main;
        }
        

        public bool SaveNewShowConfig (dynamic obj, string name) {
            try
            {
                tblShowConfig showconfig = new tblShowConfig();
                showconfig.Value = obj.Value;
                showconfig.Status = 1;

                RepositoryContext.ShowConfigs.Add(showconfig);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        public bool UpdateShowConfig (dynamic obj, tblShowConfig showconfig) {
            try
            {
                showconfig.Value = obj.value;
                RepositoryContext.ShowConfigs.Update(showconfig);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
       
    }
}