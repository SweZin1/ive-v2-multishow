using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation
{
    public class ExhibitorRepository : RepositoryBase<tblExhibitor>, IExhibitorRepository
    {
        private string showId = LoginData.showId;
        public ExhibitorRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetAllExhibitionList(dynamic obj, string sortField, string sortBy)
        {
            dynamic result;
            var mainQuery = (from e in RepositoryContext.Exhibitors
                             join cs in (from c in RepositoryContext.Countries
                                         where c.deleteFlag == false
                                         select new { c.ISO3Digit, c.CountryName }) on e.Country equals cs.ISO3Digit into csl
                             from clt in csl.DefaultIfEmpty()
                             where e.showId == showId && e.Status == ExhibitorStatus.Published
                             select new
                             {
                                 e.ExhID,
                                 e.CompanyName,
                                 description = e.Description,
                                 country = clt.CountryName,
                                 exhType = e.ExhType,
                                 exhCategory = e.ExhCategory,
                                 hallNo = e.HallNo,
                                 boothNo = e.BoothNo,
                                 boothTemplate = e.BoothTemplate,
                                 e.SequenceNo,
                                 logo = e.Logo
                             }).Distinct();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForExhibitorlist(mainQuery, obj);
                }
            }
            var objTotal = mainQuery.AsNoTracking().Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.AsNoTracking().OrderBy(sortList);
            mainQuery = PaginationSessionForExhibitorlist(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;

        }
        public dynamic GetAllExhibitionListForOrg(dynamic obj, string sortField, string sortBy)
        {
            dynamic result;
            var mainQuery = (from e in RepositoryContext.Exhibitors
                             join cs in (from c in RepositoryContext.Countries
                                         where c.deleteFlag == false
                                         select new { c.ISO3Digit, c.CountryName }) on e.Country equals cs.ISO3Digit into cls
                             from ctl in cls.DefaultIfEmpty()
                                 //where e.Status != Status.Delete //&& gb.a_type ==UserType.MainExhibitor
                             where e.showId == showId
                             select new
                             {
                                 e.ExhID,
                                 e.CompanyName,
                                 description = e.Description,
                                 country = ctl.CountryName,
                                 exhType = e.ExhType,
                                 exhCategory = e.ExhCategory,
                                 hallNo = e.HallNo,
                                 boothNo = e.BoothNo,
                                 boothTemplate = e.BoothTemplate,
                                 status = e.Status,
                                 sequenceNo = e.SequenceNo,
                                 contactPersonId = (from a in RepositoryContext.Accounts
                                                    where a.showId == showId && a.ExhId == e.ExhID && a.a_type == UserType.MainExhibitor && a.deleteFlag == false
                                                    select a.a_ID).FirstOrDefault()
                             }).Distinct();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForExhibitorlistForOrg(mainQuery, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.AsNoTracking().OrderBy(sortList);
            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForExhibitorlistForOrg(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;

        }
        private dynamic FilterSessionForExhibitorlistForOrg(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhID = default(string),
                CompanyName = default(string),
                description = default(string),
                country = default(string),
                exhType = default(string),
                exhCategory = default(string),
                hallNo = default(string),
                boothNo = default(string),
                boothTemplate = default(string),
                status = default(string),
                sequenceNo = default(int?),
                contactPersonId = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "CompanyName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.CompanyName.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationSessionForExhibitorlistForOrg(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhID = default(string),
                CompanyName = default(string),
                description = default(string),
                country = default(string),
                exhType = default(string),
                exhCategory = default(string),
                hallNo = default(string),
                boothNo = default(string),
                boothTemplate = default(string),
                status = default(string),
                sequenceNo = default(int?),
                contactPersonId = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.ExhID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.ExhID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }
        private dynamic FilterSessionForExhibitorlist(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhID = default(string),
                CompanyName = default(string),
                description = default(string),
                country = default(string),
                exhType = default(string),
                exhCategory = default(string),
                hallNo = default(string),
                boothNo = default(string),
                boothTemplate = default(string),
                SequenceNo = default(int?),
                logo = default(string)
                //Product = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "CompanyName")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.CompanyName.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "Product")
                        {
                            string PName = filterValue;
                            if (!String.IsNullOrEmpty(PName))
                            {
                                PName = PName.ToLower();
                                var exproducts = (from ep in RepositoryContext.ExhibitorProductTypes join e in tmpQuery on ep.ExhID equals e.ExhID join pm in RepositoryContext.ProductTypeMaster on ep.ProductID equals pm.ProductID where pm.ProductID.ToLower().Contains(PName) select e.ExhID).Distinct().ToArray();

                                //tmpQuery = tmpQuery.Where(x => x.Product.ToLower().Contains(PName));
                                tmpQuery = (from c in tmpQuery where exproducts.Contains(c.ExhID) select c);
                            }
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationSessionForExhibitorlist(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhID = default(string),
                CompanyName = default(string),
                description = default(string),
                country = default(string),
                exhType = default(string),
                exhCategory = default(string),
                hallNo = default(string),
                boothNo = default(string),
                boothTemplate = default(string),
                SequenceNo = default(int?),
                logo = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.ExhID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.ExhID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public void UpdateExhibitor(dynamic obj, tblExhibitor exhibitor)
        {
            SocialMediaConfig newSocialMediaConfig = new SocialMediaConfig();
            newSocialMediaConfig.facebook = obj.facebook;
            newSocialMediaConfig.instragram = obj.instragram;
            newSocialMediaConfig.twitter = obj.twitter;
            newSocialMediaConfig.linkin = obj.linkin;

            var socialMedia = Newtonsoft.Json.JsonConvert.SerializeObject(newSocialMediaConfig);
            exhibitor.CompanyName = obj.companyName;
            exhibitor.Description = obj.description;
            exhibitor.Country = obj.country;
            //exhibitor.ExhType = obj.exhType;
            exhibitor.ExhCategory = obj.exhCategory;
            exhibitor.BoothTemplate = obj.boothTemplate;
            exhibitor.Logo = obj.logo;
            exhibitor.BoothBanner = obj.boothBanner;
            exhibitor.SocialMedia = socialMedia;
            exhibitor.HallNo = obj.hallNo;
            exhibitor.BoothNo = obj.boothNo;
            exhibitor.CompanyVideo = obj.companyVideo;
            exhibitor.CompanyDocument = obj.companyDocument;
            if (obj.sequenceNo != null)
            {
                exhibitor.SequenceNo = Convert.ToInt32(obj.sequenceNo);
            }
            //exhibitor.additionalBadges = obj.additionalBadges;
            exhibitor.UpdatedDate = DateTime.Now;
            RepositoryContext.Exhibitors.Update(exhibitor);
            RepositoryContext.SaveChanges();
        }

        public void UpdateExhibitorOrg(dynamic obj, tblExhibitor exhibitor)
        {
            SocialMediaConfig newSocialMediaConfig = new SocialMediaConfig();
            newSocialMediaConfig.facebook = obj.facebook;
            newSocialMediaConfig.instragram = obj.instragram;
            newSocialMediaConfig.twitter = obj.twitter;
            newSocialMediaConfig.linkin = obj.linkin;

            var socialMedia = Newtonsoft.Json.JsonConvert.SerializeObject(newSocialMediaConfig);
            exhibitor.CompanyName = obj.companyName;
            exhibitor.Description = obj.description;
            exhibitor.Country = obj.country;
            exhibitor.ExhType = obj.exhType;
            exhibitor.HallNo = obj.hallNo;
            exhibitor.BoothNo = obj.boothNo;
            if (obj.sequenceNo != null)
            {
                exhibitor.SequenceNo = Convert.ToInt32(obj.sequenceNo);
            }
            exhibitor.additionalBadges = obj.additionalBadges;
            exhibitor.UpdatedDate = DateTime.Now;
            RepositoryContext.Exhibitors.Update(exhibitor);
            RepositoryContext.SaveChanges();
        }

        public dynamic GetExhibitorData(string exhId)
        {
            var main = (from e in RepositoryContext.Exhibitors
                        join b in RepositoryContext.Templates on e.ExhID equals b.ExhID into lb
                        from gb in lb.DefaultIfEmpty()
                        where e.showId == showId && e.ExhID == exhId
                        select new
                        {
                            e.ExhID,
                            e.CompanyName,
                            e.CompanyVideo,
                            e.CompanyDocument,
                            e.Country,
                            e.Status,
                            e.GroupChatStatus,
                            e.Description,
                            e.HallNo,
                            e.ExhType,
                            e.BoothNo,
                            e.SequenceNo,
                            e.Logo,
                            e.ExhCategory,
                            e.additionalBadges,
                            ContactPersonStatus = e.ChatContactMembers != null && e.ChatContactMembers != "" ? true : false,
                            SocialMedia = e.SocialMedia != null && e.SocialMedia != "" ? Newtonsoft.Json.JsonConvert.DeserializeObject<SocialMediaConfig>(e.SocialMedia) : null,
                            gb.TemplateID,
                            gb.BoothID,
                            entitleBadges = (from et in RepositoryContext.ExhibitorTypes
                                             where et.showId == showId && et.deleteFlag == false &&
                                             et.ID.ToString() == e.ExhType
                                             select et.entitleBadges).FirstOrDefault()
                        }).AsNoTracking().FirstOrDefault();
            return main;
        }

        public void CreateExhibitor(dynamic obj, string exhId)
        {
            SocialMediaConfig newSocialMediaConfig = new SocialMediaConfig();
            newSocialMediaConfig.facebook = obj.facebook;
            newSocialMediaConfig.instragram = obj.instragram;
            newSocialMediaConfig.twitter = obj.twitter;
            newSocialMediaConfig.linkin = obj.linkin;

            int seqNo = 0;
            int addBadges = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(obj.sequenceNo)))
            {
                seqNo = obj.sequenceNo;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(obj.additionalBadges)))
            {
                addBadges = obj.additionalBadges;
            }
            //int.TryParse(obj.sequenceNo, out seqNo);

            var socialMedia = Newtonsoft.Json.JsonConvert.SerializeObject(newSocialMediaConfig);
            tblExhibitor exhibitor = new tblExhibitor();
            exhibitor.ExhID = exhId;
            exhibitor.showId = showId;
            exhibitor.CompanyName = obj.companyName;
            exhibitor.Description = obj.description;
            exhibitor.Country = obj.country;
            exhibitor.ExhType = obj.exhType;
            exhibitor.ExhCategory = obj.exhCategory;
            exhibitor.HallNo = obj.hallNo;
            exhibitor.BoothNo = obj.boothNo;
            exhibitor.BoothTemplate = obj.boothTemplate;
            exhibitor.Logo = obj.logo;
            exhibitor.BoothBanner = obj.boothBanner;
            exhibitor.SocialMedia = socialMedia;
            exhibitor.CompanyVideo = obj.companyVideo;
            exhibitor.CompanyDocument = obj.companyDocument;
            exhibitor.SequenceNo = seqNo;
            exhibitor.additionalBadges = addBadges;
            exhibitor.CreatedDate = DateTime.Now;
            exhibitor.UpdatedDate = DateTime.Now;
            exhibitor.Status = ExhibitorStatus.InitialState;
            RepositoryContext.Exhibitors.Add(exhibitor);
            RepositoryContext.SaveChanges();
        }

        public dynamic GetVisitedVisitor(dynamic obj, string sortField, string sortBy, string exhId, string status, string returnUrl, bool isExport)
        {
            dynamic result;
            var mainQuery = (from a in RepositoryContext.Accounts
                             join au in RepositoryContext.Audits on a.a_ID equals au.UserId
                             join ct in (from cc in RepositoryContext.Countries
                                         where cc.deleteFlag == false
                                         select new { cc.ISO3Digit, cc.CountryName }) on a.a_country equals ct.ISO3Digit into ctl
                             from clist in ctl.DefaultIfEmpty()

                             where a.showId == showId && au.showId == showId
                             && au.AlternativeId == exhId && au.LogType == AuditLogType.VisitExhibitor &&
                             a.deleteFlag == false
                             select new
                             {
                                 id = a.a_ID,
                                 fullName = a.a_fullname,
                                 designation = a.a_designation,
                                 company = a.a_company,
                                 address = a.a_addressHome,
                                 country = clist.CountryName,
                                 photo = (a.ProfilePic == null || a.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + a.ProfilePic)),
                                 VisitedCount = (from aud in RepositoryContext.Audits where aud.UserId == a.a_ID && aud.AlternativeId == exhId && aud.LogType == AuditLogType.VisitExhibitor select aud.AlternativeId).Distinct().Count(),
                                 lastVisitedDate = (from aud in RepositoryContext.Audits where aud.UserId == a.a_ID && aud.AlternativeId == exhId && aud.LogType == AuditLogType.VisitExhibitor orderby aud.LogDateTime descending select aud.LogDateTime).FirstOrDefault(),
                                 favStatus = (from f in RepositoryContext.UsersFavourites
                                              where f.ItemType == FavouriteItem.Visitor && f.FavItemId == a.a_ID &&
                                              f.UserId == exhId && f.Status == 1
                                              select f.UserId).Distinct().Count() == 1 ? 1 : 0,
                             }).AsNoTracking().Distinct();
            int filter = 1;
            if (Convert.ToInt16(status) == 3)
            {
                filter = 0;
            }
            if (Convert.ToInt16(status) != 1)
            {
                mainQuery = mainQuery.Where(x => x.favStatus == filter);
            }
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForVisitedVisitor(mainQuery, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            if (isExport)
            {
                return mainQuery.AsNoTracking().ToList();
            }
            mainQuery = PaginationSessionForVisitoredVisitor(mainQuery, obj);
            var objResult = mainQuery.AsNoTracking().ToList();
            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        private dynamic FilterSessionForVisitedVisitor(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(string),
                fullName = default(string),
                designation = default(string),
                company = default(string),
                address = default(string),
                country = default(string),
                photo = default(string),
                VisitedCount = default(int),
                lastVisitedDate = default(DateTime),
                favStatus = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "fullName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.fullName.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationSessionForVisitoredVisitor(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(string),
                fullName = default(string),
                designation = default(string),
                company = default(string),
                address = default(string),
                country = default(string),
                photo = default(string),
                VisitedCount = default(int),
                lastVisitedDate = default(DateTime),
                favStatus = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public void UpdateExhibitorStatusByExhibitorId(string exhId, string status, tblExhibitor exhibitor)
        {
            exhibitor.Status = status;
            exhibitor.UpdatedDate = DateTime.Now;
            RepositoryContext.Exhibitors.Update(exhibitor);
            RepositoryContext.SaveChanges();

        }

        public bool AddChatContactMember(string exhId, string userId, tblExhibitor exhibitor)
        {
            bool needChanges = false;
            string ChatContactMembers = exhibitor.ChatContactMembers == null ? "" : exhibitor.ChatContactMembers;
            if (!string.IsNullOrEmpty(ChatContactMembers) && !ChatContactMembers.Contains(userId))
            {
                ChatContactMembers += "," + userId;
                needChanges = true;
            }

            if (string.IsNullOrEmpty(ChatContactMembers))
                ChatContactMembers = userId;

            exhibitor.ChatContactMembers = ChatContactMembers.TrimEnd(',');
            exhibitor.UpdatedDate = DateTime.Now;
            RepositoryContext.Exhibitors.Update(exhibitor);
            RepositoryContext.SaveChanges();
            return needChanges;
        }
        public bool RemoveChatContactMember(string exhId, string userId, tblExhibitor exhibitor)
        {
            bool needChanges = false;
            string ChatContactMembers = exhibitor.ChatContactMembers == null ? "" : exhibitor.ChatContactMembers;
            if (ChatContactMembers.Contains(userId))
            {
                ChatContactMembers = string.Join(",", ChatContactMembers.Split(',').Where(i => i != userId));
                ChatContactMembers = ChatContactMembers.TrimEnd(',');
                exhibitor.ChatContactMembers = ChatContactMembers;
                exhibitor.UpdatedDate = DateTime.Now;
                RepositoryContext.Exhibitors.Update(exhibitor);
                RepositoryContext.SaveChanges();
                needChanges = true;
            }
            return needChanges;
        }
        public dynamic GetChatContactMembers(string exhId)
        {
            var main = (from e in RepositoryContext.Exhibitors
                        from a in RepositoryContext.Accounts
                        where a.showId == showId && e.showId == showId && a.deleteFlag == false
                        && e.ExhID == exhId && e.ChatContactMembers.Contains(a.a_ID)
                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            a.a_designation,
                            a.ChatActiveStatus
                        }).AsNoTracking().Distinct().ToList();
            return main;
        }

        public dynamic GetExhibitorTypeList()
        {
            var main = (from et in RepositoryContext.ExhibitorTypes
                        where et.showId == showId && et.deleteFlag == false
                        select new
                        {
                            ID = et.ID.ToString(),
                            et.Type,
                            et.OrderNo,
                            et.entitleBadges,
                            et.CreatedDate
                        }).AsNoTracking().ToList();
            return main;
        }
        public string GetExhibitorTypeIDByType(string type)
        {
            var main = (from et in RepositoryContext.ExhibitorTypes
                        where et.deleteFlag == false && et.Type == type
                        select new
                        {
                            et.ID
                        }).FirstOrDefault();
            return main.ID.ToString();
        }

        public dynamic GetHallList()
        {
            var main = (from h in RepositoryContext.Halls
                        where h.showId == showId && h.deleteFlag == false
                        select new
                        {
                            h.HallId,
                            h.HallName,
                            h.CreatedDate
                        }).ToList();
            return main;
        }

        public dynamic GetBoothList()
        {
            var main = (from b in RepositoryContext.Booths
                        where b.deleteFlag == false
                        select new
                        {
                            b.ID,
                            b.BoothNo,
                            b.CreatedDate
                        }).ToList();
            return main;
        }

        public dynamic GetExhibitorCategory()
        {
            var main = (from c in RepositoryContext.ExhibitorCategories
                        where c.showId == showId && c.deleteFlag == false
                        select new
                        {
                            ID = c.ID.ToString(),
                            c.Category
                        }).AsNoTracking().ToList();
            return main;
        }

        public dynamic GetExhibitors(dynamic obj, string sortField, string sortBy)
        {

            var main = (from e in RepositoryContext.Exhibitors
                        join cs in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on e.Country equals cs.ISO3Digit
                        join exhT in RepositoryContext.ExhibitorTypes on e.ExhType equals exhT.ID.ToString() into et
                        from etl in et.DefaultIfEmpty()
                        join exhC in RepositoryContext.ExhibitorCategories on e.ExhCategory equals exhC.ID.ToString() into t3
                        from exhC in t3.DefaultIfEmpty()
                        join account in RepositoryContext.Accounts on e.ExhID equals account.ExhId into exh
                        from account in exh.DefaultIfEmpty()
                        join exhTem in RepositoryContext.Templates on e.ExhID equals exhTem.ExhID into t4
                        from tem in t4.DefaultIfEmpty()

                        where e.showId == showId && account.a_type == ContactPersonType.MainExhibitor
                        select new
                        {
                            e.ExhID,
                            e.CompanyName,
                            e.Description,
                            e.Status,
                            Country = cs.CountryName,
                            ExhType = etl.Type,
                            ExhCategory = exhC.Category,
                            e.SequenceNo,
                            tem.TemplateID,
                            e.BoothNo,
                            e.HallNo,
                            account.a_fullname,
                            account.a_email,
                            account.a_Tel,
                            account.Fax
                        }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = ExhibitorExportFilterSession(main, obj);
                }
            }

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            return main.ToList();
        }

        public dynamic GetExhibitorsByCountry()
        {
            var main = (from e in RepositoryContext.Exhibitors
                        join acc in RepositoryContext.Accounts on e.ExhID equals acc.ExhId
                        join ec in (from cs in RepositoryContext.Countries
                                    where cs.deleteFlag == false
                                    select new { cs.ISO3Digit, cs.CountryName }) on e.Country equals ec.ISO3Digit
                        where e.showId == showId && acc.showId == showId
                        && acc.deleteFlag == false && acc.a_type == ContactPersonType.MainExhibitor
                        && e.Country != null
                        select new
                        {
                            e.ExhID,
                            Country = ec.CountryName.ToUpper()
                        }).Distinct().ToList().GroupBy(x => x.Country, (key, g) => new { name = key, value = g.Count() });
            return main;
        }

        public dynamic GetExhibitorsStatus()
        {
            var main = (from e in RepositoryContext.Exhibitors
                        join acc in RepositoryContext.Accounts on e.ExhID equals acc.ExhId
                        where e.showId == showId && acc.showId == showId
                        && acc.deleteFlag == false && acc.a_type == ContactPersonType.MainExhibitor
                        select new
                        {
                            e.ExhID,
                            e.Status
                        }).Distinct().ToList().GroupBy(x => x.Status, (key, g) => new { name = key, value = g.Count() });
            return main;
        }

        private dynamic ExhibitorExportFilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                CompanyName = default(string),
                Description = default(string),
                Country = default(string),
                ExhType = default(string),
                ExhCategory = default(string),
                SequenceNo = default(int?),
                BoothNo = default(string),
                HallNo = default(string),
                a_fullname = default(string),
                a_email = default(string),
                a_Tel = default(string),
                Fax = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "CompanyName")
                    {
                        string companyName = filterValue;
                        if (!String.IsNullOrEmpty(companyName))
                        {
                            tmpQuery = tmpQuery.Where(x => x.CompanyName.ToLower().Contains(companyName.ToLower()));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public dynamic VisitorLoginAnalysisByCountry(dynamic obj, bool isExport)
        {
            string sortField = "";
            string sortBy = "";
            int objTotal = 0;
            var mainResult = new List<dynamic>();

            // need to update by day

            var logObj = (from c in RepositoryContext.Audits
                          join a in RepositoryContext.Accounts on c.UserId equals a.a_ID
                          where c.showId == showId && a.showId == showId && c.LogType == AuditLogType.login
                          select new
                          {
                              logDate = (c.LogDateTime).ToString(("yyyy/MM/dd")),
                              c.UserId,
                              a.a_country
                          }).Distinct().ToList();

            var dayList = (from a in logObj
                           join d in RepositoryContext.Days
                            on a.logDate equals d.d_date.ToString("yyyy/MM/dd")
                           where d.showId == showId && d.deleteFlag == false
                           select a.logDate).Distinct().ToList();
            if (dayList.Count == 0)
            {
                return mainResult;
            }

            var ss = (from l in logObj
                      join d in dayList on l.logDate equals d
                      join a in RepositoryContext.Accounts on l.UserId equals a.a_ID
                      join c in RepositoryContext.Countries on a.a_country equals c.ISO3Digit
                      where c.deleteFlag == false
                      && a.deleteFlag == false && a.a_country != null && c.deleteFlag == false
                      select new { Country = c.CountryName, CoId = c.ISO3Digit }).Distinct();

            var clist = (from c in RepositoryContext.Countries
                         join a in RepositoryContext.Accounts on c.ISO3Digit equals a.a_country
                         join au in RepositoryContext.Audits on a.a_ID equals au.UserId

                         where a.showId == showId && au.showId == showId
                         && c.deleteFlag == false && au.LogType == AuditLogType.login
                         && a.deleteFlag == false
                         select new { Country = c.CountryName, CoId = c.ISO3Digit }).Distinct();

            if (!isExport)
            {
                //Columns List
                List<dynamic> columns = new List<dynamic>();
                Dictionary<string, string> columnMap = new Dictionary<string, string>();
                columnMap.Add("columnDef", "Country");
                columnMap.Add("header", "Country");
                columns.Add(columnMap);

                foreach (var colItem in dayList)
                {
                    Dictionary<string, string> dayCol = new Dictionary<string, string>();
                    dayCol.Add("columnDef", colItem);
                    dayCol.Add("header", colItem);
                    columns.Add(dayCol);
                }
                mainResult.Add(columns);
            }

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    clist = CountryAnalysisFilter(clist, obj);
                }
            }
            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "Country";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            clist = clist.OrderBy(sortList);

            if (!isExport)
            {
                objTotal = clist.Count();
                clist = CountryAnalysisPagination(clist, obj);
            }

            List<dynamic> data = new List<dynamic>();
            foreach (var d in clist)
            {
                string countryName = d.Country;
                string cId = d.CoId;
                Dictionary<string, string> datalist = new Dictionary<string, string>();
                datalist.Add("Country", countryName);
                foreach (var item in dayList)
                {
                    var totalC = 0;
                    totalC = (from c in dayList
                              join audit in logObj on c equals audit.logDate
                              join acc in RepositoryContext.Accounts on audit.UserId equals acc.a_ID

                              where acc.showId == showId && acc.deleteFlag == false && acc.a_country == cId
                              && c == item
                              select audit.UserId).Distinct().Count();
                    datalist.Add(item, totalC.ToString());
                    //if(totalC > 0){
                    //datalist.Add (item, totalC.ToString ());
                    //} 
                }
                data.Add(datalist);
            }
            if (isExport)
            {
                return data;
            }
            var result = new { data, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            mainResult.Add(result);
            return mainResult;
        }

        public List<string> GetDayList()
        {
            List<string> dayList = (from d in RepositoryContext.Days where d.deleteFlag == false select d.d_date.ToString("dd-MM-yyyy")).Distinct().ToList();
            return dayList;
        }

        public List<string> GetCountryList()
        {
            List<string> result = (from c in RepositoryContext.Countries join a in RepositoryContext.Accounts on c.ISO3Digit equals a.a_country where c.deleteFlag == false && a.deleteFlag == false select new { Country = c.CountryName }).Distinct().Select(x => x.Country).ToList();
            return result;
        }

        public dynamic GetVisitedVisitorCountryList(dynamic obj, bool isExport)
        {
            string sortField = "";
            string sortBy = "";
            int objTotal = 0;
            var mainResult = new List<dynamic>();

            var clist = (from l in RepositoryContext.Audits
                         join a in RepositoryContext.Accounts on l.UserId equals a.a_ID
                         join c in (from cs in RepositoryContext.Countries
                                    where cs.deleteFlag == false
                                    select new { cs.ISO3Digit, cs.CountryName }) on a.a_country equals c.ISO3Digit
                         //into cls from ctl in cls.DefaultIfEmpty()
                         where a.deleteFlag == false && l.LogType == AuditLogType.VisitExhibitor
                         select new { Country = c.CountryName }).AsNoTracking().Distinct();

            var exhList = (from c in RepositoryContext.Exhibitors
                           join acc in RepositoryContext.Accounts on c.ExhID equals acc.ExhId
                           join l in RepositoryContext.Audits on c.ExhID equals l.AlternativeId
                           join v in RepositoryContext.Accounts on l.UserId equals v.a_ID
                           where acc.deleteFlag == false && l.LogType == AuditLogType.VisitExhibitor
                           && acc.a_type == ContactPersonType.MainExhibitor && v.a_country != null
                           select new { ExhibitingCompany = c.CompanyName, ExhId = c.ExhID }).AsNoTracking().Distinct();

            if (!isExport)
            {
                //Columns List
                List<dynamic> columns = new List<dynamic>();
                Dictionary<string, string> columnMap = new Dictionary<string, string>();
                columnMap.Add("columnDef", "ExhibitingCompany");
                columnMap.Add("header", "ExhibitingCompany");
                columns.Add(columnMap);

                foreach (var colItem in clist)
                {
                    Dictionary<string, string> dayCol = new Dictionary<string, string>();
                    dayCol.Add("columnDef", colItem.Country);
                    dayCol.Add("header", colItem.Country);
                    columns.Add(dayCol);
                }
                mainResult.Add(columns);
            }

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    exhList = VisitedVisitorsCountryReportFilterSession(exhList, obj);
                }
            }
            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "ExhibitingCompany";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            exhList = exhList.OrderBy(sortList);

            if (!isExport)
            {
                objTotal = exhList.Count();
                exhList = VisitedVisitorCountryReportPaginationSession(exhList, obj);
            }

            List<dynamic> data = new List<dynamic>();
            foreach (var item in exhList)
            {
                string companyName = item.ExhibitingCompany;
                Dictionary<string, string> datalist = new Dictionary<string, string>();
                datalist.Add("ExhibitingCompany", companyName);
                foreach (var i in clist)
                {
                    var totalC = 0;
                    totalC = (from a in RepositoryContext.Audits
                              join acc in RepositoryContext.Accounts on a.UserId equals acc.a_ID
                              join ct in RepositoryContext.Countries on acc.a_country
                              equals ct.ISO3Digit into cts
                              from ctl in cts.DefaultIfEmpty()
                              where a.AlternativeId == item.ExhId && a.LogType == AuditLogType.VisitExhibitor &&
                                  acc.deleteFlag == false && ctl.CountryName.ToUpper() == i.Country.ToUpper()
                              select a.UserId).Distinct().Count();
                    datalist.Add(i.Country, totalC.ToString());
                }
                data.Add(datalist);
            }
            if (isExport)
            {
                return data;
            }
            var result = new { data, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            mainResult.Add(result);
            return mainResult;
        }

        public dynamic GetExhibitorDashboardStatus(string exhId)
        {
            var main = (from e in RepositoryContext.Exhibitors
                        where e.showId == showId && e.ExhID == exhId
                        select new
                        {
                            ProfileSetup = e.Status == ExhibitorStatus.InitialState ? 0 : 1,
                            ContactPersonSetup = (from c in RepositoryContext.Accounts
                                                  where c.showId == showId && c.deleteFlag == false && e.ExhID == c.ExhId
                                                  && (c.a_type == ContactPersonType.ContactPerson || c.a_type == ContactPersonType.MainExhibitor)
                                                  select c.a_ID).Distinct().Count() > 0 ? 1 : 0,
                            ProductSetup = (from p in RepositoryContext.Products
                                            where p.showId == showId && p.deleteFlag == false && p.ExhID == e.ExhID
                                            select p.p_ID).Distinct().Count() > 0 ? 1 : 0,
                            BoothSetup = (from t in RepositoryContext.Templates
                                          where t.showId == showId && t.ExhID == e.ExhID && e.Status != ExhibitorStatus.Pending
                                          && e.Status != ExhibitorStatus.InitialState
                                          select t.TemplateID).Distinct().Count() > 0 ? 1 : 0

                        }).AsNoTracking().Distinct().ToList();
            return main;
        }

        public dynamic GetVisitedVisitorList(dynamic obj, bool isExport)
        {
            string sortField = null;
            string sortBy = "";

            var main = (from acc in RepositoryContext.Accounts
                        join cs in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on acc.a_country equals cs.ISO3Digit into cls
                        from clt in cls.DefaultIfEmpty()
                        join audit in RepositoryContext.Audits on acc.a_ID equals audit.UserId
                        join e in RepositoryContext.Exhibitors on audit.AlternativeId equals e.ExhID
                        where acc.showId == showId && audit.showId == showId && e.showId == showId
                        && acc.deleteFlag == false && audit.LogType == AuditLogType.VisitExhibitor
                        select
                        new
                        {
                            ExhibitingCompany = e.CompanyName,
                            FullName = acc.a_fullname,
                            FirstName = acc.a_fname,
                            LastName = acc.a_lname,
                            Country = clt.CountryName,
                            Designation = acc.a_designation,
                            Email = acc.a_email,
                            Telephone = acc.a_Tel,
                            visitorCompany = acc.a_company
                        }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = VisitedVisitorReportFilterSession(main, obj);
                }
            }

            //Sorting
            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "ExhibitingCompany";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);

            var objTotal = main.Count();
            if (isExport)
            {
                return main.AsNoTracking().ToList();
            }
            //Pagination
            main = VisitedVisitorListPaginationSession(main, obj);
            var result = new { data = main.AsNoTracking().ToList(), dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }
        public dynamic ExhibitorRecommendationReport(dynamic obj, bool isExport)
        {
            var main = (from c in RepositoryContext.Exhibitors
                        where c.showId == showId && c.Status == ExhibitorStatus.Published
                        select new
                        {
                            ExhibitingCompany = c.CompanyName,
                            Recommended = (from r in RepositoryContext.VisitorRecommendations
                                           where r.showId == showId && r.ExhID == c.ExhID
                                           select r.VisitorID).Distinct().Count(),

                            MeetUp = (from r in RepositoryContext.VisitorRecommendations
                                      join audit in RepositoryContext.Audits on r.VisitorID equals audit.UserId
                                      where r.showId == showId && audit.showId == showId
                                      && audit.LogType == AuditLogType.VisitExhibitor &&
                                      r.ExhID == c.ExhID && audit.AlternativeId == c.ExhID
                                      select audit.UserId).Distinct().Count()

                        }).Distinct().Select(x => new
                        {
                            x.ExhibitingCompany,
                            x.Recommended,
                            x.MeetUp,
                            NotTurnup = Convert.ToInt16(x.Recommended) - Convert.ToInt16(x.MeetUp)
                        });

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = ExhRecommendationFilterSession(main, obj);
                }
            }

            //Sorting
            string sortField = "";
            string sortBy = "";
            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "ExhibitingCompany";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);

            var objTotal = main.Count();
            if (isExport)
            {
                return main.AsNoTracking().ToList();
            }
            //Pagination
            main = ExhRecommendationPaginationSession(main, obj);
            var result = new { data = main.AsNoTracking().ToList(), dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic VisitedAnalysisByCountry(string exhId)
        {
            var main = (from c in RepositoryContext.Exhibitors
                        join audit in RepositoryContext.Audits on c.ExhID equals audit.AlternativeId
                        join acc in RepositoryContext.Accounts on audit.UserId equals acc.a_ID
                        join ac in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on acc.a_country equals ac.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where c.showId == showId && audit.showId == showId && acc.showId == showId
                        && acc.deleteFlag == false && acc.a_country != null &&
                        acc.a_country != "" &&
                            audit.LogType == AuditLogType.VisitExhibitor && c.ExhID == exhId
                        select new
                        {
                            name = ctl.CountryName,
                            audit.UserId
                        }).AsNoTracking().Distinct().ToList().GroupBy(x => x.name.ToUpper(), (key, g) => new { name = key, value = g.Count() });
            return main;
        }
        public dynamic VisitedAnalysisByDay(string exhId)
        {

            DateTime today = new DateTime();
            today = DateTime.Now;

            var logObj = (from c in RepositoryContext.Audits
                          join acc in RepositoryContext.Accounts on c.UserId equals acc.a_ID
                          where c.showId == showId && acc.showId == showId && acc.deleteFlag == false
                          && c.LogType == AuditLogType.VisitExhibitor && c.AlternativeId == exhId
                          select new
                          {
                              logDate = c.LogDateTime.Date,
                              c.UserId
                          }).AsNoTracking().ToList();

            var main = (from c in logObj
                        where c.logDate >= today.AddDays(-7).Date
                        && c.logDate <= today.Date
                        select new
                        {
                            d = c.logDate.ToString("yyyy/MM/dd"),
                            c.UserId
                        }).Distinct().ToList().GroupBy(x => x.d, (key, g) => new { name = key, value = g.Count() });
            return main;
        }

        public dynamic VisitedAnalysis(string exhId)
        {

            var objReach = (from c in RepositoryContext.Exhibitors
                            where c.showId == showId && c.ExhID == exhId
                            select new
                            {
                                name = "Reach",
                                value = (from a in RepositoryContext.Audits
                                         join acc in RepositoryContext.Accounts on a.UserId equals acc.a_ID
                                         where a.showId == showId && acc.showId == showId
                                         && a.LogType == AuditLogType.VisitExhibitor && acc.deleteFlag == false
                                         && a.AlternativeId == c.ExhID
                                         select a.UserId).Distinct().Count(),
                                icon = "supervisor_account"
                            }).AsNoTracking().Distinct().ToList();

            //Engagement == Visitor => Favourite Exh,Favourite/Viewed Exh-Products,GetAppointmentWithContactPerson (Confirmed)
            var objEngagement = (from c in RepositoryContext.Exhibitors
                                 where c.showId == showId && c.ExhID == exhId
                                 select new
                                 {
                                     name = "Engagements",
                                     favExh = (from eFav in RepositoryContext.UsersFavourites
                                               where eFav.showId == showId && c.ExhID == eFav.FavItemId && eFav.ItemType == FavouriteItem.Exhibitor
                                               && eFav.Status == 1 && eFav.FavItemId == exhId
                                               select eFav.UserId).Distinct().Count(),

                                     ViewedProduct = (from p in RepositoryContext.Products
                                                      join a in RepositoryContext.Audits on p.p_ID equals a.AlternativeId
                                                      where p.showId == showId && a.showId == showId
                                                      && c.ExhID == p.ExhID && p.deleteFlag == false && a.LogType == AuditLogType.ViewProduct
                                                      select a.UserId).Distinct().Count(),

                                     favProduct = (from eFav in RepositoryContext.UsersFavourites
                                                   join p in RepositoryContext.Products on eFav.FavItemId equals p.p_ID
                                                   where eFav.showId == showId && p.showId == showId
                                                   && c.ExhID == p.ExhID && eFav.ItemType == FavouriteItem.Product && eFav.Status == 1
                                                   select eFav.UserId).Distinct().Count(),

                                     appC = (from s in RepositoryContext.Schedules
                                             join app in RepositoryContext.Appointments on s.AppointmentID equals app.AppointmentID
                                             join acc in RepositoryContext.Accounts on s.OwnerID equals acc.a_ID

                                             where s.showId == showId && acc.showId == showId
                                             && app.AppType == AppointmentCondition.AppointmentConfirmed &&
                                             s.deleteFlag == false && app.deleteFlag == false &&
                                             acc.deleteFlag == false && acc.ExhId == c.ExhID
                                             select app.AppointmentID).Distinct().Count()
                                 }).Select(x => new
                                 {
                                     name = x.name,
                                     value = (x.favExh + x.favProduct + x.ViewedProduct + x.appC),
                                     icon = "thumb_up"
                                 }).AsNoTracking().Distinct().ToList();

            var objLinks = (from ex in RepositoryContext.Exhibitors
                            where ex.showId == showId && ex.ExhID == exhId
                            select new
                            {
                                name = "Favourite",
                                value = (from c in RepositoryContext.UsersFavourites
                                         where c.showId == showId
                                         && c.FavItemId == ex.ExhID && c.Status == 1 && c.ItemType == FavouriteItem.Exhibitor
                                         select c.UserId).Distinct().Count(),
                                icon = "call_made"
                            }).AsNoTracking().Distinct().ToList();

            var objAppointment = (from s in RepositoryContext.Exhibitors
                                  where s.showId == showId && s.ExhID == exhId
                                  select new
                                  {
                                      name = "Appointment",
                                      value = (from sh in RepositoryContext.Schedules
                                               join app in RepositoryContext.Appointments on sh.AppointmentID equals app.AppointmentID
                                               join acc in RepositoryContext.Accounts on sh.OwnerID equals acc.a_ID
                                               where sh.showId == showId && acc.showId == showId
                                               && sh.deleteFlag == false && app.deleteFlag == false && acc.deleteFlag == false
                                               && app.AppType == AppointmentCondition.AppointmentConfirmed && acc.ExhId == s.ExhID
                                               select app.AppointmentID).Distinct().Count(),
                                      icon = "schedule"
                                  }).AsNoTracking().Distinct().ToList();

            var result = objReach.Select(x => x)
                .Union(objEngagement.Select(z => z))
                .Union(objLinks.Select(y => y))
                .Union(objAppointment.Select(a => a)).ToList();
            return result;
        }
        public dynamic ViewedExhProductByCountry(string exhId)
        {
            var main = (from e in RepositoryContext.Exhibitors
                        join p in RepositoryContext.Products on e.ExhID equals p.ExhID
                        join a in RepositoryContext.Audits on p.p_ID equals a.AlternativeId
                        join acc in RepositoryContext.Accounts on a.UserId equals acc.a_ID
                        join ac in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on acc.a_country equals ac.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where e.showId == showId && p.showId == showId && a.showId == showId
                        && e.ExhID == exhId && p.deleteFlag == false && a.LogType == AuditLogType.ViewProduct &&
                        acc.a_country != null
                        select new
                        {
                            name = ctl.CountryName,
                            a.AlternativeId
                        }).Distinct().ToList().GroupBy(x => x.name.ToUpper(), (key, g) => new { name = key, value = g.Count() });
            return main;
        }

        public dynamic FavouriteExhProduct(string exhId)
        {
            var main = (from p in RepositoryContext.Products
                        join f in RepositoryContext.UsersFavourites on p.p_ID equals f.FavItemId
                        where p.showId == showId && f.showId == showId
                        && p.deleteFlag == false && f.Status == 1 && f.ItemType == FavouriteItem.Product
                        && p.ExhID == exhId
                        select new
                        {
                            p.p_name,
                            f.UserId
                        }).Distinct().ToList().GroupBy(x => x.p_name.ToUpper(), (key, g) => new { name = key, value = g.Count() });
            return main;
        }
        public dynamic GetExhBoothTemplates()
        {
            var main = (from c in RepositoryContext.Templates
                        join e in RepositoryContext.Exhibitors on c.ExhID equals e.ExhID
                        where c.showId == showId && e.Status == ExhibitorStatus.Published
                        select new
                        {
                            e.CompanyName,
                            c.TemplateID
                        }).AsNoTracking().Distinct().ToList();
            return main;
        }

        public string GetExistingGroupMemberId(string exhId)
        {
            string members = (from e in RepositoryContext.Exhibitors where e.ExhID == exhId select e.ChatContactMembers).FirstOrDefault();
            string member = members.Split(",")[0];
            if (member == "")
            {
                return null;
            }

            return member;
        }

        private dynamic ExhRecommendationFilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhibitingCompany = default(string),
                Recommended = default(int),
                MeetUp = default(int),
                NotTurnup = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "ExhibitingCompany")
                    {
                        string exhCompany = filterValue.ToLower();
                        if (!String.IsNullOrEmpty(exhCompany))
                        {
                            tmpQuery = tmpQuery.Where(x => x.ExhibitingCompany.ToLower().Contains(exhCompany));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic VisitedVisitorReportFilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhibitingCompany = default(string),
                FullName = default(string),
                FirstName = default(string),
                LastName = default(string),
                Country = default(string),
                Designation = default(string),
                Email = default(string),
                Telephone = default(string),
                visitorCompany = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "ExhibitingCompany")
                    {
                        string exhCompany = filterValue.ToLower();
                        if (!String.IsNullOrEmpty(exhCompany))
                        {
                            tmpQuery = tmpQuery.Where(x => x.ExhibitingCompany.ToLower().Contains(exhCompany));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public dynamic CountryAnalysisFilter(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Country = default(string),
                CoId = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "Country")
                    {
                        string country = filterValue.ToLower();
                        if (!String.IsNullOrEmpty(country))
                        {
                            tmpQuery = tmpQuery.Where(x => x.Country.ToLower().Contains(country));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic VisitedVisitorsCountryReportFilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhibitingCompany = default(string),
                ExhId = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "ExhibitingCompany")
                    {
                        string exhCompany = filterValue.ToLower();
                        if (!String.IsNullOrEmpty(exhCompany))
                        {
                            tmpQuery = tmpQuery.Where(x => x.ExhibitingCompany.ToLower().Contains(exhCompany));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic ExhRecommendationPaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhibitingCompany = default(string),
                Recommended = default(int),
                MeetUp = default(int),
                NotTurnup = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.ExhibitingCompany);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.ExhibitingCompany) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic VisitedVisitorListPaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhibitingCompany = default(string),
                FullName = default(string),
                FirstName = default(string),
                LastName = default(string),
                Country = default(string),
                Designation = default(string),
                Email = default(string),
                Telephone = default(string),
                visitorCompany = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.ExhibitingCompany + item.FullName);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.ExhibitingCompany + main.FullName) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        private dynamic VisitedVisitorCountryReportPaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ExhibitingCompany = default(string),
                ExhId = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.ExhibitingCompany);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.ExhibitingCompany) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        private dynamic CountryAnalysisPagination(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Country = default(string),
                CoId = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.Country);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.Country) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public dynamic GetDailyActiveUsers(dynamic obj)
        {

            var rawQuery = (from audit in RepositoryContext.Audits
                            join account in RepositoryContext.Accounts on audit.UserId equals account.a_ID
                            where audit.showId == showId && account.showId == showId
                            && account.deleteFlag == false && audit.LogType == AuditLogType.login
                            select new
                            {
                                Date = audit.LogDateTime.Date,
                                UserId = account.a_ID,
                                UserType = account.a_type == UserType.MainExhibitor ? "Exhibitor" :
                                        (account.a_type == UserType.Exhibitor ? "Exhibitor" :
                                        (account.a_type == UserType.Delegate ? "Delegate" :
                                        (account.a_type == UserType.FreeDelegate ? "Free Delegate" :
                                        (account.a_type == UserType.Visitor ? "Visitor" :
                                        (account.a_type == UserType.Media ? "Media" :
                                        (account.a_type == UserType.VIP ? "VIP" :
                                        (account.a_type == UserType.Speaker ? "Speaker" : "")))))))
                            });
            rawQuery = DailyActiveUsersReportFilterSession(rawQuery, obj);

            var rawDateQuery = (from raw in rawQuery select new { raw.Date }).Distinct().OrderBy(x => x.Date);
            var rawUserQuery = (from raw in rawQuery select new { raw.UserType }).Distinct();

            List<dynamic> response = new List<dynamic>();

            foreach (var item in rawDateQuery)
            {
                var seriesObj = (from r in rawUserQuery
                                 select new
                                 {
                                     name = r.UserType,
                                     value = (from r2 in rawQuery where r2.UserType == r.UserType && r2.Date == item.Date select r2.UserId).Distinct().Count()
                                 }).ToList();
                dynamic data = new { name = item.Date.ToString("dd/MM/yyyy"), series = seriesObj };
                response.Add(data);
            }
            return response;
        }

        private dynamic DailyActiveUsersReportFilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Date = default(DateTime),
                UserId = default(string),
                UserType = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                string toDateString = DateTime.Now.ToString("yyyy-MM-dd");
                string fromDateString = DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd");

                if (obj.filter != null)
                {
                    if (obj.filter.filters != null)
                    {
                        for (int i = 0; i < obj.filter.filters.Count; i++)
                        {
                            string filterName = obj.filter.filters[i].field;
                            var filterValue = obj.filter.filters[i].value;

                            if (filterName == "fromDate")
                            {
                                string fromDate = filterValue;
                                if (!String.IsNullOrEmpty(fromDate))
                                {
                                    fromDateString = fromDate;
                                }
                            }

                            if (filterName == "toDate")
                            {
                                string toDate = filterValue;
                                if (!String.IsNullOrEmpty(toDate))
                                {
                                    toDateString = toDate;
                                }
                            }
                        }

                    }
                }


                //From Date Filter
                DateTime fDate = Convert.ToDateTime(fromDateString);
                tmpQuery = tmpQuery.Where(x => x.Date >= fDate);

                //To Date Filter
                DateTime tDate = Convert.ToDateTime(toDateString);
                tmpQuery = tmpQuery.Where(x => x.Date <= tDate);

                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public dynamic ExhEngagementDetails(dynamic obj, bool isexport)
        {
            dynamic result = null;
            string sortField = null;
            string sortBy = "";
            var Engagement = (from c in RepositoryContext.Exhibitors
                              where c.showId == showId && c.Status == ExhibitorStatus.Published
                              select new
                              {
                                  c.ExhID,
                                  favExh = (from eFav in RepositoryContext.UsersFavourites
                                            where eFav.showId == showId
                                            && c.ExhID == eFav.FavItemId && eFav.ItemType == FavouriteItem.Exhibitor
                                            && eFav.Status == 1 && eFav.FavItemId == c.ExhID
                                            select eFav.UserId).Distinct().Count(),

                                  ViewedProduct = (from p in RepositoryContext.Products
                                                   join a in RepositoryContext.Audits on p.p_ID equals a.AlternativeId
                                                   where p.showId == showId && a.showId == showId
                                                   && c.ExhID == p.ExhID && p.deleteFlag == false
                                                   && a.LogType == AuditLogType.ViewProduct
                                                   select a.UserId).Distinct().Count(),

                                  favProduct = (from eFav in RepositoryContext.UsersFavourites
                                                join p in RepositoryContext.Products on eFav.FavItemId equals p.p_ID
                                                where eFav.showId == showId && p.showId == showId
                                                && c.ExhID == p.ExhID && eFav.ItemType == FavouriteItem.Product && eFav.Status == 1
                                                && p.deleteFlag == false
                                                select eFav.UserId).Distinct().Count(),

                                  appC = (from s in RepositoryContext.Schedules
                                          join app in RepositoryContext.Appointments on s.AppointmentID equals app.AppointmentID
                                          join acc in RepositoryContext.Accounts on s.OwnerID equals acc.a_ID

                                          where app.showId == showId && acc.showId == showId
                                          && app.AppType == AppointmentCondition.AppointmentConfirmed &&
                                          s.deleteFlag == false && app.deleteFlag == false &&
                                          acc.deleteFlag == false && acc.ExhId == c.ExhID
                                          select app.AppointmentID).Distinct().Count()
                              }).Select(x => new
                              {
                                  x.ExhID,
                                  value = (x.favExh + x.favProduct + x.ViewedProduct + x.appC),
                              });

            var main = (from ex in RepositoryContext.Exhibitors
                        join eng in Engagement on ex.ExhID equals eng.ExhID
                        where ex.showId == showId
                        select new
                        {
                            company = ex.CompanyName,
                            Reach = (from a in RepositoryContext.Audits
                                     join acc in RepositoryContext.Accounts on a.UserId equals acc.a_ID
                                     where a.showId == showId && acc.showId == showId
                                     && a.LogType == AuditLogType.VisitExhibitor && acc.deleteFlag == false
                                     && a.AlternativeId == ex.ExhID
                                     select a.UserId).Distinct().Count(),
                            Like = (from c in RepositoryContext.UsersFavourites
                                    where c.showId == showId
                                    && c.FavItemId == ex.ExhID && c.Status == 1 && c.ItemType == FavouriteItem.Exhibitor
                                    select c.UserId).Distinct().Count(),
                            Appointment = (from sh in RepositoryContext.Schedules
                                           join app in RepositoryContext.Appointments on sh.AppointmentID equals app.AppointmentID
                                           join acc in RepositoryContext.Accounts on sh.OwnerID equals acc.a_ID
                                           where app.showId == showId && acc.showId == showId
                                           && sh.deleteFlag == false && app.deleteFlag == false && acc.deleteFlag == false
                                           && app.AppType == AppointmentCondition.AppointmentConfirmed && acc.ExhId == ex.ExhID
                                           select app.AppointmentID).Distinct().Count(),
                            Engagement = eng.value

                        });

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = ExhEngagementDetailsFilterSession(main, obj);
                }
            }

            //Sorting
            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "company";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objSort = new DAL.SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.AsNoTracking().OrderBy(sortList);

            var objTotal = main.Count();
            if (isexport)
            {
                return main.AsNoTracking().ToList();
            }
            //Pagination
            main = ExhEngagementDetailsPagination(main, obj);
            result = new { data = main.AsNoTracking().ToList(), dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved." };
            return result;
        }

        private dynamic ExhEngagementDetailsFilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                company = default(string),
                Reach = default(int),
                Like = default(int),
                Appointment = default(int),
                Engagement = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "company")
                    {
                        string exhCompany = filterValue.ToLower();
                        if (!String.IsNullOrEmpty(exhCompany))
                        {
                            tmpQuery = tmpQuery.Where(x => x.company.ToLower().Contains(exhCompany));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic ExhEngagementDetailsPagination(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                company = default(string),
                Reach = default(int),
                Like = default(int),
                Appointment = default(int),
                Engagement = default(int)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.company);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.company) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public dynamic ActivitySummaryReport(dynamic obj)
        {
            DateTime fromdate = Convert.ToDateTime(obj.fromDate);
            DateTime todate = Convert.ToDateTime(obj.toDate);
            List<dynamic> result = new List<dynamic>();
            var audiLog = (from a in RepositoryContext.Audits
                           join acc in RepositoryContext.Accounts on a.UserId equals acc.a_ID
                           where a.showId == showId && acc.showId == showId
                           && a.LogType == AuditLogType.VisitExhibitor
                           && a.UserId != null && a.AlternativeId != null && acc.deleteFlag == false
                           select new
                           {
                               a.UserId,
                               a.AlternativeId,
                               createdDate = Convert.ToDateTime(a.LogDateTime).Date
                           }).Distinct().ToList();

            var visitObj = (from a in audiLog
                            where a.createdDate >= fromdate.Date && a.createdDate <= todate.Date
                            select new
                            {
                                a.createdDate,
                                a.UserId,
                                a.AlternativeId
                            }).Distinct().ToList()
                        .GroupBy(x => x.createdDate.ToString("yyyy/MM/dd"), (key, g) =>
                        new
                        {
                            name = key,
                            value = g.Select(x => new { x.UserId, x.AlternativeId }).Distinct().Count()
                        });

            var appObj1 = (from app in RepositoryContext.Appointments
                           join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID
                           where app.showId == showId && s.showId == showId
                           && app.deleteFlag == false && s.deleteFlag == false
                           && app.AppType == AppointmentCondition.AppointmentConfirmed
                           select new
                           {
                               createdDate = app.ConfirmDate.Date,
                               app.AppointmentID
                           }
           ).Distinct().ToList();

            var appObj = (from app in appObj1
                          where app.createdDate >= fromdate.Date && app.createdDate <= todate.Date
                          select new
                          {
                              app.createdDate,
                              app.AppointmentID
                          }
            ).Distinct().ToList()
            .GroupBy(x => x.createdDate.ToString("yyyy/MM/dd"), (key, g) => new { name = key, value = g.Count() });

            var engObj = totalEngagementByShowDate(obj);
            var engnewObj = new { name = "Engagement", series = engObj };
            var visitnewObj = new { name = "Visit", series = visitObj };
            var appNewObj = new { name = "Appointment", series = appObj };
            result.Add(appNewObj);
            result.Add(engnewObj);
            result.Add(visitnewObj);

            return result;
        }

        private dynamic totalEngagementByShowDate(dynamic obj)
        {
            DateTime fromdate = Convert.ToDateTime(obj.fromDate);
            DateTime todate = Convert.ToDateTime(obj.toDate);

            List<DateTime> dobj = new List<DateTime>();
            while (fromdate <= todate)
            {
                dobj.Add(fromdate.Date);
                DateTime dd = fromdate.AddDays(1);
                fromdate = dd;
            }
            var favExh = (from eFav in RepositoryContext.UsersFavourites
                          join exh in RepositoryContext.Exhibitors on eFav.FavItemId equals exh.ExhID

                          where eFav.showId == showId && exh.showId == showId
                          && eFav.ItemType == FavouriteItem.Exhibitor && eFav.Status == 1
                          select new { eFav.UserId, createdDate = eFav.CreatedDate.Date }).Distinct();

            var ViewedProduct = (from p in RepositoryContext.Products
                                 join a in RepositoryContext.Audits on p.p_ID equals a.AlternativeId
                                 where p.showId == showId && a.showId == showId
                                 && p.deleteFlag == false && a.LogType == AuditLogType.ViewProduct
                                 select new { a.UserId, createDate = a.LogDateTime.Date }).Distinct();

            var favProduct = (from eFav in RepositoryContext.UsersFavourites
                              join p in RepositoryContext.Products on eFav.FavItemId equals p.p_ID
                              where eFav.showId == showId && p.showId == showId
                              && eFav.ItemType == FavouriteItem.Product && eFav.Status == 1
                              && p.deleteFlag == false
                              select new { eFav.UserId, createdDate = eFav.CreatedDate.Date }).Distinct();

            var appC = (from s in RepositoryContext.Schedules
                        join app in RepositoryContext.Appointments on s.AppointmentID equals app.AppointmentID
                        where s.showId == showId && app.showId == showId
                        && app.AppType == AppointmentCondition.AppointmentConfirmed &&
                        s.deleteFlag == false && app.deleteFlag == false
                        select new { app.AppointmentID, createdDate = app.ConfirmDate.Date }).Distinct();

            var Engagement = (from d in dobj
                              select new
                              {
                                  engdate = d.Date,
                                  engCount = (from fe in favExh
                                              where fe.createdDate == d.Date.Date
                                              select fe.UserId).Distinct().Count() +
                                            (from fp in favProduct
                                             where fp.createdDate == d.Date.Date
                                             select fp.UserId).Distinct().Count() +
                                            (from vp in ViewedProduct
                                             where vp.createDate == d.Date.Date
                                             select vp.UserId).Distinct().Count() +
                                            (from app in appC
                                             where app.createdDate == d.Date.Date
                                             select app.AppointmentID).Distinct().Count()

                              }).Distinct().ToList()
                              .Where(x => x.engCount > 0)
                              .Select(x => new { name = x.engdate.ToString("yyyy/MM/dd"), value = x.engCount });
            return Engagement;
        }

        public dynamic GetAllExhibitors()
        {
            return (from e in RepositoryContext.Exhibitors
                    join t in RepositoryContext.Templates on e.ExhID equals t.ExhID
                    where e.showId == showId && e.Status == ExhibitorStatus.Published // Should remove this condition Later
                    select new
                    {
                        //e.ExhID,
                        e.CompanyName,
                        t.TemplateID,
                        //t.BoothID
                    }).Distinct().ToList();

        }

    }
}