using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace iVE.DAL.Repository.Implementation
{
    public class ProductRepository : RepositoryBase<tblProduct>, IProductRepository
    {
        private string showId = LoginData.showId;
        public ProductRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public void SaveNewProduct(dynamic obj, string exhId, string productId)
        {
            tblProduct product = new tblProduct();
            product.p_ID = productId;
            product.showId = showId;
            product.ExhID = exhId;
            product.p_name = obj.productName;
            product.Description = obj.description;
            product.p_category = obj.category;
            product.URL = obj.url;
            product.sororder = obj.seqNo;
            product.lang = 1;
            product.deleteFlag = false;
            product.UpdatedDate = DateTime.Now.ToString();
            product.p_video = obj.videoUrl;

            RepositoryContext.Products.Add(product);
            RepositoryContext.SaveChanges();

        }

        public void UpdateProduct(dynamic obj, tblProduct product)
        {
            product.p_name = obj.productName;
            product.Description = obj.description;
            product.URL = obj.url;
            product.p_video = obj.videoUrl;
            product.sororder = obj.seqNo;
            product.UpdatedDate = DateTime.Now.ToString();
            RepositoryContext.Products.Update(product);
            RepositoryContext.SaveChanges();

        }
        public void DeleteProduct(tblProduct product)
        {
            product.deleteFlag = true;
            product.UpdatedDate = DateTime.Now.ToString();
            RepositoryContext.Products.Update(product);
            RepositoryContext.SaveChanges();
        }

        public void updateProductFileUrl(tblProduct product, string url)
        {

            product.URL = url;
            product.UpdatedDate = DateTime.Now.ToString();
            RepositoryContext.Products.Update(product);
            RepositoryContext.SaveChanges();
        }

        public dynamic addToFavProduct(string userId, string productId)
        {
            try
            {
                tbl_UsersFavourite favProductObj = new tbl_UsersFavourite();
                favProductObj.UserId = userId;
                favProductObj.showId = showId;
                favProductObj.FavItemId = productId;
                favProductObj.Status = 1;
                favProductObj.CreatedDate = System.DateTime.Now;
                favProductObj.ModifiedDate = System.DateTime.Now;
                favProductObj.CreatedBy = userId;
                favProductObj.ItemType = FavouriteItem.Product;
                RepositoryContext.UsersFavourites.Add(favProductObj);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public dynamic GetProductViewedCountByExhibitorId(string exhID)
        {
            var main = (from p in RepositoryContext.Products
                        where p.showId == showId && p.deleteFlag == false && p.ExhID == exhID
                        select new
                        {
                            ProductName = p.p_name,
                            ProductDescription = p.Description,
                            ViewCount = (from au in RepositoryContext.Audits
                                         where au.showId == showId && au.AlternativeId == p.p_ID && au.LogType == AuditLogType.ViewProduct
                                         select au.AlternativeId).Count(),
                        }).AsNoTracking().ToList();
            return main;
        }

        public dynamic GetProductDetails(string exhID, string pID)
        {
            var main = (from p in RepositoryContext.Products
                        where p.showId == showId && p.deleteFlag == false && p.ExhID == exhID && p.p_ID == pID
                        select new
                        {
                            p.ExhID,
                            p.p_ID,
                            p.p_name,
                            p.sororder,
                            p.Description,
                            p.p_video,
                            p.URL,
                            p.p_category

                        }).AsNoTracking().FirstOrDefault();
            return main;
        }

        public dynamic UpdateFavProductByVisitor(tbl_UsersFavourite favProductObj, int changedStatus, string userId)
        {
            try
            {
                favProductObj.Status = changedStatus;
                favProductObj.ModifiedDate = System.DateTime.Now;
                favProductObj.CreatedBy = userId;
                RepositoryContext.UsersFavourites.Update(favProductObj);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public dynamic GetProductsByExhId(string exhId, dynamic obj)
        {

            dynamic result;
            string sortField = null; string sortBy = "";
            var mainQuery = (from p in RepositoryContext.Products
                             where p.showId == showId && p.deleteFlag == false && p.ExhID == exhId
                             select new
                             {
                                 p.p_ID,
                                 p.ExhID,
                                 p.p_name,
                                 p.p_category,
                                 p.p_mainproductID,
                                 p.p_video,
                                 p.sororder,
                                 p.URL,
                                 p.Description,
                                 seqNo = p.sororder
                             }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForProductlist(mainQuery, obj);
                }
            }
            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "seqNo";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);

            var objTotal = mainQuery.AsNoTracking().Count();
            mainQuery = PaginationSessionForProductlist(mainQuery, obj);
            var objResult = mainQuery.AsNoTracking().ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic ViewProductByCountry(string exhId)
        {
            //Product view count by country for the token user
            var main = (from p in RepositoryContext.Products
                        join audit in RepositoryContext.Audits on p.p_ID equals audit.AlternativeId
                        join acc in RepositoryContext.Accounts on audit.UserId equals acc.a_ID
                        where p.showId == showId && audit.showId == showId && acc.showId == showId
                        && p.deleteFlag == false && audit.LogType == AuditLogType.ViewProduct
                        && acc.deleteFlag == false && p.ExhID == exhId
                        select new
                        {
                            ProductName = p.p_name,
                            Country = (from c in RepositoryContext.Countries where c.deleteFlag == false && c.ISO3Digit == acc.a_country select c.CountryName).FirstOrDefault(),
                            value = (from c in RepositoryContext.Audits
                                     join s in RepositoryContext.Accounts on c.UserId equals s.a_ID
                                     where s.showId == showId && c.showId == showId
                                     && c.LogType == AuditLogType.ViewProduct && s.a_ID == acc.a_ID
                                     && c.AlternativeId == p.p_ID && s.a_country == acc.a_country
                                     select c.AlternativeId).Count()
                        }).Distinct().ToList();
            //var result = main.OrderBy(x => x.Country).ThenBy(x => x.ProductName).GroupBy(x => new{ x.Country, x.AlternativeId })
            //.Select(b => new { data = b.Select(x => new { x.Country, x.ProductName, value = b.Count() }).Distinct() });
            return main;
        }

        private dynamic FilterSessionForProductlist(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                p_ID = default(string),
                ExhID = default(string),
                p_name = default(string),
                p_category = default(string),
                p_mainproductID = default(string),
                p_video = default(string),
                sororder = default(int?),
                URL = default(string),
                Description = default(string),
                seqNo = default(int?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "p_name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.p_name.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForProductlist(dynamic mainQuery, dynamic obj)
        {

            var tmpQuery = Enumerable.Repeat(new
            {
                p_ID = default(string),
                ExhID = default(string),
                p_name = default(string),
                p_category = default(string),
                p_mainproductID = default(string),
                p_video = default(string),
                sororder = default(int?),
                URL = default(string),
                Description = default(string),
                seqNo = default(int?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.p_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.p_ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }


    }
}