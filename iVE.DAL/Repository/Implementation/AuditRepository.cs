using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation {
    public class AuditRepository : RepositoryBase<tblAudit>, IAuditRepository {
        public AuditRepository (AppDB repositoryContext) : base (repositoryContext) {

        }
    }
}