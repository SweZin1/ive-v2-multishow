using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using iVE.DAL.Util;
using System;
using System.Collections.Generic;
namespace iVE.DAL.Repository.Implementation
{
    public class AdminRepository : RepositoryBase<tbl_Administrator>, IAdminRepository
    {
        public AdminRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
        public dynamic GetAdminBySuperAdmin(int id)
        {
            return (from a in RepositoryContext.Administrators
                    where a.admin_id == id && a.admin_isdeleted == false && a.IsSuperAdmin == false
                    select new
                    {
                        id = a.admin_id,
                        userName = a.admin_username,
                        name = a.admin_name,
                        adminRole = a.admin_role,
                        email = a.admin_email,
                        mobile = a.admin_mobile,
                        profilePic = a.admin_profilepic,
                        remark = a.admin_remark
                    }).FirstOrDefault();
        }

        public dynamic GetAdminListBySuperAdmin(dynamic obj, string showId)
        {
            string sortField = null; string sortBy = "";

            var main = (from a in RepositoryContext.Administrators
                        where a.admin_isdeleted == false && a.showId == showId && a.IsSuperAdmin == false
                        select new
                        {
                            id = a.admin_id,
                            userName = a.admin_username,
                            name = a.admin_name,
                            adminRole = a.admin_role,
                            email = a.admin_email,
                            mobile = a.admin_mobile,
                            updatedDate = a.admin_updateddate
                        }).AsNoTracking();

            if (obj.filter.filters != null)
            {
                main = FilterSessionForAdminList(main, obj);
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "updatedDate";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            var objTotal = main.Count();
            main = PaginationSessionForAdminList(main, obj);

            var objResult = main.AsNoTracking().ToList();
            return new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved." };
        }

        private dynamic FilterSessionForAdminList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                userName = default(string),
                name = default(string),
                adminRole = default(string),
                email = default(string),
                mobile = default(string),
                updatedDate = default(DateTime?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "Name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.name.ToLower().Contains(Name));

                        }
                    }

                    if (filterName == "Email")
                    {
                        string Email = filterValue;
                        if (!String.IsNullOrEmpty(Email))
                        {
                            Email = Email.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.email.ToLower().Contains(Email));

                        }
                    }

                    if (filterName == "Mobile")
                    {
                        string Mobile = filterValue;
                        if (!String.IsNullOrEmpty(Mobile))
                        {
                            Mobile = Mobile.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.mobile.ToLower().Contains(Mobile));

                        }
                    }
                }

                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForAdminList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                userName = default(string),
                name = default(string),
                adminRole = default(string),
                email = default(string),
                mobile = default(string),
                updatedDate = default(DateTime?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, int> pageDictionary = new Dictionary<int, int>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<int> PaginationFilterArray = new List<int> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public int AddAdminBySuperAdmin(dynamic adminObj)
        {
            tbl_Administrator newAdminObj = new tbl_Administrator();
            newAdminObj.admin_username = adminObj.userName;
            string password = adminObj.password;
            newAdminObj.admin_password = GlobalFunctionForDAL.GenerateSalthKey(password);
            newAdminObj.admin_name = adminObj.name;
            newAdminObj.admin_role = adminObj.adminRole;
            newAdminObj.admin_remark = adminObj.remark;
            newAdminObj.admin_email = adminObj.email;
            newAdminObj.admin_mobile = adminObj.mobile;
            newAdminObj.showId = adminObj.showId;
            newAdminObj.IsSuperAdmin = false;
            newAdminObj.admin_isdeleted = false;
            newAdminObj.admin_createddate = System.DateTime.Now;
            newAdminObj.admin_updateddate = System.DateTime.Now;
            RepositoryContext.Administrators.Add(newAdminObj);
            RepositoryContext.SaveChanges();
            return newAdminObj.admin_id;
        }

        public int UpdateAdminBySuperAdmin(dynamic adminObj)
        {
            int adminId = adminObj.id;
            tbl_Administrator updataAdminObj = RepositoryContext.Administrators.Find(adminId);
            updataAdminObj.admin_username = adminObj.userName;
            updataAdminObj.admin_name = adminObj.name;
            updataAdminObj.admin_role = adminObj.adminRole;
            updataAdminObj.admin_remark = adminObj.remark;
            updataAdminObj.admin_email = adminObj.email;
            updataAdminObj.admin_mobile = adminObj.mobile;
            updataAdminObj.admin_updateddate = System.DateTime.Now;
            RepositoryContext.Administrators.Update(updataAdminObj);
            RepositoryContext.SaveChanges();
            return adminId;
        }

        public void UpdateAdminProfile(int id, string url)
        {
            tbl_Administrator updataAdminObj = RepositoryContext.Administrators.Find(id);
            updataAdminObj.admin_profilepic = url;
            updataAdminObj.admin_updateddate = System.DateTime.Now;
            RepositoryContext.Administrators.Update(updataAdminObj);
            RepositoryContext.SaveChanges();
        }

        public bool DeleteAdminBySuperAdmin(int id)
        {
            tbl_Administrator deleteAdminObj = RepositoryContext.Administrators.Find(id);
            deleteAdminObj.admin_isdeleted = true;
            deleteAdminObj.admin_updateddate = System.DateTime.Now;
            RepositoryContext.Administrators.Update(deleteAdminObj);
            RepositoryContext.SaveChanges();
            return true;
        }

        public int CheckDuplicateForAdmin(dynamic adminObj, bool isNew)
        {
            int result = 0;
            string email = adminObj.email;
            string mobile = adminObj.mobile;
            string userName = adminObj.userName;
            string showId = adminObj.showId;
            if (isNew)
            {
                var emailQuery = (from a in RepositoryContext.Administrators where a.admin_email == email && a.showId == showId && a.admin_isdeleted == false && a.IsSuperAdmin == false select a);
                if (emailQuery.Count() > 0)
                {
                    result = 1;
                    return result;
                }

                var mobileQuery = (from a in RepositoryContext.Administrators where a.admin_mobile == mobile && a.showId == showId && a.admin_isdeleted == false && a.IsSuperAdmin == false select a);
                if (mobileQuery.Count() > 0)
                {
                    result = 2;
                    return result;
                }

                var userNameQuery = (from a in RepositoryContext.Administrators where a.admin_username == userName && a.showId == showId && a.admin_isdeleted == false && a.IsSuperAdmin == false select a);
                if (userNameQuery.Count() > 0)
                {
                    result = 3;
                    return result;
                }
            }
            else
            {
                int adminId = adminObj.id;
                var emailQuery = (from a in RepositoryContext.Administrators where a.admin_email == email && a.showId == showId && a.admin_id != adminId && a.admin_isdeleted == false && a.IsSuperAdmin == false select a);
                if (emailQuery.Count() > 0)
                {
                    result = 1;
                    return result;
                }

                var mobileQuery = (from a in RepositoryContext.Administrators where a.admin_mobile == mobile && a.showId == showId && a.admin_id != adminId && a.admin_isdeleted == false && a.IsSuperAdmin == false select a);
                if (mobileQuery.Count() > 0)
                {
                    result = 2;
                    return result;
                }

                var userNameQuery = (from a in RepositoryContext.Administrators where a.admin_username == userName && a.showId == showId && a.admin_id != adminId && a.admin_isdeleted == false && a.IsSuperAdmin == false select a);
                if (userNameQuery.Count() > 0)
                {
                    result = 3;
                    return result;
                }
            }
            return result;
        }

        public void ChangeAdminPassword(dynamic adminObj)
        {
            int adminId = adminObj.id;
            tbl_Administrator updataAdminObj = RepositoryContext.Administrators.Find(adminId);
            string password = adminObj.password;
            updataAdminObj.admin_password = GlobalFunctionForDAL.GenerateSalthKey(password);
            updataAdminObj.admin_updateddate = System.DateTime.Now;
            RepositoryContext.Administrators.Update(updataAdminObj);
            RepositoryContext.SaveChanges();
        }
    }
}