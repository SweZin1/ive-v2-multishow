using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using System.Globalization;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation
{
    public class AppointmentRepository : RepositoryBase<tbl_Appointments>, IAppointmentRepository
    {
        private string showId = LoginData.showId;
        public AppointmentRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
        public dynamic GetActiveEventDate()
        {
            return RepositoryContext.Days.Where(x => x.showId == showId && x.deleteFlag == false).Select(x => x.d_ID).ToList();
        }
        // Update Time Slots
        public dynamic CreateTimeSlots(dynamic days, dynamic starttimes, int duration, string prefix, int currentNo, int runno_incr)
        {
            bool isOk = false;
            try
            {
                foreach (var day in days)
                {
                    foreach (var startTime in starttimes)
                    {
                        DateTime tmpDateTime = DateTime.ParseExact(startTime, "HH:mm:ss", CultureInfo.InvariantCulture);
                        DateTime tmpEndDateTime = tmpDateTime.AddMinutes(duration);

                        tbl_App_TimeSlot newTimeSlot = new tbl_App_TimeSlot();
                        newTimeSlot.showId = showId;
                        newTimeSlot.TimeSlotID = prefix + currentNo;
                        newTimeSlot.StartTime = startTime;
                        newTimeSlot.EndTime = tmpEndDateTime.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
                        newTimeSlot.Day = day;
                        newTimeSlot.TimeSlotGroupType = TimeSlotGroupType.Default;
                        newTimeSlot.CreatedDate = System.DateTime.Now;
                        newTimeSlot.UpdatedDate = System.DateTime.Now.ToString();
                        newTimeSlot.deleteFlag = false;
                        RepositoryContext.AppTimeSlot.Add(newTimeSlot);
                        currentNo = currentNo + runno_incr;
                    }
                }
                RepositoryContext.SaveChanges();
                isOk = true;
            }
            catch
            {
                isOk = true;
            }
            return isOk;
        }
        // Get Schedule
        public dynamic GetUserSchedule(string ownerId, string userId, string curretnUserType)
        {

            //Owner View
            //Block TimeSlot By Organization
            var blockQuery = (from app in RepositoryContext.Appointments
                              join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID into st
                              from stmp in st.DefaultIfEmpty()
                              where app.showId == showId && app.deleteFlag == false
                              && app.AppType == AppointmentCondition.BlockUser
                              && (app.BookingType == AppointmentBookingType.OrganizerBlock || ((app.BookingType == AppointmentBookingType.ExhibitorBlock && stmp.OwnerID == ownerId && curretnUserType != AppointmentUserType.Organizer)))
                              select new
                              {
                                  AppointmentID = app.AppointmentID,
                                  TimeSlotID = app.TimeSlotID,
                                  Status = AppointmentStatus.Block,//(from us in RepositoryContext.Schedules where s.deleteFlag == false && s.OwnerID == userId && s.AppointmentID == app.AppointmentID && app.AppType == AppointmentCondition.AppointmentConfirmed select us).Count() > 0 ? AppointmentStatus.Confirmed : AppointmentStatus.Block,
                                  Description = app.Description,
                                  BookingType = app.BookingType,
                                  Condition = "Block Appointment"
                              }).Distinct();

            //Appointment Status
            var appointmentQuery = (from app in RepositoryContext.Appointments
                                    join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID
                                    where app.showId == showId && app.deleteFlag == false && s.deleteFlag == false && s.OwnerID == ownerId && app.AppType == AppointmentCondition.AppointmentConfirmed
                                    select new
                                    {
                                        AppointmentID = app.AppointmentID,
                                        TimeSlotID = app.TimeSlotID,
                                        Status = (from us in RepositoryContext.Schedules where us.deleteFlag == false && us.OwnerID == userId && us.AppointmentID == app.AppointmentID select us).Count() > 0 ? AppointmentStatus.Confirmed : AppointmentStatus.Block,
                                        //AppointmentStatus.Confirmed,
                                        Description = app.Description,
                                        BookingType = app.BookingType,//"You have an appointment with "
                                        Condition = (from us in RepositoryContext.Schedules where us.deleteFlag == false && us.OwnerID == userId && us.AppointmentID == app.AppointmentID select us).Count() > 0 ? "Appointment" : "Block Appointment"
                                        //"Appointment"
                                    }).Distinct();

            //Request Status
            var requestQuery = (from req in RepositoryContext.AppRequest
                                where req.showId == showId && req.deleteFlag == false && req.RequestFrom == userId && req.RequestTo == ownerId && req.Status == RequestCondition.InProcess
                                select new
                                {
                                    AppointmentID = "",
                                    TimeSlotID = req.TimeSlotID,
                                    Status = AppointmentStatus.Requested,
                                    Description = "",
                                    BookingType = req.BookingType,
                                    Condition = "Request"
                                }).Distinct();

            //User View
            var busyAppointTimeSlot = (from app in RepositoryContext.Appointments
                                       join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID
                                       where app.showId == showId && app.deleteFlag == false && s.deleteFlag == false && s.OwnerID == userId && (app.AppType == AppointmentCondition.AppointmentConfirmed || (app.AppType == AppointmentCondition.BlockUser && app.BookingType == AppointmentBookingType.VisitorBlock))
                                       select app.TimeSlotID);

            var busyRequestTimeSlot = (from req in RepositoryContext.AppRequest
                                       where req.showId == showId && req.deleteFlag == false && (req.RequestFrom == userId || req.RequestTo == userId) && req.Status == RequestCondition.InProcess
                                       select req.TimeSlotID);

            if (busyAppointTimeSlot.Count() > 0)
            {
                if (busyRequestTimeSlot.Count() > 0)
                {
                    busyAppointTimeSlot = busyAppointTimeSlot.Concat(busyRequestTimeSlot);
                }
            }
            else
            {
                busyAppointTimeSlot = busyRequestTimeSlot;
            }

            var busyTimeSlotArray = busyAppointTimeSlot.Distinct().ToArray();

            var timeSlotQuery = (from ts in RepositoryContext.AppTimeSlot
                                 join d in RepositoryContext.Days on ts.Day equals d.d_ID
                                 join bObject in blockQuery on ts.TimeSlotID equals bObject.TimeSlotID into b
                                 from bt in b.DefaultIfEmpty()
                                 join appObject in appointmentQuery on ts.TimeSlotID equals appObject.TimeSlotID into t
                                 from at in t.DefaultIfEmpty()
                                 join reqObject in requestQuery on ts.TimeSlotID equals reqObject.TimeSlotID into r
                                 from rt in r.DefaultIfEmpty()
                                 where ts.showId == showId && ts.deleteFlag == false
                                 orderby ts.Day, ts.TimeSlotID
                                 select new
                                 {
                                     Day = d.d_date,
                                     data = new
                                     {
                                         ts.TimeSlotID,
                                         ts.StartTime,
                                         ts.EndTime,
                                         AppointmentID = (bt.Status != null ? bt.AppointmentID : (at.Status != null ? at.AppointmentID : "")),//at.AppointmentID != null ? at.AppointmentID : "",
                                         Description = (bt.Status != null ? bt.Description : (at.Status != null ? at.Description : (rt.Status != null ? rt.Description : ""))),//(at.Description != null ? at.Description : (rt.Description != null ? rt.Description : "")),
                                         Status = (bt.Status != null ? bt.Status : (at.Status != null ? at.Status : (rt.Status != null ? rt.Status : AppointmentStatus.Available))),//(at.Status != null ? at.Status : (rt.Status != null ? rt.Status : AppointmentStatus.Available))
                                         Type = (bt.Status != null ? bt.BookingType : (at.Status != null ? at.BookingType : (rt.Status != null ? rt.BookingType : ""))),//(at.BookingType != null ? at.BookingType : (rt.BookingType != null ? rt.BookingType : ""))
                                         Condition = (bt.Status != null ? bt.Condition : (at.Status != null ? at.Condition : (rt.Status != null ? rt.Condition : ""))),//(at.Condition != null ? at.Condition : (rt.Condition != null ? rt.Condition : ""))
                                         isBusy = busyTimeSlotArray.Contains(ts.TimeSlotID)
                                     }
                                 }).AsNoTracking().Distinct().ToList();

            var mainQuery = timeSlotQuery.GroupBy(
                            p => p.Day,
                            p => p.data,
                            (key, g) => new { Day = key, Data = g.ToList() });
            return mainQuery;
        }

        // Get My Schedule
        public dynamic GetUserOwnSchedule(string ownerId)
        {
            string UserType = GetAppointmentUserType(ownerId);
            string BlockUserType = "";

            if (UserType == AppointmentUserType.Exhibitor)
            {
                BlockUserType = AppointmentBookingType.ExhibitorBlock;
            }

            if (UserType == AppointmentUserType.Visitor)
            {
                BlockUserType = AppointmentBookingType.VisitorBlock;
            }

            //Owner View
            //Block TimeSlot By Organization
            var blockQuery = (from app in RepositoryContext.Appointments
                              join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID into st
                              from stmp in st.DefaultIfEmpty()
                              where app.showId == showId
                              && app.deleteFlag == false && app.AppType == AppointmentCondition.BlockUser
                              && (app.BookingType == AppointmentBookingType.OrganizerBlock || ((app.BookingType == BlockUserType && stmp.OwnerID == ownerId)))
                              //where app.deleteFlag == false && app.AppType == AppointmentCondition.BlockUser
                              select new
                              {
                                  AppointmentID = app.AppointmentID,
                                  TimeSlotID = app.TimeSlotID,
                                  Status = AppointmentStatus.Block,//(from us in RepositoryContext.Schedules where s.deleteFlag == false && s.OwnerID == userId && s.AppointmentID == app.AppointmentID && app.AppType == AppointmentCondition.AppointmentConfirmed select us).Count() > 0 ? AppointmentStatus.Confirmed : AppointmentStatus.Block,
                                  Description = app.Description,
                                  BookingType = app.BookingType,
                                  Condition = "Block Appointment"
                              }).Distinct();


            //Appointment Status
            var appointmentQuery = (from app in RepositoryContext.Appointments
                                    join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID
                                    where app.showId == showId && app.deleteFlag == false && s.deleteFlag == false
                                    && s.OwnerID == ownerId && app.AppType == AppointmentCondition.AppointmentConfirmed
                                    select new
                                    {
                                        AppointmentID = app.AppointmentID,
                                        TimeSlotID = app.TimeSlotID,
                                        Status = AppointmentStatus.Confirmed,
                                        Description = (from us in RepositoryContext.Schedules.Where(x => x.OwnerID != ownerId && x.deleteFlag == false && x.AppointmentID == app.AppointmentID)
                                                       join usr in RepositoryContext.Accounts.Where(x => x.deleteFlag == false) on us.OwnerID equals usr.a_ID
                                                       select usr.a_fullname).FirstOrDefault(),//app.Description,
                                        BookingType = app.BookingType,
                                        Condition = "Appointment"
                                    }).Distinct();

            //Request Status
            var requestQuery = (from req in RepositoryContext.AppRequest
                                join acc in (from a in RepositoryContext.Accounts
                                             join exh in RepositoryContext.Exhibitors on a.ExhId equals exh.ExhID into ex
                                             from extmp in ex.DefaultIfEmpty()
                                             where a.showId == showId
                                             select new
                                             {
                                                 a.a_ID,
                                                 name = a.a_fullname,
                                                 extmp.CompanyName
                                             }) on req.RequestTo equals acc.a_ID

                                where req.showId == showId
                                && req.deleteFlag == false && req.RequestFrom == ownerId
                                && req.Status == RequestCondition.InProcess
                                select new
                                {
                                    AppointmentID = "",
                                    TimeSlotID = req.TimeSlotID,
                                    Status = AppointmentStatus.Requested,
                                    Description = acc.name + (acc.CompanyName == null ? "" : " ( " + acc.CompanyName + " )"),
                                    BookingType = req.BookingType,
                                    Condition = "Request"
                                }).Distinct();

            var timeSlotQuery = (from ts in RepositoryContext.AppTimeSlot
                                 join d in RepositoryContext.Days on ts.Day equals d.d_ID
                                 join bObject in blockQuery on ts.TimeSlotID equals bObject.TimeSlotID into b
                                 from bt in b.DefaultIfEmpty()
                                 join appObject in appointmentQuery on ts.TimeSlotID equals appObject.TimeSlotID into t
                                 from at in t.DefaultIfEmpty()
                                 join reqObject in requestQuery on ts.TimeSlotID equals reqObject.TimeSlotID into r
                                 from rt in r.DefaultIfEmpty()
                                 where ts.showId == showId && d.showId == showId && ts.deleteFlag == false
                                 orderby ts.Day, ts.TimeSlotID
                                 select new
                                 {
                                     Day = d.d_date,
                                     data = new
                                     {
                                         ts.TimeSlotID,
                                         ts.StartTime,
                                         ts.EndTime,
                                         AppointmentID = (bt.Status != null ? bt.AppointmentID : (at.Status != null ? at.AppointmentID : "")),
                                         Description = (bt.Status != null ? bt.Description : (at.Status != null ? at.Description : (rt.Status != null ? rt.Description : ""))),
                                         Status = (bt.Status != null ? bt.Status : (at.Status != null ? at.Status : (rt.Status != null ? rt.Status : AppointmentStatus.Available))),
                                         Type = (bt.Status != null ? bt.BookingType : (at.Status != null ? at.BookingType : (rt.Status != null ? rt.BookingType : ""))),
                                         Condition = (bt.Status != null ? bt.Condition : (at.Status != null ? at.Condition : (rt.Status != null ? rt.Condition : ""))),
                                         requestCount = (from req in RepositoryContext.AppRequest
                                                         where req.deleteFlag == false && req.RequestTo == ownerId && ts.TimeSlotID == req.TimeSlotID && req.Status == RequestCondition.InProcess
                                                         select req.TimeSlotID).Count()
                                     }
                                 }).AsNoTracking().Distinct().ToList();

            var mainQuery = timeSlotQuery.GroupBy(
                            p => p.Day,
                            p => p.data,
                            (key, g) => new { Day = key, Data = g.ToList() });
            return mainQuery;
        }

        public dynamic GetUserScheduleInfo(string userId, string timeSlotId)
        {
            List<string> activeStatus = new List<string>();
            var tmpQuery = Enumerable.Repeat(new
            {
                userId = default(string),
                status = default(string),

            }, 0);

            var busyAppointTimeSlot = (from mainApp in (from app in RepositoryContext.Appointments
                                                        join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID
                                                        where app.showId == showId && app.deleteFlag == false && s.deleteFlag == false && s.OwnerID == userId && app.TimeSlotID == timeSlotId && app.AppType == AppointmentCondition.AppointmentConfirmed
                                                        select app)
                                       join user in (
                                   from os in RepositoryContext.Schedules
                                   where os.showId == showId && os.deleteFlag == false && os.OwnerID != userId
                                   select os
                                    ) on mainApp.AppointmentID equals user.AppointmentID
                                       select new
                                       {
                                           userId = user.OwnerID,
                                           status = AppointmentStatus.Confirmed
                                       });

            if (busyAppointTimeSlot.Count() > 0)
            {
                activeStatus.Add(AppointmentStatus.Confirmed);
                tmpQuery = busyAppointTimeSlot;
            }

            var busyRequestTimeSlot = (from req in RepositoryContext.AppRequest
                                       where req.showId == showId && req.deleteFlag == false && req.RequestFrom == userId && req.TimeSlotID == timeSlotId && req.Status == RequestCondition.InProcess
                                       select new
                                       {
                                           userId = req.RequestFrom,
                                           status = AppointmentStatus.Requested
                                       });

            if (busyRequestTimeSlot.Count() > 0)
            {
                activeStatus.Add(AppointmentStatus.Requested);
                if (tmpQuery.Count() > 0)
                {
                    tmpQuery = tmpQuery.Concat(busyRequestTimeSlot);
                }
                else
                {
                    tmpQuery = busyRequestTimeSlot;
                }
            }

            var busyReponseTimeSlot = (from req in RepositoryContext.AppRequest
                                       where req.showId == showId && req.deleteFlag == false && req.RequestTo == userId && req.TimeSlotID == timeSlotId && req.Status == RequestCondition.InProcess
                                       select new
                                       {
                                           userId = req.RequestFrom,
                                           status = AppointmentStatus.ReceivedRequest
                                       });

            if (busyReponseTimeSlot.Count() > 0)
            {
                activeStatus.Add(AppointmentStatus.ReceivedRequest);
                if (tmpQuery.Count() > 0)
                {
                    tmpQuery = tmpQuery.Concat(busyReponseTimeSlot);
                }
                else
                {
                    tmpQuery = busyReponseTimeSlot;
                }
            }


            var userData = (from a in RepositoryContext.Accounts
                            join mc in tmpQuery on a.a_ID equals mc.userId
                            join exh in RepositoryContext.Exhibitors on a.ExhId equals exh.ExhID into ex
                            from extmp in ex.DefaultIfEmpty()
                            where a.deleteFlag == false && a.showId == showId
                            select new
                            {
                                userId = a.a_ID,
                                name = a.a_fullname,
                                extmp.CompanyName,
                                status = mc.status
                            }).ToList();

            return new { contact = userData, activeStatus = activeStatus };
        }

        public dynamic GetOwnerRequest(string ownerId, string timeSlotId)
        {
            var requestQuery = (from req in RepositoryContext.AppRequest
                                join acc in (from a in RepositoryContext.Accounts
                                             join exh in RepositoryContext.Exhibitors on a.ExhId equals exh.ExhID into ex
                                             from extmp in ex.DefaultIfEmpty()
                                             where a.showId == showId
                                             select new
                                             {
                                                 a.a_ID,
                                                 name = a.a_fullname,
                                                 extmp.CompanyName
                                             }) on req.RequestFrom equals acc.a_ID
                                where req.showId == showId && req.deleteFlag == false && req.RequestFrom == ownerId
                                && req.TimeSlotID == timeSlotId && req.Status == RequestCondition.InProcess
                                select new
                                {
                                    userId = req.RequestFrom,
                                    status = AppointmentStatus.Requested,
                                    name = acc.name,
                                    companyName = acc.CompanyName,
                                    requestDate = req.RequestDateTime
                                }).Distinct().ToList();
            return requestQuery;
        }

        public dynamic GetOwnerReceivedRequestList(string ownerId, string timeSlotId, dynamic obj, string sortField, string sortBy)
        {
            dynamic result;
            var mainQuery = (from req in RepositoryContext.AppRequest
                             join acc in (from a in RepositoryContext.Accounts
                                          where a.showId == showId
                                          select new
                                          {
                                              a.a_ID,
                                              name = a.a_fullname,
                                              email = a.a_email
                                          }) on req.RequestFrom equals acc.a_ID
                             where req.showId == showId && req.deleteFlag == false
                             && req.RequestTo == ownerId && req.TimeSlotID == timeSlotId
                             && req.Status == RequestCondition.InProcess
                             select new
                             {
                                 userId = req.RequestFrom,
                                 status = AppointmentStatus.ReceivedRequest,
                                 name = acc.name,
                                 email = acc.email,
                                 requestDate = req.RequestDateTime
                             }).Distinct();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterForReceivedRequest(mainQuery, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            mainQuery = PaginationForReceivedRequest(mainQuery, obj);

            var objResult = mainQuery.ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal };
            return result;
        }

        private dynamic FilterForReceivedRequest(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                userId = default(string),
                status = default(string),
                name = default(string),
                email = default(string),
                requestDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "userId")
                    {
                        string UserId = filterValue;
                        if (!String.IsNullOrEmpty(UserId))
                        {
                            UserId = UserId.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.userId == UserId);

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationForReceivedRequest(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                userId = default(string),
                status = default(string),
                name = default(string),
                email = default(string),
                requestDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.userId);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.userId) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public dynamic CreateRequest(dynamic obj, string userType)
        {
            bool result = false;
            try
            {
                tbl_App_Request newRequest = new tbl_App_Request();

                newRequest.RequestFrom = obj.requestFrom;
                newRequest.RequestTo = obj.requestTo;
                newRequest.TimeSlotID = obj.timeSlotId;
                newRequest.Status = RequestCondition.InProcess;
                newRequest.showId = showId;
                if (userType == AppointmentUserType.Exhibitor)
                {
                    newRequest.BookingType = RequestBookingType.ExhibitorRequst;
                }

                if (userType == AppointmentUserType.Visitor)
                {
                    newRequest.BookingType = RequestBookingType.VisterRequest;
                }
                newRequest.RequestDateTime = DateTime.Now;
                newRequest.CreatedDate = DateTime.Now;
                newRequest.deleteFlag = false;
                RepositoryContext.AppRequest.Add(newRequest);
                RepositoryContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                result = false;
            }
            return result;
        }

        public dynamic UpdateRequest(dynamic status, tbl_App_Request requestObj)
        {
            bool isOK = false;
            try
            {
                if (status == RequestCondition.Completed)
                {
                    requestObj.Status = RequestCondition.Completed;
                    requestObj.Action = RequestAction.Accept;
                }
                else if (status == RequestCondition.Cancel)
                {
                    requestObj.Status = RequestCondition.Cancel;
                }
                else if (status == RequestCondition.Reject)
                {
                    requestObj.Status = RequestCondition.Reject;
                }
                requestObj.showId = showId;
                requestObj.deleteFlag = true;
                requestObj.ActionTime = DateTime.Now;
                RepositoryContext.AppRequest.Update(requestObj);
                RepositoryContext.SaveChanges();
                isOK = true;
            }
            catch
            {
                isOK = false;
            }
            return isOK;
        }

        public dynamic GetAppointmentUserType(string userId)
        {
            string userType = "";
            var accountUserQuery = RepositoryContext.Accounts.Where(x => x.showId == showId && x.a_ID == userId && x.deleteFlag == false).FirstOrDefault();

            var admistratorQuery = RepositoryContext.Administrators.Where(x => x.showId == showId && (x.admin_id.ToString()) == userId && x.IsSuperAdmin == false && x.admin_isdeleted == false).FirstOrDefault();

            if (admistratorQuery != null)
            {
                userType = AppointmentUserType.Organizer;
            }

            if (accountUserQuery != null)
            {
                if (accountUserQuery.a_type == ContactPersonType.MainExhibitor || accountUserQuery.a_type == ContactPersonType.ContactPerson)
                {
                    userType = AppointmentUserType.Exhibitor;
                }
                else
                {
                    userType = AppointmentUserType.Visitor;
                }
            }

            return userType;
        }

        public dynamic GetActiveRequest(string requestFrom, string requestTo, string timeSlotId)
        {
            return RepositoryContext.AppRequest.Where(x => x.showId == showId && x.RequestFrom == requestFrom && x.RequestTo == requestTo && x.TimeSlotID == timeSlotId && x.deleteFlag == false && x.Status == RequestCondition.InProcess).FirstOrDefault();
        }

        public dynamic GetActiveRequest(string requestFrom, string timeSlotId)
        {
            return RepositoryContext.AppRequest.Where(x => x.showId == showId && x.RequestFrom == requestFrom && x.TimeSlotID == timeSlotId && x.deleteFlag == false && x.Status == RequestCondition.InProcess).FirstOrDefault();
        }

        public dynamic RejectAllReceivedRequest(string requestFrom, string requestTo, string timeSlotId)
        {
            bool isOk = false;
            try
            {
                ((from r in RepositoryContext.AppRequest
                  where r.showId == showId && r.RequestFrom != requestFrom
&& r.RequestTo == requestTo && r.TimeSlotID == timeSlotId
&& r.deleteFlag == false
&& r.Status == RequestCondition.InProcess
                  select r).ToList())
                .ForEach(x => { x.deleteFlag = true; x.Status = RequestCondition.Cancel; });
                RepositoryContext.SaveChanges();
                isOk = true;
            }
            catch
            {
                isOk = false;
            }
            return isOk;
        }

        public dynamic CreateAppointment(string appointmentId, string timeSlotId, string appType, string bookingType, string tokenUser, string description)
        {
            bool result = false;
            try
            {
                tbl_Appointments newAppointment = new tbl_Appointments();
                newAppointment.AppointmentID = appointmentId;
                newAppointment.showId = showId;
                newAppointment.TimeSlotID = timeSlotId;
                newAppointment.AppType = appType;
                newAppointment.BookingType = bookingType;
                newAppointment.ConfirmDate = DateTime.Now;
                newAppointment.CreatedDate = DateTime.Now;
                newAppointment.deleteFlag = false;
                newAppointment.CreatedBy = tokenUser;
                newAppointment.ModifiedDate = DateTime.Now;
                newAppointment.Description = description;
                RepositoryContext.Appointments.Add(newAppointment);
                RepositoryContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                result = false;
            }
            return result;
        }

        public dynamic UpdateAppointment(tbl_Appointments appointmentObj)
        {
            bool result = false;
            try
            {
                appointmentObj.deleteFlag = true;
                RepositoryContext.Appointments.Update(appointmentObj);
                RepositoryContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                result = false;
            }
            return result;
        }

        public dynamic CreateMySchedule(string appointmentId, string ownerId)
        {
            bool result = false;
            try
            {
                tbl_App_MySchedule newMySchedule = new tbl_App_MySchedule();
                newMySchedule.OwnerID = ownerId;
                newMySchedule.showId = showId;
                newMySchedule.AppointmentID = appointmentId;
                newMySchedule.CreatedDate = DateTime.Now;
                newMySchedule.deleteFlag = false;
                RepositoryContext.Schedules.Add(newMySchedule);
                RepositoryContext.SaveChanges();
                result = true;
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public dynamic UpdateAllMyScheduleByAppointmentId(string appointmentId)
        {
            bool result = false;
            try
            {
                ((from r in RepositoryContext.Schedules
                  where r.showId == showId && r.AppointmentID == appointmentId && r.deleteFlag == false
                  select r).ToList())
                .ForEach(x => x.deleteFlag = true);
                RepositoryContext.SaveChanges();
                result = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                result = false;
            }
            return result;
        }

        public dynamic IsFreeAtRequestedTime(string timeSlotId, string userId)
        {
            bool isFree = true;
            var appQuery = (from app in RepositoryContext.Appointments
                            join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID
                            where app.showId == showId &&
                            app.deleteFlag == false && s.deleteFlag == false
                            && s.OwnerID == userId && app.TimeSlotID == timeSlotId
                            select app);
            if (appQuery.Count() > 0)
            {
                var tst = appQuery.Count();
                isFree = false;
            }
            return isFree;
        }

        public dynamic GetActiveAppointment(string timeSlotId, string userId)
        {
            return (from app in RepositoryContext.Appointments
                    join s in RepositoryContext.Schedules on app.AppointmentID equals s.AppointmentID
                    where app.showId == showId
                    && app.deleteFlag == false && s.deleteFlag == false
                    && s.OwnerID == userId && app.TimeSlotID == timeSlotId
                    select app).FirstOrDefault();
        }

        public dynamic GetTimeSlotDetail(string timeSlotId)
        {
            return (from t in RepositoryContext.AppTimeSlot
                    join d in RepositoryContext.Days on t.Day equals d.d_ID
                    where t.deleteFlag == false && t.TimeSlotID == timeSlotId
                    select new
                    {
                        day = d.d_date,
                        st = t.StartTime,
                        et = t.EndTime
                    }).FirstOrDefault();
        }

        public dynamic GetDay(string dateId)
        {
            return (from d in RepositoryContext.Days where d.d_ID == dateId && d.deleteFlag == false select d).FirstOrDefault();
        }
    }
}