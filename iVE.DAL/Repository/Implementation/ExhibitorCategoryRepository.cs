using System;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;

namespace iVE.DAL.Repository.Implementation
{
    public class ExhibitorCategoryRepository: RepositoryBase<tblExhibitorCategory>, IExhibitorCategoryRepository
    {
        private string showId = LoginData.showId;
        public ExhibitorCategoryRepository(AppDB repositoryContext): base(repositoryContext)
        {
            
        }

        public dynamic AddNewExhibitorCategory(dynamic obj)
        {
            try
            {
                var exhCategory = new tblExhibitorCategory();
                exhCategory.showId = showId;
                exhCategory.Category = obj.category;
                exhCategory.deleteFlag = false;
                exhCategory.CreatedDate = DateTime.Now;
                RepositoryContext.ExhibitorCategories.Add(exhCategory);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public dynamic deleteExhibitorCategory(tblExhibitorCategory exhCategory)
        {
            try
            {
                exhCategory.deleteFlag = true;
                exhCategory.UpdatedDate = DateTime.Now;
                RepositoryContext.ExhibitorCategories.Update(exhCategory);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public dynamic getExhibitorCategories()
        {
            var main = (from c in RepositoryContext.ExhibitorCategories
                        where c.showId == showId && c.deleteFlag == false
                        select new
                        {
                            ID = c.ID.ToString(),
                            c.Category
                        }).AsNoTracking().ToList();
            return main;
        }

        public dynamic getExhibitorCategories(dynamic obj)
        {
            string sortField = null; string sortBy = "";
            var main = (from c in RepositoryContext.ExhibitorCategories
                        where c.deleteFlag == false && c.showId == showId
                        select new
                        {
                            id = c.ID,
                            c.Category,
                            createdDate = c.CreatedDate
                        }).AsNoTracking();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSession(main, obj);
                }
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "createdDate";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            var objTotal = main.Count();
            main = PaginationSession(main, obj);

            var objResult = main.AsNoTracking().ToList();
            return new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved." };
        }

        public dynamic getExhibitorCategoryDetails(int Id)
        {
             var main = (from c in RepositoryContext.ExhibitorCategories
            where c.deleteFlag == false && c.showId == showId && c.ID == Id
            select c).FirstOrDefault();
            return main;
        }

        public dynamic UpdateExhibitorCategory(dynamic obj, tblExhibitorCategory exhCategory)
        {
            try
            {
                exhCategory.Category = obj.category;
                exhCategory.UpdatedDate = DateTime.Now;
                RepositoryContext.ExhibitorCategories.Update(exhCategory);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private dynamic FilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                Category = default(string),
                createdDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "Name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.Category.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                Category = default(string),
                createdDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id.ToString());
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }
                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id.ToString()) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
    }
}