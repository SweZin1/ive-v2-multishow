using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using iVE.DAL.Util;
using System;

namespace iVE.DAL.Repository.Implementation
{
    public class TemplateConfigRepository : RepositoryBase<tbl_TemplateConfig>, ITemplateConfigRepository
    {
        private string showId = LoginData.showId;
        public TemplateConfigRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetTemplate(dynamic obj)
        {
            var main = (from t in RepositoryContext.tbl_TemplateConfigs
                        where t.showId == showId && t.deleteFlag == false && t.status == TemplateConfigStatus.Publish
                        select new
                        {
                            id = t.Id,
                            title = t.title,
                            url = t.url,
                            videoUrl = t.videoUrl,
                            mediaType = t.mediaType,
                            templateType = t.templateType,
                            blockUserType = t.blockUserType,
                            category = t.category,
                            sequenceNo = t.sequenceNo,
                            configData = t.configData,
                            status = t.status
                        }).AsNoTracking();

            string category = obj.category;
            if (!string.IsNullOrEmpty(category))
            {
                main = main.Where(x => x.category == category);
            }

            string templateType = obj.templateType;
            if (!string.IsNullOrEmpty(templateType))
            {
                main = main.Where(x => x.templateType == templateType);
            }

            string sequenceNo = obj.sequenceNo;
            if (!string.IsNullOrEmpty(sequenceNo))
            {
                int seq = Convert.ToInt32(sequenceNo);
                main = main.Where(x => x.sequenceNo == seq);
            }

            return main.FirstOrDefault();
        }
        public dynamic GetTemplateList(dynamic obj)
        {
            var main = (from t in RepositoryContext.tbl_TemplateConfigs
                        where t.showId == showId && t.deleteFlag == false
                        select new
                        {
                            id = t.Id,
                            title = t.title,
                            url = t.url,
                            videoUrl = t.videoUrl,
                            mediaType = t.mediaType,
                            templateType = t.templateType,
                            blockUserType = t.blockUserType,
                            category = t.category,
                            sequenceNo = t.sequenceNo,
                            configData = t.configData,
                            status = t.status
                        }).AsNoTracking();

            string category = obj.category;
            if (!string.IsNullOrEmpty(category))
            {
                main = main.Where(x => x.category == category);
            }

            string templateType = obj.templateType;
            if (!string.IsNullOrEmpty(templateType))
            {
                main = main.Where(x => x.templateType == templateType);
            }

            string sequenceNo = obj.sequenceNo;
            if (!string.IsNullOrEmpty(sequenceNo))
            {
                int seq = Convert.ToInt32(sequenceNo);
                main = main.Where(x => x.sequenceNo == seq);
            }

            string status = obj.status;
            if (!string.IsNullOrEmpty(status))
            {
                main = main.Where(x => x.status == status);
            }

            return main.ToList().OrderBy(x => x.templateType).ThenBy(x => x.sequenceNo);
        }

        public int AddTemplateConfig(dynamic templateData)
        {
            int sequenceNo = 0;
            if (templateData.sequenceNo != null)
            {
                sequenceNo = Convert.ToInt32(templateData.sequenceNo);
            }
            tbl_TemplateConfig newTemplateConfig = new tbl_TemplateConfig();
            newTemplateConfig.title = templateData.title;
            newTemplateConfig.templateType = templateData.templateType;
            newTemplateConfig.showId = showId;
            // newTemplateConfig.url = templateData.url;
            // newTemplateConfig.videoUrl = templateData.videoUrl;
            newTemplateConfig.mediaType = templateData.mediaType;
            newTemplateConfig.blockUserType = templateData.blockUserType;
            newTemplateConfig.category = templateData.category;
            newTemplateConfig.sequenceNo = sequenceNo;
            newTemplateConfig.configData = templateData.configData;
            newTemplateConfig.deleteFlag = false;
            newTemplateConfig.status = TemplateConfigStatus.Pending;
            RepositoryContext.tbl_TemplateConfigs.Add(newTemplateConfig);
            RepositoryContext.SaveChanges();
            return newTemplateConfig.Id;
        }

        public int UpdateTemplateConfig(dynamic templateData)
        {
            int Id = templateData.id;
            int sequenceNo = 0;
            string tmpSeq = templateData.sequenceNo;
            if (tmpSeq != null)
            {
                sequenceNo = Convert.ToInt32(tmpSeq);
            }
            tbl_TemplateConfig updateTemplateConfig = RepositoryContext.tbl_TemplateConfigs.Find(Id);

            string title = templateData.title;
            if (!string.IsNullOrEmpty(title))
                updateTemplateConfig.title = title;

            string templateType = templateData.templateType;
            if (!string.IsNullOrEmpty(templateType))
                updateTemplateConfig.templateType = templateType;

            // string url = templateData.url;
            // if (!string.IsNullOrEmpty(url))
            //     updateTemplateConfig.url = url;

            // string videoUrl = templateData.videoUrl;
            // if (!string.IsNullOrEmpty(videoUrl))
            //     updateTemplateConfig.videoUrl = videoUrl;

            string mediaType = templateData.mediaType;
            if (!string.IsNullOrEmpty(mediaType))
                updateTemplateConfig.mediaType = mediaType;

            string blockUserType = templateData.blockUserType;
            if (!string.IsNullOrEmpty(blockUserType))
                updateTemplateConfig.blockUserType = blockUserType;

            string category = templateData.category;
            if (!string.IsNullOrEmpty(category))
                updateTemplateConfig.category = category;

            if (sequenceNo != 0)
                updateTemplateConfig.sequenceNo = sequenceNo;

            string configData = templateData.configData;
            if (!string.IsNullOrEmpty(configData))
                updateTemplateConfig.configData = configData;

            RepositoryContext.tbl_TemplateConfigs.Update(updateTemplateConfig);
            RepositoryContext.SaveChanges();
            return updateTemplateConfig.Id;
        }

        public int ChangeStatus(int id, string status)
        {
            int result = 0;
            //tbl_TemplateConfig updateTemplateConfig = RepositoryContext.tbl_TemplateConfigs.Find(id);
            tbl_TemplateConfig updateTemplateConfig = (from c in RepositoryContext.tbl_TemplateConfigs
                                                       where c.deleteFlag == false && c.showId == showId && c.Id == id
                                                       select c).FirstOrDefault();
            if (updateTemplateConfig != null)
            {
                updateTemplateConfig.status = status;
                RepositoryContext.tbl_TemplateConfigs.Update(updateTemplateConfig);
                RepositoryContext.SaveChanges();
                result = updateTemplateConfig.Id;
            }
            return result;
        }

        public void ChangePendingStatus(int id)
        {
            //tbl_TemplateConfig updateTemplateConfig = RepositoryContext.tbl_TemplateConfigs.Find(id);
            tbl_TemplateConfig updateTemplateConfig = (from c in RepositoryContext.tbl_TemplateConfigs
                                                       where c.deleteFlag == false && c.showId == showId && c.Id == id
                                                       select c).FirstOrDefault();
            (from tc in RepositoryContext.tbl_TemplateConfigs
             where tc.showId == showId && tc.templateType == updateTemplateConfig.templateType
             select tc).ToList()
                    .ForEach(x => x.status = TemplateConfigStatus.Pending);
            RepositoryContext.SaveChanges();
        }

        public void SaveImageUrl(int id, string url)
        {
            tbl_TemplateConfig updateTemplateConfig = RepositoryContext.tbl_TemplateConfigs.Find(id);
            updateTemplateConfig.url = url;
            updateTemplateConfig.videoUrl = null;
            RepositoryContext.tbl_TemplateConfigs.Update(updateTemplateConfig);
            RepositoryContext.SaveChanges();
        }

        public void SaveVideoUrl(int id, string videoUrl)
        {
            tbl_TemplateConfig updateTemplateConfig = RepositoryContext.tbl_TemplateConfigs.Find(id);
            updateTemplateConfig.videoUrl = videoUrl;
            updateTemplateConfig.url = null;
            RepositoryContext.tbl_TemplateConfigs.Update(updateTemplateConfig);
            RepositoryContext.SaveChanges();
        }

        public dynamic GetTemplateConfig(int id)
        {
            var main = (from c in RepositoryContext.tbl_TemplateConfigs
                        where c.deleteFlag == false && c.showId == showId && c.Id == id
                        select c).FirstOrDefault();
            return main;
        }

        public bool DeleteTemplateConfig(int id)
        {
            tbl_TemplateConfig updateTemplateConfig = (from c in RepositoryContext.tbl_TemplateConfigs
                                                       where c.deleteFlag == false && c.showId == showId && c.Id == id
                                                       select c).FirstOrDefault();
            if (updateTemplateConfig != null)
            {
                updateTemplateConfig.deleteFlag = true;
                RepositoryContext.tbl_TemplateConfigs.Update(updateTemplateConfig);
                RepositoryContext.SaveChanges();
                return true;
            }
            return false;
        }
    }
}