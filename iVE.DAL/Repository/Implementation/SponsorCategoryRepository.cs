using System;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation
{
    public class SponsorCategoryRepository : RepositoryBase<tblSponsorCategory>, ISponsorCategoryRepository
    {
        private string showId = LoginData.showId;
        public SponsorCategoryRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetSponsorCategories()
        {
            var main = (from s in RepositoryContext.SponsorCategories
                        where s.showId == showId && s.deleteFlag == false
                        orderby s.sponsorCat_seq
                        select new
                        {
                            SponserCategoryName = s.sponsorCat_name,
                            SponserCategoryID = s.sponsorCat_ID,
                        }).AsNoTracking().Distinct().ToList();
            return main;
        }

        public dynamic GetSponsorCategory(string id)
        {
            dynamic main = (from c in RepositoryContext.SponsorCategories
                            where c.showId == showId && c.deleteFlag == false && c.sponsorCat_ID == id
                            select new
                            {
                                ID = c.sponsorCat_ID,
                                name = c.sponsorCat_name,
                                sequenceNo = c.sponsorCat_seq
                            }).AsNoTracking().Distinct().FirstOrDefault();
            return main;
        }

        public bool AddNewSponsorCategory(string id, dynamic obj)
        {
            try
            {
                var category = new tblSponsorCategory();
                category.showId = showId;
                category.sponsorCat_ID = id;
                category.sponsorCat_name = obj.name;
                category.sponsorCat_seq = obj.sequenceNo;
                category.deleteFlag = false;
                category.createdDate = DateTime.Now;
                RepositoryContext.SponsorCategories.Add(category);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool UpdateSponsorCategory(dynamic obj, tblSponsorCategory category)
        {
            try
            {
                category.sponsorCat_name = obj.name;
                category.sponsorCat_seq = obj.sequenceNo;
                category.updatedDate = DateTime.Now;
                RepositoryContext.SponsorCategories.Update(category);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteSponsorCategory(tblSponsorCategory category)
        {
            try
            {
                category.deleteFlag = true;
                category.updatedDate = DateTime.Now;
                RepositoryContext.SponsorCategories.Update(category);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}