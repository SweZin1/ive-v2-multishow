using System;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;

namespace iVE.DAL.Repository.Implementation
{
    public class ExhibitorTypeRepository : RepositoryBase<tblExhibitorType>, IExhibitorTypeRepository
    {
        private string showId = LoginData.showId;
        public ExhibitorTypeRepository(AppDB repositoryContext) : base (repositoryContext)
        {
            
        }

        public dynamic AddNewExhibitorType(dynamic obj)
        {
            try
            {
                var exhType = new tblExhibitorType();
                exhType.showId = showId;
                exhType.Type = obj.entitlementType;
                exhType.entitleBadges = obj.noOfBadges;
                exhType.OrderNo = obj.sequenceNo;
                exhType.deleteFlag = false;
                exhType.CreatedDate = DateTime.Now;
                RepositoryContext.ExhibitorTypes.Add(exhType);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public dynamic UpdateExhibitorType(dynamic obj, tblExhibitorType exhType)
        {
            try
            {
                exhType.Type = obj.entitlementType;
                exhType.entitleBadges = obj.noOfBadges;
                exhType.OrderNo = obj.sequenceNo;
                exhType.UpdatedDate = DateTime.Now;
                RepositoryContext.ExhibitorTypes.Update(exhType);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public dynamic deleteExhibitorType(tblExhibitorType exhType)
        {
            try
            {
                exhType.deleteFlag = true;
                exhType.UpdatedDate = DateTime.Now;
                RepositoryContext.ExhibitorTypes.Update(exhType);
                RepositoryContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public dynamic getExhibitorTypeDetails(int Id)
        {
            var main = (from c in RepositoryContext.ExhibitorTypes
            where c.deleteFlag == false && c.showId == showId && c.ID == Id
            select c).FirstOrDefault();
            return main;
        }

        public dynamic getExhibitorTypes()
        {
             var main = (from et in RepositoryContext.ExhibitorTypes
                        where et.showId == showId && et.deleteFlag == false
                        select new
                        {
                            ID = et.ID.ToString(),
                            et.Type,
                            et.OrderNo,
                            et.entitleBadges,
                            et.CreatedDate
                        }).AsNoTracking().ToList();
            return main;
        }

        public dynamic getExhibitorTypes(dynamic obj)
        {
            string sortField = null; string sortBy = "";
            var main = (from c in RepositoryContext.ExhibitorTypes
                        where c.deleteFlag == false && c.showId == showId
                        select new
                        {
                            id = c.ID.ToString(),
                            entitlementType = c.Type,
                            noOfBadges = c.entitleBadges,
                            sequenceNo = c.OrderNo,
                            createdDate = c.CreatedDate
                        }).AsNoTracking();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSession(main, obj);
                }
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "CreatedDate";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            var objTotal = main.Count();
            main = PaginationSession(main, obj);

            var objResult = main.AsNoTracking().ToList();
            return new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved." };
        }

        private dynamic FilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(string),
                entitlementType = default(string),
                noOfBadges = default(int?),
                sequenceNo = default(int),
                createdDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "Name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.entitlementType.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(string),
                entitlementType = default(string),
                noOfBadges = default(int?),
                sequenceNo = default(int),
                createdDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
    }
}