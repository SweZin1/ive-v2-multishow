using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using System.Globalization;

using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
namespace iVE.DAL.Repository.Implementation
{
    public class TempAccountRepository: RepositoryBase<TempAccount>, ITempAccountRepository
    {
        public TempAccountRepository(AppDB repositoryContext): base(repositoryContext)
        {
            
        }
    }
}