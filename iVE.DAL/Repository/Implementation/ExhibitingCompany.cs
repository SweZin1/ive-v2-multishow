using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation {
    public class ExhibitingCompany : RepositoryBase<tblExhibitingCompany>, IExhibitingCompanyRepository {
        public ExhibitingCompany (AppDB repositoryContext) : base (repositoryContext) {

        }

    }
}