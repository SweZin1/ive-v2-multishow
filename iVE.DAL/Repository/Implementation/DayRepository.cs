using System;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation
{
    public class DayRepository : RepositoryBase<tbl_Day>, IDayRepository
    {
        public DayRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public void AddDay(DateTime date, string id, string showId)
        {
            tbl_Day newDay = new tbl_Day();
            newDay.d_ID = id;
            newDay.d_date = date;
            newDay.d_year = date.Year.ToString();
            newDay.deleteFlag = false;
            newDay.UpdatedDate = System.DateTime.Now;
            newDay.showId = showId;
            RepositoryContext.Days.Add(newDay);
            RepositoryContext.SaveChanges();
        }

        public void UpdateDay(string id)
        {
            tbl_Day updateDay = RepositoryContext.Days.Find(id);
            updateDay.deleteFlag = false;
            updateDay.UpdatedDate = System.DateTime.Now;
            RepositoryContext.Days.Update(updateDay);
            RepositoryContext.SaveChanges();
        }

        public void DeleteDay(tbl_Day deleteDayObj)
        {
            deleteDayObj.deleteFlag = true;
            deleteDayObj.UpdatedDate = System.DateTime.Now;
            RepositoryContext.Days.Update(deleteDayObj);
            RepositoryContext.SaveChanges();
        }

        public void DeleteDays(DateTime sDate, DateTime eDate, string showId)
        {
            (from d in RepositoryContext.Days where (d.d_date.Date < sDate || d.d_date.Date > eDate) && d.showId == showId && d.deleteFlag == false select d).ToList()
                .ForEach(x => x.deleteFlag = true);
            RepositoryContext.SaveChanges();
        }

    }
}