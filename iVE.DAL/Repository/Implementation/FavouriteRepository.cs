using System;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.DAL.Repository.Implementation
{
    public class FavouriteRepository : RepositoryBase<tbl_UsersFavourite>, IFavouriteRepository
    {
        private string showId = LoginData.showId;
        public FavouriteRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetFavouriteExhibitors(string userid)
        {
            var main = (from uf in RepositoryContext.UsersFavourites
                        join e in RepositoryContext.Exhibitors on uf.FavItemId equals e.ExhID
                        where uf.Status == 1 && uf.UserId == userid && uf.ItemType == FavouriteItem.Exhibitor && e.Status == ExhibitorStatus.Published
                        orderby uf.FavItemId
                        select new
                        {
                            ExhID = e.ExhID,
                            CompanyName = e.CompanyName,
                            Description = e.Description,
                            Country = e.Country,
                            ExhibitorType = e.ExhType,
                            HallNo = e.HallNo,
                            BoothNo = e.BoothNo,
                            BoothTemplate = e.BoothTemplate,
                            Logo = e.Logo,
                            BoothBanner = e.BoothBanner,
                            SocialMedia = e.SocialMedia,
                            CompanyVideo = e.CompanyVideo,
                            CompanyDocument = e.CompanyDocument,
                        }).ToList();
            return main;
        }

        public void SaveNewUserFavourite(string actionId, string favouriteId, string favType)
        {
            tbl_UsersFavourite userFavourite = new tbl_UsersFavourite();
            userFavourite.UserId = actionId;
            userFavourite.showId = showId;
            userFavourite.FavItemId = favouriteId;
            userFavourite.Status = 1;
            userFavourite.ItemType = favType;
            userFavourite.CreatedDate = DateTime.Now;
            userFavourite.ModifiedDate = DateTime.Now;
            userFavourite.CreatedBy = actionId;
            RepositoryContext.UsersFavourites.Add(userFavourite);
            RepositoryContext.SaveChanges();
        }

        public void UpdateUserFavourite(tbl_UsersFavourite obj, int _status)
        {
            obj.Status = _status;
            obj.ModifiedDate = DateTime.Now;
            RepositoryContext.UsersFavourites.Update(obj);
            RepositoryContext.SaveChanges();
        }
    }
}