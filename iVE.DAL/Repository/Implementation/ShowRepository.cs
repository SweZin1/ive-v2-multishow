using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using iVE.DAL.Util;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;

namespace iVE.DAL.Repository.Implementation
{
    public class ShowRepository : RepositoryBase<tblShow>, IShowRepository
    {
        private string showId = LoginData.showId;
        public ShowRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
        public dynamic GetShowBySuperAdmin(string id)
        {
            return (from s in RepositoryContext.Shows
                    where s.showId == id && s.deleteFlag == false
                    select new
                    {
                        showId = s.showId,
                        showPrefix = s.showPrefix,
                        showName = s.showName,
                        title = s.showTitle,
                        country = s.country,
                        category = s.category,
                        timeZone = s.timeZone,
                        registrationLink = s.RegLink,
                        routeName = s.route,
                        showStartDate = s.showStartDate,
                        showEndDate = s.showEndDate,
                        status = s.Status,
                        chatId = s.chatId
                    }).FirstOrDefault();
        }

        public dynamic GetShowByOrganizer()
        {
            return (from s in RepositoryContext.Shows
                    where s.showId == showId && s.deleteFlag == false
                    select new
                    {
                        showId = s.showId,
                        showName = s.showName,
                        title = s.showTitle,
                        category = s.category,
                        conferenceTemplate = s.conferenceTemplate,
                        conferenceStaticUrl = s.conferenceStaticUrl
                    }).FirstOrDefault();
        }

        public dynamic GetShowListBySuperAdmin(dynamic obj)
        {
            string sortField = null; string sortBy = "";

            var main = (from s in RepositoryContext.Shows
                        where s.deleteFlag == false
                        select new
                        {
                            showId = s.showId,
                            showName = s.showName,
                            showStartDate = s.showStartDate,
                            showEndDate = s.showEndDate,
                            status = s.Status,
                            updatedDate = s.updatedDate,
                            routeName = s.route
                        }).AsNoTracking();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForShowList(main, obj);
                }
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "updatedDate";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            var objTotal = main.Count();
            main = PaginationSessionForShowList(main, obj);

            var objResult = main.AsNoTracking().ToList();
            return new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved." };
        }

        private dynamic FilterSessionForShowList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                showId = default(string),
                showName = default(string),
                showStartDate = default(DateTime),
                showEndDate = default(DateTime),
                status = default(string),
                updatedDate = default(DateTime),
                routeName = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "Name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.showName.ToLower().Contains(Name));

                        }
                    }

                    if (filterName == "fromDate" && !string.IsNullOrEmpty(filterValue))
                    {
                        DateTime fDate = Convert.ToDateTime(filterValue);
                        tmpQuery = tmpQuery.Where(x => x.showStartDate.Date >= fDate.Date);
                    }

                    if (filterName == "toDate" && !string.IsNullOrEmpty(filterValue))
                    {
                        DateTime tDate = Convert.ToDateTime(filterValue);
                        tmpQuery = tmpQuery.Where(x => x.showEndDate.Date <= tDate.Date);
                    }
                }

                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForShowList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                showId = default(string),
                showName = default(string),
                showStartDate = default(DateTime),
                showEndDate = default(DateTime),
                status = default(string),
                updatedDate = default(DateTime),
                routeName = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.showId);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.showId) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public string AddShowBySuperAdmin(dynamic showObj)
        {
            using (var transaction = RepositoryContext.Database.BeginTransaction())
            {
                try
                {
                    string showId = "";
                    tblShow newShowObj = new tblShow();
                    newShowObj.showId = showObj.showId;
                    showId = showObj.showId;
                    newShowObj.showName = showObj.showName;
                    newShowObj.showPrefix = showObj.showPrefix;
                    newShowObj.showTitle = showObj.title;
                    newShowObj.country = showObj.country;
                    newShowObj.category = showObj.category;
                    newShowObj.timeZone = showObj.timeZone;
                    newShowObj.RegLink = showObj.registrationLink;
                    newShowObj.route = showObj.routeName;
                    newShowObj.showStartDate = showObj.showStartDate;
                    newShowObj.showEndDate = showObj.showEndDate;
                    newShowObj.chatId = showObj.chatId;
                    newShowObj.Status = ShowStatus.Initial;
                    newShowObj.deleteFlag = false;
                    newShowObj.createdDate = System.DateTime.Now;
                    newShowObj.updatedDate = System.DateTime.Now;
                    RepositoryContext.Shows.Add(newShowObj);
                    RepositoryContext.SaveChanges();

                    //SET DEFAULT DATA
                    var showValue = new SqlParameter("@showId", showId);
                    RepositoryContext.Database.ExecuteSqlRaw("EXEC setShowDefaultData @showId", showValue);
                    transaction.Commit();
                    return newShowObj.showId;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return "";
                }
            }

        }

        public string UpdateShowBySuperAdmin(dynamic showObj)
        {
            string showId = showObj.showId;
            tblShow updateShowObj = RepositoryContext.Shows.Find(showId);
            updateShowObj.showName = showObj.showName;
            updateShowObj.showPrefix = showObj.showPrefix;
            updateShowObj.showTitle = showObj.title;
            updateShowObj.country = showObj.country;
            updateShowObj.category = showObj.category;
            updateShowObj.timeZone = showObj.timeZone;
            updateShowObj.RegLink = showObj.registrationLink;
            updateShowObj.route = showObj.routeName;
            updateShowObj.showStartDate = showObj.showStartDate;
            updateShowObj.showEndDate = showObj.showEndDate;
            updateShowObj.updatedDate = System.DateTime.Now;
            RepositoryContext.Shows.Update(updateShowObj);
            RepositoryContext.SaveChanges();
            return showId;
        }

        public string UpdateShowByOrganizer(dynamic showObj)
        {
            tblShow updateShowObj = RepositoryContext.Shows.Find(showId);
            updateShowObj.showName = showObj.showName;
            updateShowObj.showTitle = showObj.title;
            updateShowObj.category = showObj.category;
            updateShowObj.conferenceTemplate = showObj.conferenceTemplate;
            updateShowObj.conferenceStaticUrl = showObj.conferenceStaticUrl;
            updateShowObj.Status = ShowStatus.Pending;
            updateShowObj.updatedDate = System.DateTime.Now;
            RepositoryContext.Shows.Update(updateShowObj);
            RepositoryContext.SaveChanges();
            return showId;
        }

        public void UpdateShowConfigUrl(string id, string url)
        {
            GlobalFunctionForDAL.WriteSystemLog(LogType.Info, "show", "show Id : " + id + "url : " + url, "");
            tblShow updateShowObj = RepositoryContext.Shows.Find(id);
            updateShowObj.showConfigUrl = url;
            updateShowObj.updatedDate = System.DateTime.Now;
            RepositoryContext.Shows.Update(updateShowObj);
            RepositoryContext.SaveChanges();
        }

        public bool DeleteShowBySuperAdmin(string id)
        {
            tblShow deleteShowObj = RepositoryContext.Shows.Find(id);
            deleteShowObj.deleteFlag = true;
            deleteShowObj.updatedDate = System.DateTime.Now;
            RepositoryContext.Shows.Update(deleteShowObj);
            RepositoryContext.SaveChanges();
            return true;
        }

        public int CheckDuplicateForShow(dynamic showObj, bool isNew)
        {
            int result = 0;
            string prefix = showObj.showPrefix;
            string route = showObj.routeName;
            if (isNew)
            {
                var prefixQuery = (from s in RepositoryContext.Shows where s.showPrefix == prefix && s.deleteFlag == false select s);
                if (prefixQuery.Count() > 0)
                {
                    result = 1;
                    return result;
                }

                var routeQuery = (from s in RepositoryContext.Shows where s.route == route && s.deleteFlag == false select s);
                if (routeQuery.Count() > 0)
                {
                    result = 2;
                    return result;
                }
            }
            else
            {
                string showId = showObj.showId;
                var prefixQuery = (from s in RepositoryContext.Shows where s.showPrefix == prefix && s.deleteFlag == false && s.showId != showId select s);
                if (prefixQuery.Count() > 0)
                {
                    result = 1;
                    return result;
                }

                var routeQuery = (from s in RepositoryContext.Shows where s.route == route && s.deleteFlag == false && s.showId != showId select s);
                if (routeQuery.Count() > 0)
                {
                    result = 2;
                    return result;
                }
            }
            return result;
        }

        public void ChangeStatusForShow(string id, string status)
        {
            tblShow updatedShow = RepositoryContext.Shows.Find(id);
            updatedShow.Status = status;
            updatedShow.updatedDate = System.DateTime.Now;
            RepositoryContext.Shows.Update(updatedShow);
            RepositoryContext.SaveChanges();
        }

        public string GetThemeConfigFileName(string id)
        {
            string fileName = "";
            tblShow showObj = RepositoryContext.Shows.Find(id);
            string showConfigUrl = showObj.showConfigUrl;
            if (showConfigUrl != null)
            {
                string[] subs = showConfigUrl.Split('/');
                fileName = subs[subs.Length - 1];
            }

            return fileName;
        }
    }
}