using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation
{
    public class ProductCommantRepository : RepositoryBase<tblProductCommant>, IProductCommantRepository
    {
        private string showId = LoginData.showId;
        public ProductCommantRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetProductCommantList(string exhId, string productId)
        {
            var main = (from pc in RepositoryContext.ProductCommants
                        join p in RepositoryContext.Products on pc.ProductId equals p.p_ID
                        join a in RepositoryContext.Accounts on pc.UserId equals a.a_ID
                        where p.showId == showId && a.deleteFlag == false
                        && pc.ExhId == exhId && pc.ProductId == productId && pc.Status == 1
                        select new
                        {
                            ProductName = p.p_name,
                            UserName = a.a_fullname,
                            Commant = pc.Command,
                            CommantDate = pc.UpdatedDate == null ? pc.CreatedDate : pc.UpdatedDate
                        }).OrderBy(x => x.CommantDate).ToList();
            return main;
        }

    }
}