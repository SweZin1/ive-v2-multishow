using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface ISponsorCategoryRepository : IRepositoryBase<tblSponsorCategory> {
        dynamic GetSponsorCategories ();
        dynamic GetSponsorCategory(string id);
        bool AddNewSponsorCategory(string id, dynamic obj);
        bool UpdateSponsorCategory(dynamic obj, tblSponsorCategory category);
        bool DeleteSponsorCategory(tblSponsorCategory category);
    }
}