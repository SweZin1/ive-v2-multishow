using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IChatMessageDetailsRepository : IRepositoryBase<tblChatMessageDetails>
    {
        void AddMessages(dynamic response);
        dynamic GetMessageList(dynamic obj, string sortField, string sortBy, bool isexport);
    }
}