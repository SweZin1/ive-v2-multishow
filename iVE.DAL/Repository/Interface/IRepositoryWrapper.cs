using iVE.DAL.Models;
using iVE.DAL.Repository.Implementation;
namespace iVE.DAL.Repository.Interface
{
    public interface IRepositoryWrapper
    {
        ISiteRunNumberRepository SiteRunNumber { get; }
        IAdminRepository Admin { get; }
        IAccountRepository Account { get; }
        INotificationTemplateRepository NotificationTemplate { get; }
        INotificationSendListRepository NotificationSendList { get; }
        IAuditRepository Audit { get; }
        IExhibitorRepository Exhibitor { get; }
        IProductRepository Product { get; }
        ISponserRepository Sponser { get; }
        IBoothTemplateRepository BoothTemplate { get; }
        ITemplateRepository Template { get; }
        IProductCommantRepository ProductCommant { get; }
        IShowConfigRepository ShowConfig { get; }
        ISponsorCategoryRepository SponsorCategory { get; }
        IFavouriteRepository Favourite { get; }
        IVisitorRecommendationRepository VisitorRecommendations { get; }
        IVisitorRecommLogRepository VisitorRecommLog { get; }
        IProductTypeRepository ProductType { get; }
        IAppointmentRepository Appointment { get; }
        ICountryRepository Country { get; }
        IFeedBackBoardRepository FeedBackBoard { get; }
        ILeaderBoardRepository LeaderBoard { get; }
        IAccountLogicBlockerRepository AccountLogicBlocker { get; }
        IMCQScoreBoardRepository MCQScoreBoard { get; }
        IEmailConfigRepository EmailConfig { get; }
        IAppTimeSlotRepository AppTimeSlot { get; }
        IChatMessageDetailsRepository ChatMessageDetails { get; }
        ITemplateConfigRepository TemplateConfig { get; }
        ITempAccountRepository TempAccount { get; }
        IShowRepository Show { get; }
        IDayRepository Day { get; }
        IChatConfigurationRepository ChatConfiguration { get; }
        IExhibitorTypeRepository ExhibitorType { get; }
        IExhibitorCategoryRepository ExhibitorCategory { get; }
        void Save();
    }
}