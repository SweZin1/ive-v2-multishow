using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IShowRepository : IRepositoryBase<tblShow>
    {
        dynamic GetShowBySuperAdmin(string id);
        dynamic GetShowListBySuperAdmin(dynamic obj);
        string AddShowBySuperAdmin(dynamic showObj);
        string UpdateShowBySuperAdmin(dynamic showObj);
        string UpdateShowByOrganizer(dynamic showObj);
        void UpdateShowConfigUrl(string id, string url);
        bool DeleteShowBySuperAdmin(string id);
        int CheckDuplicateForShow(dynamic showObj, bool isNew);
        void ChangeStatusForShow(string id, string status);
        string GetThemeConfigFileName(string id);
        dynamic GetShowByOrganizer();
    }
}