using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface INotificationTemplateRepository : IRepositoryBase<tblNotificationTemplate> {
        string GetNotificationData(string userName);
    }
}