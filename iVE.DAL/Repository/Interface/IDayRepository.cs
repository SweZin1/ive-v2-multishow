using iVE.DAL.Models;
using System;
namespace iVE.DAL.Repository.Interface
{
    public interface IDayRepository : IRepositoryBase<tbl_Day>
    {
        void AddDay(DateTime date, string id, string showId);
        void UpdateDay(string id);
        void DeleteDay(tbl_Day deleteDayObj);
        void DeleteDays(DateTime sDate, DateTime eDate, string showId);
    }
}