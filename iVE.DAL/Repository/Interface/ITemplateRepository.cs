using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface ITemplateRepository : IRepositoryBase<tblTemplate>
    {
        dynamic GetAllBooth(string exhId);
        dynamic GetBoothById(string exhId, string boothId);
        dynamic CreateBoothById(dynamic obj, string templateId, string exhId);
        dynamic UpdateBoothById(tblTemplate template, dynamic obj);
        dynamic GetBoothByTemplateId(string templateId);
        void UpdateBoothConfigByName(string boothTemplateId, dynamic obj);
        /*8-7-2020*/
        void UpdateTemplateImageByExhID(string exhId, tblTemplate template, dynamic obj);
    }
}