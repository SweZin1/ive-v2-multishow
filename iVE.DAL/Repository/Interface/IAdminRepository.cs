using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IAdminRepository : IRepositoryBase<tbl_Administrator>
    {
        dynamic GetAdminBySuperAdmin(int id);
        dynamic GetAdminListBySuperAdmin(dynamic obj, string showId);
        int AddAdminBySuperAdmin(dynamic adminObj);
        int UpdateAdminBySuperAdmin(dynamic adminObj);
        void UpdateAdminProfile(int id, string url);
        bool DeleteAdminBySuperAdmin(int id);
        int CheckDuplicateForAdmin(dynamic adminObj, bool isNew);
        void ChangeAdminPassword(dynamic adminObj);
    }
}