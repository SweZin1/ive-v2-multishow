using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IExhibitorTypeRepository : IRepositoryBase<tblExhibitorType>
    {
        dynamic getExhibitorTypes();
        dynamic getExhibitorTypes(dynamic obj);
        dynamic getExhibitorTypeDetails(int Id);
        dynamic AddNewExhibitorType(dynamic obj);
        dynamic UpdateExhibitorType(dynamic obj, tblExhibitorType exhType);
        dynamic deleteExhibitorType(tblExhibitorType exhType);
    }
}