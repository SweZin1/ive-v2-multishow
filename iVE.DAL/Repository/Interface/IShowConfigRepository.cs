using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface IShowConfigRepository : IRepositoryBase<tblShowConfig> {
        dynamic GetShowConfig ();
        bool SaveNewShowConfig (dynamic obj, string name);
        bool UpdateShowConfig (dynamic obj, tblShowConfig showconfig);
    }
}