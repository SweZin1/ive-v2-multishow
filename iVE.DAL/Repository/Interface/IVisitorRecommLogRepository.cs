using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IVisitorRecommLogRepository : IRepositoryBase<tblVisitorRecommendationLog> {

    }
}