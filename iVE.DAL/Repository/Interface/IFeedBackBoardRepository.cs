using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IFeedBackBoardRepository: IRepositoryBase<tblFeedbackboard> 
    {
        dynamic GetFeedbacksbyThreadOwnerId(dynamic obj,string Id);
        dynamic GetAllComments(dynamic obj,string febId);
        void CreateComment(dynamic obj);
        void CreateCommentActions(dynamic obj);
        void UpdateCommentStatus(string febId, string cmtId, dynamic obj,string loginId);
    }
    
}