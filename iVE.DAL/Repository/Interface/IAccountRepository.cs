using System.Collections.Generic;
using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface IAccountRepository : IRepositoryBase<tbl_Account> {
        void SaveNewContactPerson (dynamic obj, string exhId, string contactId);
        void UpdateContactPerson (dynamic obj, tbl_Account contactPerson);
        void DeleteContactPerson (string exhId, string contactId);
        dynamic GetVisitor(dynamic obj, string sortField, string sortBy, bool isDefaultSort);
        dynamic GetVisitorById(string Id);
        dynamic GetVisitedExhListByVisitorId(string Id);        
        void SaveNewVisitor (dynamic obj, string visitorId);
        void UpdateVisitor (dynamic obj, tbl_Account visitorObj);
        void DeleteVisitor (tbl_Account visitorObj);
        dynamic GetFavouriteExhibitorsByVisitorId(string Id);
        dynamic GetFavouriteProductsByVisitorId(string Id);

        dynamic GetVisitedExhibitor(dynamic obj, string sortField, string sortBy, string userId);

        /*8-7-2020*/
        dynamic GetAllContactPersonsWithStatusByExhId(string exhId);

        dynamic GetAllExhibitorProductsByVisitorId(dynamic obj, string sortField, string sortBy, bool isDefaultSort,string exhId, string vId);

        dynamic GetFavouriteExhStatusByVisitor(string vId, string exhId);

        void ChangePassword(string pwd,tbl_Account contactPerson);

        dynamic GetVisitorsByCountry();
        bool CheckBadgeEntitlement(string exhId);
        dynamic GetActiveLoginUsers(dynamic obj,bool isexport);
        dynamic GetContactPersonDetail(string exhId, string contactId);
        dynamic GetContactPersonListbyExhId(string exhId);
        dynamic GetDays();
        void updateProfilePicUrl(tbl_Account account, string url);
        dynamic GetSalutation();
        void UpdateTempAccount(List<TempAccount> lst);
        dynamic getTempAccounts(dynamic obj);
        bool UpdateAccounts(List<tbl_Account> lst);
    }

}