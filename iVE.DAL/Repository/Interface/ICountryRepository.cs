using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface ICountryRepository : IRepositoryBase<tbl_Country>
    {
        dynamic GetCountryList ();
        string GetCountryISO3DigitByCountry (string countryName);
    }
}