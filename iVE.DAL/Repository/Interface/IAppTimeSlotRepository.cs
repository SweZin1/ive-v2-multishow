using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IAppTimeSlotRepository: IRepositoryBase<tbl_App_TimeSlot>
    {
        
    }
}