using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IProductTypeRepository : IRepositoryBase<tblProductTypeMaster>
    {
        dynamic GetProductTypeList ();
    }
}