using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface ITemplateConfigRepository : IRepositoryBase<tbl_TemplateConfig>
    {
        dynamic GetTemplate(dynamic obj);
        dynamic GetTemplateList(dynamic obj);
        int AddTemplateConfig(dynamic templateData);
        int UpdateTemplateConfig(dynamic templateData);
        int ChangeStatus(int id, string status);
        void SaveImageUrl(int id, string url);
        void SaveVideoUrl(int id, string videoUrl);
        dynamic GetTemplateConfig(int id);
        bool DeleteTemplateConfig(int id);
        void ChangePendingStatus(int id);
    }
}