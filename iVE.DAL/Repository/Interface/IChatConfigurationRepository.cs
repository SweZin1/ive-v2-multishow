using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IChatConfigurationRepository : IRepositoryBase<tblChatConfiguration>
    {
        dynamic GetChatConfigs();
        dynamic GetChatConfig(int id);
        dynamic GetChatConfigList(dynamic obj);
        int AddChatConfig(dynamic obj);
        int UpdateChatConfig(dynamic obj);
        bool DeleteChatConfig(int id);
        int CheckDuplicateForChatConfig(dynamic obj, bool isNew);
    }
}