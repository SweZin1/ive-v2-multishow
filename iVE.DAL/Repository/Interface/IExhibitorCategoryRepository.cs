using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IExhibitorCategoryRepository : IRepositoryBase<tblExhibitorCategory>
    {
        dynamic getExhibitorCategories();
        dynamic getExhibitorCategories(dynamic obj);
        dynamic getExhibitorCategoryDetails(int Id);
        dynamic AddNewExhibitorCategory(dynamic obj);
        dynamic UpdateExhibitorCategory(dynamic obj, tblExhibitorCategory exhCategory);
        dynamic deleteExhibitorCategory(tblExhibitorCategory exhCategory);
    }
}