using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using iVE.BLL.ChatAPI;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace iVE.BLL.Implementation
{
    public class ChatConfigurationLogic : IChatConfigurationLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        public ChatConfigurationLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
        }

        public dynamic GetChatConfig(int id)
        {
            return _repositoryWrapper.ChatConfiguration.GetChatConfig(id);
        }

        public dynamic GetChatConfigs()
        {
            return _repositoryWrapper.ChatConfiguration.GetChatConfigs();
        }

        public dynamic GetChatConfigList(dynamic obj)
        {
            return _repositoryWrapper.ChatConfiguration.GetChatConfigList(obj);
        }

        public dynamic DeleteChatConfig(int id)
        {
            tblChatConfiguration chatConfig = _repositoryWrapper.ChatConfiguration.FindByCondition(x => x.Id == id && x.deleteFlag == false).FirstOrDefault();
            if (chatConfig != null)
            {
                _repositoryWrapper.ChatConfiguration.DeleteChatConfig(id);
                return true;
            }
            return false;
        }

        public dynamic AddOrUpdateChatConfig(dynamic obj)
        {
            int id = obj.id == null ? 0 : obj.id;
            tblChatConfiguration chatConfig = _repositoryWrapper.ChatConfiguration.FindByCondition(x => x.Id == id && x.deleteFlag == false).FirstOrDefault();
            if (chatConfig != null)
            {
                int duplicateResult = _repositoryWrapper.ChatConfiguration.CheckDuplicateForChatConfig(obj, false);
                if (duplicateResult == 1)
                {
                    return new { success = false, message = "Chat App ID already exists!" };
                }
                if (duplicateResult == 2)
                {
                    return new { success = false, message = "Chat Name already exists!" };
                }
                id = _repositoryWrapper.ChatConfiguration.UpdateChatConfig(obj);
            }
            else
            {
                int duplicateResult = _repositoryWrapper.ChatConfiguration.CheckDuplicateForChatConfig(obj, true);
                if (duplicateResult == 1)
                {
                    return new { success = false, message = "Chat App ID already exists!" };
                }
                if (duplicateResult == 2)
                {
                    return new { success = false, message = "Chat Name already exists!" };
                }
                id = _repositoryWrapper.ChatConfiguration.AddChatConfig(obj);
            }
            return new { data = id, success = true, message = "Success" };
        }
    }
}