using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using iVE.BLL.Interface;
using iVE.BLL.Util;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;

using System.Text;
using System.IO.Compression;
using System.Threading.Tasks;
using iVE.DAL;
namespace iVE.BLL.Implementation
{
    public class BoothLogic : IBoothLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        string showId = string.Empty;
        public BoothLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }

        public dynamic GetAllBooth(string exhId)
        {
            dynamic data = _repositoryWrapper.Template.GetAllBooth(exhId);
            return data;
        }

        public dynamic GetBoothById(string exhId, string boothId)
        {
            dynamic data = _repositoryWrapper.Template.GetBoothById(exhId, boothId);
            return data;
        }
        public string CreateOrUpdateBoothById(string exhId, dynamic obj)
        {
            string templateId = "";
            tblTemplate template = _repositoryWrapper.Template.FindByCondition(x => x.showId == showId && x.ExhID == exhId).FirstOrDefault();
            if (template != null)
            {
                templateId = _repositoryWrapper.Template.UpdateBoothById(template, obj);
            }
            else
            {
                string boothId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.BoothTemplateRunKey);
                templateId = _repositoryWrapper.Template.CreateBoothById(obj, boothId, exhId);
                _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.BoothTemplateRunKey);
            }
            return templateId;
        }
        public dynamic GetBoothbyTemplateId(string templateId)
        {
            return _repositoryWrapper.Template.GetBoothByTemplateId(templateId);
        }
        public void UpdateBoothFileConfig(string boothTemplateId, dynamic obj)
        {

            _repositoryWrapper.Template.UpdateBoothConfigByName(boothTemplateId, obj);
        }

        /*8-7-2020*/
        public void UpdateTemplateImageByExhID(dynamic obj, string exhId)
        {
            tblTemplate template = _repositoryWrapper.Template.FindByCondition(x => x.showId == showId && x.ExhID == exhId).FirstOrDefault();
            if (template != null)
            {
                _repositoryWrapper.Template.UpdateTemplateImageByExhID(exhId, template, obj);
            }
        }

        public async Task<byte[]> GetBoothTemplateZipFile(string exhId)
        {
            var main = _repositoryWrapper.BoothTemplate.GetTemplateByExhId(exhId);
            dynamic expendObj = new ExpandedObject(main);

            List<TemplateConfig> templateConfigs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>>(expendObj.TemplateConfig);
            TemplateConfig objBackground = new TemplateConfig();
            objBackground.image = expendObj.Image;
            objBackground.name = "Background";
            templateConfigs.Add(objBackground);

            return await GenerateZipFile(templateConfigs);
        }

        private async Task<byte[]> GenerateZipFile(List<TemplateConfig> templateConfigs)
        {
            byte[] archiveFile;
            using (var archiveStream = new MemoryStream())
            {
                using (var archive = new ZipArchive(archiveStream, ZipArchiveMode.Create, true))
                {
                    foreach (var template in templateConfigs)
                    {
                        string imgfile = "";
                        imgfile = template.image;
                        if (imgfile != null && imgfile != "")
                        {
                            FileInfo fi = new FileInfo(imgfile);
                            string fileName = template.name + fi.Extension;
                            byte[] fileContents;
                            using (var client = new HttpClient())
                            using (HttpResponseMessage response = await client.GetAsync(imgfile))
                            {
                                fileContents = await response.Content.ReadAsByteArrayAsync();
                                var zipArchiveEntry = archive.CreateEntry(fileName, CompressionLevel.Fastest);
                                using (var zipStream = zipArchiveEntry.Open())
                                    zipStream.Write(fileContents, 0, fileContents.Length);
                            }
                        }
                    }
                }
                archiveFile = archiveStream.ToArray();
            }
            return archiveFile;
        }
    }
}