using iVE.BLL.Interface;
using iVE.DAL.Repository.Interface;
using System.Linq;
using System.Collections.Generic;
using iVE.DAL.Models;
using iVE.DAL.Util;
using System;
namespace iVE.BLL.Implementation
{
    public class AdminLogic : IAdminLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        public AdminLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
        }

        public dynamic GetAdminBySuperAdmin(int id)
        {
            return _repositoryWrapper.Admin.GetAdminBySuperAdmin(id);
        }

        public dynamic GetAdminListBySuperAdmin(dynamic obj, string showId)
        {
            return _repositoryWrapper.Admin.GetAdminListBySuperAdmin(obj, showId);
        }

        public dynamic DeleteAdminBySuperAdmin(int id)
        {
            tbl_Administrator admin = _repositoryWrapper.Admin.FindByCondition(x => x.admin_id == id && x.admin_isdeleted == false && x.IsSuperAdmin == false).FirstOrDefault();
            if (admin != null)
            {
                _repositoryWrapper.Admin.DeleteAdminBySuperAdmin(id);
                return true;
            }
            return false;
        }

        public dynamic AddOrUpdateAdminBySuperAdmin(dynamic obj)
        {
            int adminId = obj.id == null ? 0 : obj.id;
            tbl_Administrator admin = _repositoryWrapper.Admin.FindByCondition(x => x.admin_id == adminId && x.admin_isdeleted == false && x.IsSuperAdmin == false).FirstOrDefault();
            if (admin != null)
            {
                int duplicateResult = _repositoryWrapper.Admin.CheckDuplicateForAdmin(obj, false);
                if (duplicateResult == 1)
                {
                    return new { result = false, message = "Email already exists!" };
                }
                if (duplicateResult == 2)
                {
                    return new { result = false, message = "Mobile already exists!" };
                }
                if (duplicateResult == 3)
                {
                    return new { result = false, message = "User Name already exists!" };
                }
                adminId = _repositoryWrapper.Admin.UpdateAdminBySuperAdmin(obj);
            }
            else
            {
                int duplicateResult = _repositoryWrapper.Admin.CheckDuplicateForAdmin(obj, true);
                if (duplicateResult == 1)
                {
                    return new { result = false, message = "Email already exists!" };
                }
                if (duplicateResult == 2)
                {
                    return new { result = false, message = "Mobile already exists!" };
                }
                if (duplicateResult == 3)
                {
                    return new { result = false, message = "User Name already exists!" };
                }
                adminId = _repositoryWrapper.Admin.AddAdminBySuperAdmin(obj);
            }
            return new { id = adminId, result = true, message = "Success" };
        }

        public dynamic ChangeAdminPassword(dynamic obj)
        {
            int adminId = obj.id == null ? 0 : obj.id;
            tbl_Administrator admin = _repositoryWrapper.Admin.FindByCondition(x => x.admin_id == adminId && x.admin_isdeleted == false && x.IsSuperAdmin == false).FirstOrDefault();
            if (admin != null)
            {
                _repositoryWrapper.Admin.ChangeAdminPassword(obj);
                return true;
            }
            return false;
        }

        public void UpdateAdminProfile(int id, string url)
        {
            tbl_Administrator admin = _repositoryWrapper.Admin.FindByCondition(x => x.admin_id == id && x.admin_isdeleted == false && x.IsSuperAdmin == false).FirstOrDefault();
            if (admin != null)
            {
                _repositoryWrapper.Admin.UpdateAdminProfile(id, url);
            }
        }
    }
}