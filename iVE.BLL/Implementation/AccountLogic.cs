using System.Data;
using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using iVE.BLL.Util;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using iVE.DAL;

namespace iVE.BLL.Implementation
{
    public class AccountLogic : IAccountLogic
    {
        string showId = "";
        private IRepositoryWrapper _repositoryWrapper;
        public AccountLogic (IRepositoryWrapper rw) {
            showId = LoginData.showId;
            _repositoryWrapper = rw;
        }

        public dynamic GetVisitor(dynamic obj){
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            bool isDefaultSort = false;
            if (sortField == null || sortField == "")
                sortField = "fullName";
            isDefaultSort = true;
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var visitor = _repositoryWrapper.Account.GetVisitor(obj, sortField, sortBy, isDefaultSort);
            return visitor;
        }

        public dynamic GetVisitorById(string Id){
            var visitor = _repositoryWrapper.Account.GetVisitorById (Id);
            return visitor;
        }

        public dynamic GetVisitedExhListByVisitor(string Id){
            var visitor = _repositoryWrapper.Account.GetVisitedExhListByVisitorId(Id);
            return visitor;
        }

        public dynamic CreateOrUpdateVisitor(dynamic obj, string visitorId)
        {
            string email = obj.email;
            string userName = obj.loginName;

            var emailObj = _repositoryWrapper.Account
            .FindByCondition(x => x.showId == showId && x.deleteFlag == false && x.a_email.ToLower() == email.ToLower() && (visitorId == "0" || x.a_ID != visitorId))
            .Select(x => x.a_email).FirstOrDefault();
            var userNameObj = _repositoryWrapper.Account
            .FindByCondition(x =>x.showId == showId && x.deleteFlag == false && x.a_LoginName.ToLower() == userName.ToLower() && (visitorId == "0" || x.a_ID != visitorId))
            .Select(x => x.a_LoginName).FirstOrDefault();
            if (emailObj != null)
            {
                return new { data = "", success = false, message = "Email already existed!" };
            }
            else if (userNameObj != null)
            {
                return new { data = "", success = false, message = "Login name already existed!" };
            }
            else
            {
                tbl_Account acountObj = _repositoryWrapper.Account.FindByCondition(x => x.showId == showId && x.a_ID == visitorId && x.deleteFlag == false).FirstOrDefault();
                if (acountObj != null)
                {
                    _repositoryWrapper.Account.UpdateVisitor(obj, acountObj);
                    return  new { data = "", success = true, message = "Successfully Updated." };
                }
                else
                {
                    visitorId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.AccountRunKey);
                    _repositoryWrapper.Account.SaveNewVisitor(obj, visitorId);
                    _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.AccountRunKey);
                    return  new { data = visitorId, success = true, message = "Successfully Created." };
                }
            }
        }

        public void DeleteVisitorByVisitorId(string visitorId)
        {
            tbl_Account acountObj = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == visitorId 
            && x.deleteFlag == false && x.showId == showId).FirstOrDefault();
            if (acountObj != null)
            {
                _repositoryWrapper.Account.DeleteVisitor(acountObj);
            }
        }

        public dynamic GetFavouriteExhibitorsByVisitorId(string Id){
            return _repositoryWrapper.Account.GetFavouriteExhibitorsByVisitorId(Id);
        }

        public dynamic GetFavouriteProductsByVisitorId(string Id){
            return _repositoryWrapper.Account.GetFavouriteProductsByVisitorId(Id);
        }
        
        #region VisitedExhibitor
        public dynamic GetVisitedExhibitor(dynamic obj, string userId)
        {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "CompanyName";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var objResult = _repositoryWrapper.Account.GetVisitedExhibitor(obj, sortField, sortBy, userId);
            return objResult;
        }
        #endregion

         public void AddUsersFavouriteByVisitor(string vId, string exhId){
            tbl_UsersFavourite obj = _repositoryWrapper.Favourite.FindByCondition(x => x.showId == showId && x.ItemType == FavouriteItem.Exhibitor
            && (x.Status == 1 || x.Status == 0) && x.UserId == vId && x.FavItemId == exhId).FirstOrDefault();
            if(obj == null){
                //Add a new visitor favourite to exhibitor
                _repositoryWrapper.Favourite.SaveNewUserFavourite(vId,exhId,FavouriteItem.Exhibitor);
            }else
            {
                //Update based on the existing visitor favourite status
                obj = _repositoryWrapper.Favourite.FindByCondition(x => x.showId == showId && x.Status == 0 && 
                x.ItemType == FavouriteItem.Exhibitor && x.UserId == vId && x.FavItemId == exhId).FirstOrDefault();
                if(obj.Status == 0){
                    _repositoryWrapper.Favourite.UpdateUserFavourite(obj,1);
                }
            } 
        }

        public void RemoveUsersFavourite(string VId, string exhId){
            tbl_UsersFavourite obj = _repositoryWrapper.Favourite.FindByCondition(x => x.showId == showId && x.Status == 1 && 
                x.ItemType == FavouriteItem.Exhibitor && x.UserId == VId && x.FavItemId == exhId ).FirstOrDefault();
            if(obj != null){
                _repositoryWrapper.Favourite.UpdateUserFavourite(obj,0);
            }
        }

        public dynamic GetAllExhibitorProductsByVisitorId(dynamic obj,string exhId,string vId)
        {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            bool isDefaultSort = false;
            if (sortField == null || sortField == "")
                sortField = "p_name";
            isDefaultSort = true;
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objResult = _repositoryWrapper.Account.GetAllExhibitorProductsByVisitorId(obj, sortField, sortBy, isDefaultSort, exhId, vId);
            return objResult;
        }

        public dynamic GetFavouriteExhStatusByVisitor(string vId, string exhId){
            var result = _repositoryWrapper.Account.GetFavouriteExhStatusByVisitor(vId,exhId);
            return result;
        }

        public dynamic GetVisitorByCountry(){
            return _repositoryWrapper.Account.GetVisitorsByCountry();
        }

         public dynamic GetActiveLoginUsers(dynamic obj,bool isexport){
            return _repositoryWrapper.Account.GetActiveLoginUsers(obj,isexport);
        }

        public dynamic GetDays()
        {
            return _repositoryWrapper.Account.GetDays();
        }

        public void updateProfilePicUrl(string Id, string url)
        {
            tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == Id && x.showId == showId && x.deleteFlag == false).FirstOrDefault();
            if (account != null)
            {
                _repositoryWrapper.Account.updateProfilePicUrl(account, url);
            }
        }

        public dynamic GetSalutation(){
            return _repositoryWrapper.Account.GetSalutation();
        }

        public dynamic DownloadExcelTemplate(){
            
            return ExcelUpload.DownloadExcelTemplate("Account");
        }

        public dynamic UploadExcelFile(IFormFile file)
        {
            //1.Validation Process, 2.Save to temp db, 3.Return result set to UI
            dynamic result = null;
            DataTable dt = ExcelUpload.UploadExcelFile(file);
            List<TempAccount> lst = new List<TempAccount>();
            lst = ExcelUpload.ConvertDataTable<TempAccount>(dt);
            if (lst.Count > 0)
            {
                string templateId = generateGUID();
                lst.ForEach(i => { i.templateId = templateId; i.status = true; i.showId = showId; });
                lst.Where(e => IsValidAccountType(e.a_type) == false).ToList().ForEach(i => { i.remark = "Invalid account type."; i.status = false; });
                lst.Where(e => GlobalFunction.IsValidEmail(e.a_email) == false).ToList().ForEach(i => { i.remark = "Invalid email."; i.status = false; });
                _repositoryWrapper.Account.UpdateTempAccount(lst);
                return getTempAccounts(null);
            }
            return result;
        }

        public dynamic SaveExcelData(dynamic obj){
            //Generate RunNumber and Password, update Account
            //2 types to save, 1. All, 2. By emails
            dynamic emails = obj.data;
            string updatedBy = string.Empty;
            updatedBy = obj.updatedBy;
            if(updatedBy.Trim().ToLower() == "all"){
                emails = _repositoryWrapper.TempAccount.FindByCondition(x => x.showId == showId && x.status == true).Select(x => x.a_email).AsNoTracking().Distinct().ToList();
            }
            if(emails == null) {
                return new { data = "", success = true, savedCount = 0, message = "There is no data to update." };
            }
            List<tbl_Account> lst = new List<tbl_Account>();
            var runNoObj = _repositoryWrapper.SiteRunNumber.FindByCondition(x => x.prefix == SiteRunNumbers.AccountRunKey).AsNoTracking().FirstOrDefault();
            string prefix = runNoObj.prefix;
            int run_cur = runNoObj.runno_curr;
            int run_incr = runNoObj.runno_incr;
            foreach(var email in emails){

                string e = email.ToString();
                string runNum = string.Empty;
                TempAccount temp = _repositoryWrapper.TempAccount.FindByCondition(x => x.a_email == e && x.showId == showId && x.status == true).FirstOrDefault();
                if (temp == null)
                {
                    //add invalid records
                    continue;
                }
                tbl_Account account = new tbl_Account();
                runNum = prefix + run_cur;
                account.showId = showId;
                account.a_fname = temp.a_fname;
                account.a_fullname = temp.a_fullname;
                account.a_lname = temp.a_lname;
                account.a_LoginName = temp.a_LoginName;
                account.a_Mobile = temp.a_Mobile;
                account.a_type = temp.a_type;
                account.a_email = temp.a_email;
                account.a_addressOfc = temp.a_addressOfc;
                account.a_company = temp.a_company;
                account.a_designation = temp.a_designation;
                account.a_country = temp.countryCode;
                account.a_Password = GlobalFunction.GenerateSalthKey(temp.a_Password);
                account.RegPortalID = temp.RegPortalID;
                account.Signed_Date = temp.Signed_Date;
                account.Biography = temp.biography;
                account.ChatActiveStatus = 1;
                account.deleteFlag = false;
                account.ModifiedDate = DateTime.Now;
                account.UpdatedDate = DateTime.Now;
                account.a_ID = runNum;
                lst.Add(account);
                run_cur += run_incr;
            }
            if(lst.Count > 0){
                bool success = false; string invalid = string.Empty;
                success = _repositoryWrapper.Account.UpdateAccounts(lst);
                if(success){
                    UpdateAccountRunNumber(run_cur,runNoObj);
                    return new { data = "", success = true, savedCount = lst.Count, message = "Successfully updated." + invalid };
                }
                return new { data = "", success = false, savedCount = lst.Count, message = "Fail to update." };
            }
            return new { data = "", success = false, savedCount = 0, message = "Fail to update." };
        }

        public dynamic getTempAccounts(dynamic obj){
            return _repositoryWrapper.Account.getTempAccounts(obj);
        }

        private void UpdateAccountRunNumber(int value,SiteRunNumber runNoObj){
            //Update last runNumberkey
            runNoObj.runno_curr = value;
            _repositoryWrapper.SiteRunNumber.Update(runNoObj);
            _repositoryWrapper.Save();
        }

        private bool IsValidAccountType(string type){
            
            //var accTypes = typeof(UserType).GetFields().Select(field => field.GetValue(typeof(UserType))).ToList();
            if(UserType.Speaker == type || UserType.Delegate == type){
                return true;
            }
            return false;
        }
         private string generateGUID(){
            Guid guid = Guid.NewGuid();
            return guid.ToString();
        }

        public dynamic changePassword(dynamic obj){
            try
            {
                string contactID = obj.contactId;
                string pwd = obj.password;
                tbl_Account contactPerson = _repositoryWrapper.Account.FindByCondition(x => x.showId == showId && x.a_ID == contactID && x.showId == showId).FirstOrDefault();
                if (contactPerson == null)
                {
                    return new { data = "", success = false, message = "No data to update." };
                }
                _repositoryWrapper.Account.ChangePassword(pwd, contactPerson);
                return new { data = "", success = true, message = "Successfully Updated." };
            }
            catch (System.Exception)
            {
                return new { data = "", success = false, message = "Failed to update." };
            }
        }

    }
}