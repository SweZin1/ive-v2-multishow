using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using iVE.DAL;
namespace iVE.BLL.Implementation
{
    public class SponsorLogic : ISponsorLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        string showId = string.Empty;
        public SponsorLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }

        public dynamic GetSponsors()
        {
            return _repositoryWrapper.Sponser.GetSponsors();
        }

        public dynamic GetSponsor(string sponsorId)
        {
            dynamic result = null;
            var sObj = _repositoryWrapper.Sponser.FindByCondition(x => x.showId == showId && x.s_ID == sponsorId && x.deleteFlag == false).FirstOrDefault();
            if(sObj != null){
                result = _repositoryWrapper.Sponser.GetSponsorById(sponsorId);
            }
            return result;
        }

        public string CreateOrUpdateSponsor(dynamic obj, string sponsorId)
        {
            int? seqNo = null; int count = 0; string message = "";
            seqNo = obj.seq;
            string exhId = null;
            //Check duplicate sequence No
            count = _repositoryWrapper.Sponser.FindByCondition(x => x.showId == showId && x.deleteFlag == false
                && x.s_seq == seqNo && (sponsorId == "0" || sponsorId == "" || x.s_ID != sponsorId)).Select(x => x.s_seq).Count();
            if(count > 0){
                message = "Duplicated sequence No."; 
            }else{
                
                string templateId = obj.templateId;
                if(!string.IsNullOrEmpty(templateId)){
                    exhId = _repositoryWrapper.Template.FindByCondition(x => x.showId == showId && x.TemplateID == templateId)
                .Select(x => x.ExhID).FirstOrDefault();
                }
                tblSponsor sponsor = _repositoryWrapper.Sponser.FindByCondition(x => x.showId == showId && x.s_ID == sponsorId && x.deleteFlag == false).FirstOrDefault();
                if (sponsor != null)
                {
                    obj.exhID = exhId;
                    _repositoryWrapper.Sponser.UpdateSponsor(obj, sponsor);
                }
                else
                {
                    sponsorId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.SponsorRunKey);
                    tblSponsor sObj = new tblSponsor();
                    sObj.s_ID = sponsorId;
                    sObj.s_exhID = exhId;
                    _repositoryWrapper.Sponser.SaveNewSponsor(obj, sObj);
                    _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.SponsorRunKey);
                }
            }
            return message;
        }

        public void DeleteSponsorBySponsorId(string sponsorId)
        {
            tblSponsor sponsor = _repositoryWrapper.Sponser.FindByCondition(x => x.showId == showId && x.s_ID == sponsorId && x.deleteFlag == false).FirstOrDefault();
            if (sponsor != null)
            {
                _repositoryWrapper.Sponser.DeleteSponsor(sponsor);
            }
        }

        public dynamic GetSponsorList(dynamic obj){
            return _repositoryWrapper.Sponser.GetSponsorList(obj);
        }
        public dynamic GetSponsorsByCateGrouping(dynamic obj){
            return _repositoryWrapper.Sponser.GetSponsorListByCateGrouping(obj);
        }

        public dynamic GetSponsorDetails(string Name){
            return _repositoryWrapper.Sponser.GetSponsorDetails(Name);
        }
    }
}