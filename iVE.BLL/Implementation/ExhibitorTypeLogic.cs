using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using iVE.DAL;
using System.Collections.Generic;
using System.Data;
using System;

namespace iVE.BLL.Implementation
{
    public class ExhibitorTypeLogic : IExhibitorTypeLogic
    {
        string showId = string.Empty;
        private IRepositoryWrapper _repositoryWrapper;
        public ExhibitorTypeLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }

        public dynamic createorUpdateExhibitorType(string Id, dynamic obj)
        {
            bool success = false;
            tblExhibitorType exhType = _repositoryWrapper.ExhibitorType.FindByCondition(x => x.showId == showId
            && x.deleteFlag == false && x.ID == Convert.ToInt16(Id)).FirstOrDefault();
            if(exhType == null){
                //Add New
                success = _repositoryWrapper.ExhibitorType.AddNewExhibitorType(obj);
            }else{
                //Update
                success = _repositoryWrapper.ExhibitorType.UpdateExhibitorType(obj, exhType);
            }
            if(!success){
                return new { data = "", success = false, message = "Failed to update." };
            }
            return new { data = "", success = true, message = "Successfully updated." };
        }

        public dynamic deleteExhibitorType(string Id)
        {
            bool success = false;
            tblExhibitorType exhType = _repositoryWrapper.ExhibitorType.FindByCondition(x => x.showId == showId && x.deleteFlag == false && x.ID == Convert.ToInt16(Id)).FirstOrDefault();
            if(exhType != null){
                tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition(x => x.showId == showId && x.ExhType == Id).FirstOrDefault();
                if(exhibitor != null){
                    return new { data = "", success = false, message = "Exhibitor type is still using in exhibitor." };
                }
                success = _repositoryWrapper.ExhibitorType.deleteExhibitorType(exhType);
                if(!success){
                    return new { data = "", success = false, message = "Failed to delete." };
                }
                return new { data = "", success = true, message = "Successfully deleted." };
            }
            return new { data = "", success = false, message = "No data to delete." };
        }

        public dynamic getExhibitorTypeDetails(string Id)
        {
            int typeId = Convert.ToInt16(Id);
            tblExhibitorType exhType = _repositoryWrapper.ExhibitorType.FindByCondition(x => x.showId == showId && x.deleteFlag == false && x.ID == typeId).FirstOrDefault();
            if(exhType != null){
                dynamic data = _repositoryWrapper.ExhibitorType.getExhibitorTypeDetails(typeId);
                return new { data = data, success = true, message = "Successfully retrieved." };
            }
            return new { data = "", success = false, message = "No data to retrieve." };
        }

        public dynamic getExhibitorTypes()
        {
            return _repositoryWrapper.ExhibitorType.getExhibitorTypes();
        }

        public dynamic getExhibitorTypes(dynamic obj)
        {
            return _repositoryWrapper.ExhibitorType.getExhibitorTypes(obj);
        }
    }
}