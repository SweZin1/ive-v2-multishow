using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.BLL.Implementation {
    public class ShowConfigLogic : IShowConfigLogic {
        private IRepositoryWrapper _repositoryWrapper;
        public ShowConfigLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public dynamic GetShowConfig () {
            return _repositoryWrapper.ShowConfig.GetShowConfig ();
        }

        public dynamic CreateOrUpdateShowConfig (dynamic obj) {
            string name = obj.name; bool success = false;
            tblShowConfig showconfig = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == name && x.Status == 1).FirstOrDefault ();
            if (showconfig == null) {
                return new { data = name, success = false, message = "No data to update." };
            }
            success = _repositoryWrapper.ShowConfig.UpdateShowConfig (obj, showconfig);
            if(!success){
                return new { data = name, success = false, message = "Failed to update." };
            }
            return new { data = name, success = true, message = "Successfully updated." };
        }

    }
}