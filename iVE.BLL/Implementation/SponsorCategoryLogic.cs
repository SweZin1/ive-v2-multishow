using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using iVE.DAL;
namespace iVE.BLL.Implementation {
    public class SponsorCategoryLogic : ISponsorCategoryLogic {
        private IRepositoryWrapper _repositoryWrapper;
        string showId = string.Empty;
        public SponsorCategoryLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }

        public dynamic GetSponsorCategories () {
            return _repositoryWrapper.SponsorCategory.GetSponsorCategories ();
        }

        public dynamic GetSponsorCategory(string id)
        {
            return _repositoryWrapper.SponsorCategory.GetSponsorCategory(id);
        }

        public dynamic UpdateOrCreateSponsorCategory(string id, dynamic obj)
        {
            bool success = false;
            tblSponsorCategory objCategory = _repositoryWrapper.SponsorCategory.FindByCondition(x =>x.showId == showId && x.deleteFlag ==false && x.sponsorCat_ID == id).FirstOrDefault();
            if(objCategory == null){
                //add new
                id = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.SponsorCategoryRunKey);
                success = _repositoryWrapper.SponsorCategory.AddNewSponsorCategory(id,obj);
                if(!success){
                    return new { data = "", success = false, message = "Failed to create." };
                }
                _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.SponsorCategoryRunKey);
            }else{
                //update
                success = _repositoryWrapper.SponsorCategory.UpdateSponsorCategory(obj,objCategory);
                if(!success){
                    return new { data = "", success = false, message = "Failed to create." };
                }
            }
            return new { data = id, success = true, message = "Successfully updated." };
        }

        public dynamic DeleteSponsorCategory(string id)
        {
            bool success = false;
            tblSponsorCategory objCategory = _repositoryWrapper.SponsorCategory.FindByCondition(x =>x.showId == showId && x.deleteFlag ==false && x.sponsorCat_ID == id).FirstOrDefault();
            if(objCategory != null){
                tblSponsor sponsor = _repositoryWrapper.Sponser.FindByCondition(x => x.showId == showId && x.s_category == id && x.deleteFlag ==false).FirstOrDefault();
                if(sponsor != null){
                    return new { data = "", success = false, message = "This category is still using in sponsor." };
                }
                success = _repositoryWrapper.SponsorCategory.DeleteSponsorCategory(objCategory);
                if(!success){
                    return new { data = "", success = false, message = "Failed to delete." };
                }
                return new { data = "", success = true, message = "Successfully deleted." };
            }
            return new { data = "", success = false, message = "No data to delete." };
        }
    }
}