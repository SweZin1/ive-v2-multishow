using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using iVE.DAL;
namespace iVE.BLL.Implementation
{
    public class TemplateConfigLogic : ITemplateConfigLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        string showId = string.Empty;
        public TemplateConfigLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }

        public dynamic GetTemplate(dynamic obj)
        {
            return _repositoryWrapper.TemplateConfig.GetTemplate(obj);
        }

        public dynamic GetTemplateConfigList(dynamic obj)
        {
            return _repositoryWrapper.TemplateConfig.GetTemplateList(obj);
        }

        public dynamic AddOrUpdateTemplateConfig(dynamic templateData)
        {
            if (templateData != null)
            {
                string templateType = templateData.templateType;
                int sequenceNo = 0;
                if (templateData.sequenceNo != null)
                {
                    sequenceNo = Convert.ToInt32(templateData.sequenceNo);
                }
                string category = templateData.category;
                if (templateData.id == null || templateData.id == 0) // Create
                {
                    //Check Same Template Type & Sequence & Category
                    dynamic duplicateCount = _repositoryWrapper.TemplateConfig.FindByCondition(x => x.showId == showId && x.templateType == templateType && x.sequenceNo == sequenceNo && x.category == category && x.deleteFlag == false && x.templateType != TemplateConfigTemplate.Lobby).Count();
                    if (duplicateCount > 0)
                    {
                        return new { data = false, success = false, message = "Already existed with same type with same sequence no and category" };
                    }
                    else
                    {
                        int Id = _repositoryWrapper.TemplateConfig.AddTemplateConfig(templateData);
                        return new { data = Id, success = true, message = "Successfully created" };
                    }
                }
                else // Update
                {
                    int Id = templateData.id;
                    dynamic duplicateCount = 0;
                    //Check Same Template Type & Sequence & Category

                    if (!string.IsNullOrEmpty(templateType) && sequenceNo >= 0)
                    {
                        duplicateCount = _repositoryWrapper.TemplateConfig.FindByCondition(x => x.showId == showId && x.templateType == templateType && x.sequenceNo == sequenceNo && x.category == category && x.deleteFlag == false && x.Id != Id && x.templateType != TemplateConfigTemplate.Lobby).Count();
                    }

                    if (duplicateCount > 0)
                    {
                        return new { data = false, success = false, message = "Already existed with same type with same sequence no and category" };
                    }
                    else
                    {
                        Id = _repositoryWrapper.TemplateConfig.UpdateTemplateConfig(templateData);
                        return new { data = Id, success = true, message = "Successfully updated" };
                    }
                }
            }
            else
            {
                return new { data = false, success = false, message = "There is no data to save!" };
            }
        }

        public dynamic ChangeStatus(int id, string status)
        {
            tbl_TemplateConfig tObj = _repositoryWrapper.TemplateConfig.FindByCondition(x => x.showId == showId && x.Id == id).FirstOrDefault();
            if ((tObj.templateType == TemplateConfigTemplate.Lobby || tObj.templateType == TemplateConfigTemplate.FloorPlan) 
            && status == TemplateConfigStatus.Publish)
            {
                _repositoryWrapper.TemplateConfig.ChangePendingStatus(id);
            }
            return _repositoryWrapper.TemplateConfig.ChangeStatus(id, status);
        }

        public void SaveUrl(int id, string url, string mediaType)
        {
            if (mediaType == "image")
            {
                _repositoryWrapper.TemplateConfig.SaveImageUrl(id, url);
            }
            else if (mediaType == "video")
            {
                _repositoryWrapper.TemplateConfig.SaveVideoUrl(id, url);
            }
        }

        public dynamic GetTemplateConfig(int id)
        {
            return _repositoryWrapper.TemplateConfig.GetTemplateConfig(id);
        }

        public bool DeleteTemplateConfig(int id)
        {
            return _repositoryWrapper.TemplateConfig.DeleteTemplateConfig(id);
        }

    }
}