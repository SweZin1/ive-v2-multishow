using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using iVE.DAL;
using System.Collections.Generic;
using System.Data;
using System;
namespace iVE.BLL.Implementation
{
    public class ExhibitorCategoryLogic : IExhibitorCategoryLogic
    {
        string showId = string.Empty;
        private IRepositoryWrapper _repositoryWrapper;
        public ExhibitorCategoryLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }
        public dynamic createorUpdateExhibitorCategory(string Id, dynamic obj)
        {
            bool success = false;
            string category = obj.category;
            if(obj != null && !string.IsNullOrEmpty(category)){
                tblExhibitorCategory exhCategory = _repositoryWrapper.ExhibitorCategory.FindByCondition(x => x.showId == showId
                && x.deleteFlag == false && x.ID == Convert.ToInt16(Id) && x.Category.ToLower() == category.ToLower()).FirstOrDefault();
                if(exhCategory != null){
                    return new { data = "", success = false, message = "Duplicate exhibitor category." };
                }
                exhCategory = _repositoryWrapper.ExhibitorCategory.FindByCondition(x => x.showId == showId
                && x.deleteFlag == false && x.ID == Convert.ToInt16(Id)).FirstOrDefault();
                if (exhCategory == null)
                {
                    //Add New
                    success = _repositoryWrapper.ExhibitorCategory.AddNewExhibitorCategory(obj);
                }
                else
                {
                    //Update
                    success = _repositoryWrapper.ExhibitorCategory.UpdateExhibitorCategory(obj, exhCategory);
                }
                if (!success)
                {
                    return new { data = "", success = false, message = "Failed to update." };
                }
                return new { data = "", success = true, message = "Successfully updated." };
            }
            return new { data = "", success = false, message = "No data to update." };
        }

        public dynamic deleteExhibitorCategory(string Id)
        {
            bool success = false;
            tblExhibitorCategory exhCategory = _repositoryWrapper.ExhibitorCategory.FindByCondition(x => x.showId == showId
            && x.deleteFlag == false && x.ID == Convert.ToInt16(Id)).FirstOrDefault();
            if(exhCategory != null){
                tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition(x => x.showId == showId && x.ExhCategory == Id).FirstOrDefault();
                if(exhibitor != null){
                    return new { data = "", success = false, message = "Exhibitor category is still using in exhibitor." };
                }
                success = _repositoryWrapper.ExhibitorCategory.deleteExhibitorCategory(exhCategory);
                if(!success){
                    return new { data = "", success = false, message = "Failed to delete." };
                }
                return new { data = "", success = true, message = "Successfully deleted." };
            }
            return new { data = "", success = false, message = "No data to delete." };
        }

        public dynamic getExhibitorCategories()
        {
            try{
                dynamic data = _repositoryWrapper.ExhibitorCategory.getExhibitorCategories();
                return new { data = data, success = true, message = "Successfully reterieved." };
            }catch(Exception){
                return new { data = "", success = false, message = "Failed to retrieve." };
            }
        }

        public dynamic getExhibitorCategories(dynamic obj)
        {
            try{
                return _repositoryWrapper.ExhibitorCategory.getExhibitorCategories(obj);
            }catch(Exception){
                return new { data = "", dataFoundRowsCount = 0, result = false, message = "No data to show." };
            }
        }

        public dynamic getExhibitorCategoryDetails(string Id)
        {
            int typeId = Convert.ToInt16(Id);
            tblExhibitorCategory exhCategory = _repositoryWrapper.ExhibitorCategory.FindByCondition(x => x.showId == showId && x.deleteFlag == false && x.ID == typeId).FirstOrDefault();
            if(exhCategory != null){
                dynamic data = _repositoryWrapper.ExhibitorCategory.getExhibitorCategoryDetails(typeId);
                return new { data = data, success = true, message = "Successfully retrieved." };
            }
            return new { data = "", success = false, message = "No data to retrieve." };
        }
    }
}