using iVE.BLL.Interface;
using iVE.DAL.Repository.Interface;
using System.Linq;
using System.Collections.Generic;
using iVE.DAL.Models;
using iVE.DAL.Util;
using System;
using iVE.DAL;
namespace iVE.BLL.Implementation
{
    public class ShowLogic : IShowLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        string _showId = string.Empty;
        public ShowLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
            _showId = LoginData.showId;
        }

        public dynamic GetShowBySuperAdmin(string id)
        {
            return _repositoryWrapper.Show.GetShowBySuperAdmin(id);
        }

        public dynamic GetShowListBySuperAdmin(dynamic obj)
        {
            return _repositoryWrapper.Show.GetShowListBySuperAdmin(obj);
        }

        public dynamic DeleteShowBySuperAdmin(string id)
        {
            tblShow show = _repositoryWrapper.Show.FindByCondition(x => x.showId == id && x.deleteFlag == false).FirstOrDefault();
            if (show != null)
            {
                _repositoryWrapper.Show.DeleteShowBySuperAdmin(id);
                return true;
            }
            return false;
        }

        public dynamic AddOrUpdateShowBySuperAdmin(dynamic obj)
        {
            string showId = obj.showId;
            tblShow show = _repositoryWrapper.Show.FindByCondition(x => x.showId == showId && x.deleteFlag == false).FirstOrDefault();
            if (show != null)
            {
                int duplicateResult = _repositoryWrapper.Show.CheckDuplicateForShow(obj, false);
                if (duplicateResult == 1)
                {
                    return new { result = false, message = "Show Prefix already exists!" };
                }
                if (duplicateResult == 2)
                {
                    return new { result = false, message = "Route Name already exists!" };
                }
                showId = _repositoryWrapper.Show.UpdateShowBySuperAdmin(obj);
            }
            else
            {
                int duplicateResult = _repositoryWrapper.Show.CheckDuplicateForShow(obj, true);
                if (duplicateResult == 1)
                {
                    return new { result = false, message = "Show Prefix already exists!" };
                }
                if (duplicateResult == 2)
                {
                    return new { result = false, message = "Route Name already exists!" };
                }
                showId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.Show);
                string GUID = System.Guid.NewGuid().ToString();
                obj.showId = showId + "-" + GUID;
                showId = _repositoryWrapper.Show.AddShowBySuperAdmin(obj);
                _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.Show);
            }

            // Show Day Creation
            DateTime sDate = obj.showStartDate;
            DateTime eDate = obj.showEndDate;
            DateTime currentDate = sDate;

            while (currentDate <= eDate)
            {
                tbl_Day tmpDayObj = _repositoryWrapper.Day.FindByCondition(x => x.d_date.Date == currentDate.Date && x.showId == showId).FirstOrDefault();
                // Create
                if (tmpDayObj == null)
                {
                    string dayId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.Day);
                    _repositoryWrapper.Day.AddDay(currentDate, dayId, showId);
                    _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.Day);
                }
                else
                {
                    // Update
                    if (tmpDayObj.deleteFlag == true)
                    {
                        _repositoryWrapper.Day.UpdateDay(tmpDayObj.d_ID);
                    }
                }
                currentDate = currentDate.AddDays(1);
            }
            // Delete
            _repositoryWrapper.Day.DeleteDays(sDate, eDate, showId);
            return new { showId = showId, result = true, message = "Success" };
        }

        public dynamic UpdateShowByOrganizer(dynamic obj)
        {
            tblShow show = _repositoryWrapper.Show.FindByCondition(x => x.showId == _showId && x.deleteFlag == false).FirstOrDefault();
            if (show != null)
            {
                _repositoryWrapper.Show.UpdateShowByOrganizer(obj);
                return new { data = _showId, success = true, message = "Success" };
            }
            return new { data = _showId, success = true, message = "Can't update Show!" };
        }

        public dynamic GetShowByOrganizer()
        {
            return _repositoryWrapper.Show.GetShowByOrganizer();
        }

        public void ChangeStatusForShow(dynamic obj)
        {
            string showId = obj.showId;
            string status = obj.status;
            tblShow show = _repositoryWrapper.Show.FindByCondition(x => x.showId == showId && x.deleteFlag == false).FirstOrDefault();
            if (show != null)
            {
                _repositoryWrapper.Show.ChangeStatusForShow(showId, status);
            }
        }

        public dynamic GetThemeConfigFileName(string id)
        {
            tblShow show = _repositoryWrapper.Show.FindByCondition(x => x.showId == id && x.deleteFlag == false).FirstOrDefault();
            if (show != null)
            {
                return _repositoryWrapper.Show.GetThemeConfigFileName(id);
            }
            return "";
        }

        public void UpdateShowConfigUrl(string id, string url)
        {
            GlobalFunction.WriteSystemLog(LogType.Info, "show", "show Id : " + id + "url : " + url, "");
            tblShow show = _repositoryWrapper.Show.FindByCondition(x => x.showId == id && x.deleteFlag == false).FirstOrDefault();
            if (show != null)
            {
                _repositoryWrapper.Show.UpdateShowConfigUrl(id, url);
            }
        }
    }
}