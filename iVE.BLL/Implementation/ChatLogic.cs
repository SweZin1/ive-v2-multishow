using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using iVE.BLL.ChatAPI;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using iVE.DAL;
namespace iVE.BLL.Implementation {
    public class ChatLogic : IChatLogic {
        private IRepositoryWrapper _repositoryWrapper;
        string showId = string.Empty;
        string showPrefix = string.Empty;
        public ChatLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }

        public bool JoinChatWithContactPerson (string exhId, string userId) {
            bool result = false;
            var showObj = _repositoryWrapper.Show.FindByCondition (x => x.showId == showId && x.deleteFlag == false).FirstOrDefault();
            if(showObj == null){
                return false;
            }
            showPrefix = showObj.showPrefix;
            int chatId = Convert.ToInt16(showObj.chatId);
            tbl_Account account = _repositoryWrapper.Account.FindByCondition (x => x.showId == showId && x.a_ID == userId && x.deleteFlag == false).FirstOrDefault ();
            tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition (x => x.showId == showId && x.ExhID == exhId ).FirstOrDefault ();
            List<string> contactPersonList = _repositoryWrapper.Account.FindByCondition (x => x.showId == showId && x.ExhId == exhId && x.deleteFlag == false).Select (x => x.a_ID).ToList ();
            if (account == null && exhibitor == null) {
                return false;
            }
            tblChatConfiguration chatConfig = _repositoryWrapper.ChatConfiguration.FindByCondition(x => x.Id == chatId && x.deleteFlag == false).FirstOrDefault();
            if (chatConfig == null)
            {
                GlobalFunction.WriteSystemLog (LogType.Error, "ChatLogic - JoinChatWithContactPerson", "Invalid chat configuration.", "");
                return false;
            }
            string gId = (exhId + "_" + userId).ToLower();
            string gName = exhibitor.CompanyName;
            bool isOk = GroupAPI.CreateGroup(gId, gName, showPrefix, chatConfig);
            GlobalFunction.WriteSystemLog(LogType.Info, "ChatLogic - JoinChatWithContactPerson", "CreateStatus: " + isOk, "showId:" + showId + "showPrefix:" + showPrefix);
            if (isOk)
            {
                GroupAPI.JoinMember(userId, gId, UserType.Delegate, showPrefix, chatConfig);
                foreach (var item in contactPersonList)
                {
                    GroupAPI.JoinMember(item, gId, UserType.Speaker, showPrefix, chatConfig);
                }
                result = true;
            }
            return result;
        }

        
    }
}