using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using System.IO;


namespace iVE.BLL.Implementation
{
    public class FileStorageLogic : IFileStorageLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        private readonly IEnumerable<IStorageStrategy> _storageStrategies;
        public FileStorageLogic(IRepositoryWrapper repositoryWrapper, IEnumerable<IStorageStrategy> storageStrategies)
        {
            _repositoryWrapper = repositoryWrapper;
            _storageStrategies = storageStrategies;
        }

        public async Task<dynamic> UploadFile(IFormFile file, string folderName, string subFolderName, string fileName, StorageStrategy ss)
        {
            return await _storageStrategies.FirstOrDefault(x => x.StorageStrategy == ss)?.UploadFile(file, folderName, subFolderName, fileName) ?? throw new ArgumentNullException(nameof(ss));
        }

        public async Task<dynamic> ReadFile(string filePath, string fileName, StorageStrategy ss)
        {
            return await _storageStrategies.FirstOrDefault(x => x.StorageStrategy == ss)?.ReadFile(filePath, fileName) ?? throw new ArgumentNullException(nameof(ss));
        }
        public void WriteFile(Stream data, string filePath, string fileName, StorageStrategy ss)
        {
            _storageStrategies.FirstOrDefault(x => x.StorageStrategy == ss)?.WriteFile(data, filePath, fileName);
        }

    }
}