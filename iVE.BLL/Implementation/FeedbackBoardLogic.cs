using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.Extensions.Configuration;
namespace iVE.BLL.Implementation {
    public class FeedbackBoardLogic : IFeedbackBoardLogic {
        private IRepositoryWrapper _repositoryWrapper;
        public FeedbackBoardLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public dynamic GetFeedbacksbyThreadOwnerId (dynamic obj, string Id) {
            return _repositoryWrapper.FeedBackBoard.GetFeedbacksbyThreadOwnerId (obj, Id);
        }

        public dynamic GetAllComments (dynamic obj, string febId) {
            return _repositoryWrapper.FeedBackBoard.GetAllComments (obj, febId);
        }

        public void CreateComment (dynamic obj) {
            _repositoryWrapper.FeedBackBoard.CreateComment (obj);
        }
        public void CreateCommentActions (dynamic obj) {
            _repositoryWrapper.FeedBackBoard.CreateCommentActions (obj);
        }

        public void UpdateCommentStatus (string febId, string cmtId, dynamic obj, string loginId) {
            _repositoryWrapper.FeedBackBoard.UpdateCommentStatus (febId, cmtId, obj, loginId);
        } 
    }
}