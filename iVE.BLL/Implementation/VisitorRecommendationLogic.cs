using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using System.Collections.Generic;
using iVE.DAL;
namespace iVE.BLL.Implementation
{
    public class VisitorRecommendationLogic : IVisitorRecommendationLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        string showId = string.Empty;
        public VisitorRecommendationLogic(IRepositoryWrapper rw){
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }

        public dynamic GetAllRecommendedExhibitorsByVisitor(string vId){
            return _repositoryWrapper.VisitorRecommendations.GetAllRecommendedExhibitorsByVisitor(vId);
        }

        public string AddVisitorRecommendation(dynamic obj, string vId)
        {
            dynamic recomList = obj.RecommendedList;
            bool resetAll = obj.ResetAll;
            tblVisitorRecommendations visitorRecObj = new tblVisitorRecommendations();
            List<tblVisitorRecommendations> lst = new List<tblVisitorRecommendations>();
            lst = _repositoryWrapper.VisitorRecommendations.FindByCondition(x => x.showId == showId && x.VisitorID == vId).ToList();
            if (lst.Count > 0 && resetAll == true)
            {
                // delete first the existing records by visitorID, add all new records
                foreach (var i in lst)
                {
                    visitorRecObj = _repositoryWrapper.VisitorRecommendations.FindByCondition(x => x.showId == showId && x.VisitorID == vId && x.ExhID == i.ExhID).SingleOrDefault();
                    if (visitorRecObj != null)
                    {
                        _repositoryWrapper.VisitorRecommendations.AddVisitorRecommendationLog(visitorRecObj);
                        _repositoryWrapper.VisitorRecommendations.Delete(visitorRecObj);
                    }
                }
                UpdateVisitorRecommendation(recomList, vId, true);
            }
            else if (lst.Count > 0 && resetAll == false)
            {
                //If exist vId and exhId, skip. If not exist, insert new record.
                UpdateVisitorRecommendation(recomList, vId, false);
            }
            else if (lst.Count == 0)
            {
                //Add all new records
                UpdateVisitorRecommendation(recomList, vId, true);
            }
            return vId;
        }

        public void UpdateVisitorRecommendation(dynamic objRecList, string vId, bool resetAll)
        {
            string exhibitorID = "";
            if (resetAll)
            {
                for (int i = 0; i < objRecList.Count; i++)
                {
                    exhibitorID = objRecList[i];
                    _repositoryWrapper.VisitorRecommendations.AddVisitorRecommendation(vId, exhibitorID);
                }
            }
            else
            {

                for (int i = 0; i < objRecList.Count; i++)
                {
                    tblVisitorRecommendations visitorRecObj = new tblVisitorRecommendations();
                    exhibitorID = objRecList[i];
                    visitorRecObj = _repositoryWrapper.VisitorRecommendations.FindByCondition(x => x.showId == showId && x.VisitorID == vId && x.ExhID == exhibitorID).SingleOrDefault();
                    if (visitorRecObj == null)
                    {
                        _repositoryWrapper.VisitorRecommendations.AddVisitorRecommendation(vId, exhibitorID);
                    }
                }

            }
        }
    }

}