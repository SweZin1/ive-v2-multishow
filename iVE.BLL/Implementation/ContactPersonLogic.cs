using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using iVE.DAL;
namespace iVE.BLL.Implementation
{
    public class ContactPersonLogic : IContactPersonLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        string showId = string.Empty;
        public ContactPersonLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
            showId = LoginData.showId;
        }

        public dynamic AddOrUpdateContactPerson(dynamic obj, string exhId, string contactId)
        {
            string email = obj.email;
            string loginName = obj.loginName;
            string exhType = obj.type;

            var emailObj = _repositoryWrapper.Account
                .FindByCondition(x => x.showId == showId && x.deleteFlag == false && (contactId == "0" || x.a_ID != contactId) && x.a_email == email)
                .Select(x => x.a_email).FirstOrDefault();

            var userNameObj = _repositoryWrapper.Account
                .FindByCondition(x => x.showId == showId && (contactId == "0" || x.a_ID != contactId) && x.a_LoginName == loginName && x.deleteFlag == false)
                .Select(x => x.a_LoginName).FirstOrDefault();

            if (emailObj != null)
            {
                return new { data = "false", success = false, message = "Duplicate email." };
            }
            if (userNameObj != null)
            {
                return new { data = "false", success = false, message = "Duplicate user name." };
            }
            obj.LoginBlocker = _repositoryWrapper.AccountLogicBlocker
                    .FindByCondition(x => x.showId == showId && x.deleteFlag == false && x.LoginBlocker == UserBlockRule.Exh_Block)
                    .Select(x => x.ID).FirstOrDefault();

            tbl_Account contactPerson = _repositoryWrapper.Account.FindByCondition(x => x.showId == showId && x.a_ID == contactId && x.ExhId == exhId).FirstOrDefault();
            if (contactPerson == null)
            {
                if (exhType != ContactPersonType.MainExhibitor && !CheckBadgeEntitlement(exhId))
                {
                    return new { data = "false", success = false, message = "Can't create. Exceeded the max count for contact person." };
                }
                contactId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.AccountRunKey);
                _repositoryWrapper.Account.SaveNewContactPerson(obj, exhId, contactId);
                _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.AccountRunKey);
                // if (obj.addToRepresentative != null)
                // {
                //     bool addToRepresentative = obj.addToRepresentative;
                //     tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition(x => x.ExhID == exhId).FirstOrDefault();
                //     if (addToRepresentative)
                //     {
                //         _repositoryWrapper.Exhibitor.AddChatContactMember(exhId, contactId, exhibitor);
                //     }
                //     else
                //     {
                //         _repositoryWrapper.Exhibitor.RemoveChatContactMember(exhId, contactId, exhibitor);
                //     }
                // }
                return new { data = contactId, success = true, message = "Successfully Created." };
            }
            else
            {
                _repositoryWrapper.Account.UpdateContactPerson(obj, contactPerson);
                // if (obj.addToRepresentative != null)
                // {
                //     bool addToRepresentative = obj.addToRepresentative;
                //     tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition(x => x.ExhID == exhId).FirstOrDefault();
                //     if (addToRepresentative)
                //     {
                //         _repositoryWrapper.Exhibitor.AddChatContactMember(exhId, contactId, exhibitor);
                //     }
                //     else
                //     {
                //         _repositoryWrapper.Exhibitor.RemoveChatContactMember(exhId, contactId, exhibitor);
                //     }
                // }
                return new { data = contactId, success = true, message = "Successfully Updated." };
            }
        }

        public dynamic GetAllContactPersonByExhId(string exhId)
        {
            return _repositoryWrapper.Account.GetContactPersonListbyExhId(exhId);
        }
        public void DeleteContactPersonByExhId(string exhId, string contactId)
        {
            _repositoryWrapper.Account.DeleteContactPerson(exhId, contactId);
        }
        public dynamic GetContactPersonById(string exhId, string contactId)
        {
            return _repositoryWrapper.Account.GetContactPersonDetail(exhId, contactId);
        }

        public dynamic GetContactPersonByType(string type)
        {
            dynamic contactPerson = null;

            if (type == ContactPersonType.MainExhibitor)
            {
                contactPerson = _repositoryWrapper.Account.FindByCondition(x => x.showId == showId && x.a_type == ContactPersonType.MainExhibitor && x.deleteFlag == false).FirstOrDefault();
            }
            else if (type == ContactPersonType.ContactPerson)
            {
                contactPerson = _repositoryWrapper.Account.FindByCondition(x => x.showId == showId && x.a_type == ContactPersonType.ContactPerson && x.deleteFlag == false).ToList();
            }
            return contactPerson;
        }

        /*8-7-2020*/
        public dynamic GetAllContactPersonsWithStatusByExhId(string exhId)
        {
            dynamic data = new System.Dynamic.ExpandoObject();
            data.data = _repositoryWrapper.Account.GetAllContactPersonsWithStatusByExhId(exhId);
            data.groupChatStatus = _repositoryWrapper.Exhibitor.FindByCondition(x => x.showId == showId && x.ExhID == exhId).Select(x => x.GroupChatStatus).FirstOrDefault();
            data.exhId = exhId;
            return data;
        }

        public void ChangeContactPersonPassword(dynamic obj)
        {
            string contactID = obj.contactId;
            string pwd = obj.password;
            tbl_Account contactPerson = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == contactID).FirstOrDefault();
            if (contactPerson != null)
            {
                _repositoryWrapper.Account.ChangePassword(pwd, contactPerson);
            }
        }

        public bool CheckBadgeEntitlement(string exhId)
        {
            return _repositoryWrapper.Account.CheckBadgeEntitlement(exhId);
        }

        public bool ChangeExhibitorGroupChatStatus(string exhId, int type)
        {
            bool result = false;
            tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition(x => x.showId == showId && x.ExhID == exhId).FirstOrDefault();
            if (exhibitor != null)
            {
                exhibitor.GroupChatStatus = type;
                _repositoryWrapper.Exhibitor.Update(exhibitor);
                _repositoryWrapper.Save();
                result = true;
            }
            return result;
        }
    }
}