using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.AccessControl;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
namespace iVE.BLL
{
    public static class GlobalFunction
    {
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                bool regTest = Regex.IsMatch(email, @"^[a-zA-Z0-9._%+-]+[a-zA-Z0-9+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$");
                return (addr.Address == email) && regTest;
            }
            catch
            {
                return false;
            }
        }

        public static string GenerateSalthKey(string password)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string salt = Configuration.GetSection("SaltKey").Value;
            return SaltedHash.ComputeHash(salt, password);
        }

        public static void WriteSystemLog(string logType, string apiName, string logMessage, string userId)
        {
            if (logType == LogType.Info)
            {
                Log.Information("In {APIName} , {LogMessage} by {UserId}", apiName, logMessage, userId);
            }

            if (logType == LogType.Error)
            {
                Log.Error("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Fatal)
            {
                Log.Fatal("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Debug)
            {
                Log.Debug("In {APIName} , {LogMessage}", apiName, logMessage);
            }

            if (logType == LogType.Warn)
            {
                Log.Warning("In {APIName} , {LogMessage}", apiName, logMessage);
            }
        }


    }
}