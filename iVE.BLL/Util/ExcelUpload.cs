using System.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.IO;
using OfficeOpenXml;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace iVE.BLL
{
    public static class ExcelUpload
    {
       
        public static dynamic DownloadExcelTemplate(string fileName)
        {
            byte[] fileContents;
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string templatePath = Configuration.GetSection("appSettings:TemplatesPath").Value;
            string extension = ".xlsx";
            string filepath = templatePath + fileName + extension;
            byte[] fileBytes = System.IO.File.ReadAllBytes(filepath);
            fileContents = fileBytes;
            return fileContents;
        }
        public static DataTable UploadExcelFile(IFormFile file)
        {
            DataTable dtResult = new DataTable();
            try
            {
                var uploadFile = file;
                DataTable dt = new DataTable();
                ExcelEngine excelEngine = new ExcelEngine();

                //Instantiate the Excel application object
                IApplication application = excelEngine.Excel;
                //Assigns default application version
                application.DefaultVersion = ExcelVersion.Excel2013;

                string basePath = SaveExcelImportFile(uploadFile);
                FileStream sampleFile = new FileStream(basePath, FileMode.Open);

                IWorkbook workbook = application.Workbooks.Open(sampleFile);

                //Reading Excel
                dt = ReadExcel(workbook);

                try
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Closing the workbook.
                    workbook.Close();
                    //Dispose the Excel engine
                    excelEngine.Dispose();
                }
                catch { }

                if (dt.Rows.Count > 0)
                {
                    RemoveNullColumnFromDataTable(dt);

                    //HttpContext.Session.SetString("UploadData", JsonConvert.SerializeObject(dt));
                    //// HttpContext.ISession

                    dtResult = dt;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

                // throw ex;
            }
            return dtResult;
        }

        private static void RemoveNullColumnFromDataTable(DataTable dt)
        {
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                if (dt.Rows[i][0] == DBNull.Value && dt.Rows[i][1] == DBNull.Value && dt.Rows[i][2] == DBNull.Value && dt.Rows[i][3] == DBNull.Value)
                    dt.Rows[i].Delete();
            }
            dt.AcceptChanges();
        }

        private static DataTable ReadExcel(IWorkbook workbook)
        {
            DataTable dt = new DataTable();

            //Access first worksheet from the workbook.
            IWorksheet worksheet = workbook.Worksheets[0];
            int Cnum = 0;
            int Rnum = 0;

            IRange ShtRange = worksheet.UsedRange;
            

            // int id = ShtRange.Columns.Count();
            //string no = ShtRange[1, 1].Text;
            string[] columnNames = new string[] { };

            for (Cnum = 1; Cnum <= ShtRange.Columns.Length; Cnum++)
            {
                if ((ShtRange[1, Cnum] as IRange).Value != null)
                {
                    string aa = (ShtRange[1, Cnum] as IRange).Value.ToString(); 
                    dt.Columns.Add((ShtRange[1, Cnum] as IRange).Value.ToString());
                }
            }

            int i = 1;
            for (Rnum = 2; Rnum <= ShtRange.Rows.Length; Rnum++)
            {
                try
                {
                    i = 1;

                    DataRow dr = dt.NewRow();

                    for (Cnum = 1; Cnum <= ShtRange.Columns.Length; Cnum++)
                    {
                        try
                        {
                            if ((ShtRange[Rnum, Cnum] as IRange).Value != null)
                                dr[Cnum - i] = (ShtRange[Rnum, Cnum] as IRange).Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            //throw ex;
                            //WriteInfoLog(ex.ToString());
                        }
                    }
                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    //throw ex;

                }
            }
            return dt;
        }

        private static string SaveExcelImportFile(IFormFile file)
        {
            string filename = Path.GetFileNameWithoutExtension(file.FileName);
            string fileextension = Path.GetExtension(file.FileName);
            filename = filename + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + fileextension;
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string templatePath = Configuration.GetSection("appSettings:ExcelUploadPath").Value;

            if (!Directory.Exists(templatePath))
            {
                Directory.CreateDirectory(templatePath);

                using (FileStream fileStream = System.IO.File.Create(templatePath + filename))
                {
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
            else
            {
                using (FileStream fileStream = System.IO.File.Create(templatePath + filename))
                {

                    file.CopyTo(fileStream);
                    fileStream.Flush();

                }
            }
            string url = templatePath + filename;

            return url;
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            string colName = string.Empty;
            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    colName = GetAttributeDisplayName(pro);
                    if (colName == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        private static string GetAttributeDisplayName(PropertyInfo property)
        {
            var atts = property.GetCustomAttributes(
                typeof(DisplayNameAttribute), true);
            if (atts.Length == 0)
                return null;
            return (atts[0] as DisplayNameAttribute).DisplayName;
        }
    }
}