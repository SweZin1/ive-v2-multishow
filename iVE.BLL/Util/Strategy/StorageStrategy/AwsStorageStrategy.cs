using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using iVE.DAL.Util;
using iVE.BLL.Interface;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.IO;
using System.Threading.Tasks;
using System;
namespace iVE.BLL
{
    public class AwsStorageStrategy : IStorageStrategy
    {
        public StorageStrategy StorageStrategy => StorageStrategy.Aws;

        public async Task<dynamic> UploadFile(IFormFile file, string folderName, string subFolderName, string fileName)
        {
            return await SaveFileToS3(file, folderName, subFolderName, fileName);
        }

        public async Task<dynamic> ReadFile(string filePath, string fileName)
        {
            return await ReadObjectDataAsync(filePath, fileName);
        }

        private async Task<dynamic> SaveFileToS3(IFormFile file, string folderName, string subFolderName, string fileName)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var configuration = appsettingbuilder.Build();
            string mainUrl = configuration.GetSection("AWSConfigs:mainUrl").Value;
            string folderPath = configuration.GetSection("AWSConfigs:folderPath").Value;
            string accessKey = configuration.GetSection("AWSConfigs:accessKey").Value;
            string secretKey = configuration.GetSection("AWSConfigs:secretKey").Value;
            string bucketPath = configuration.GetSection("AWSConfigs:bucketPath").Value;
            if (!string.IsNullOrEmpty(subFolderName))
            {
                folderName = folderName + "/" + subFolderName;
            }
            bucketPath += folderName;

            var client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.APSoutheast1);

            using (var newMemoryStream = new MemoryStream())
            {
                file.CopyTo(newMemoryStream);
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = newMemoryStream,
                    Key = fileName,
                    BucketName = bucketPath,
                    CannedACL = S3CannedACL.PublicRead
                };

                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);

                return mainUrl + folderPath + folderName + "/" + fileName;
            }
        }

        private async Task<dynamic> ReadObjectDataAsync(string bucketName, string keyName)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var configuration = appsettingbuilder.Build();
            string accessKey = configuration.GetSection("AWSConfigs:accessKey").Value;
            string secretKey = configuration.GetSection("AWSConfigs:secretKey").Value;
            var client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.APSoutheast1);
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName
                };
                using (GetObjectResponse response = await client.GetObjectAsync(request))
                using (Stream responseStream = response.ResponseStream)
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    string title = response.Metadata["x-amz-meta-title"]; // Assume you have "title" as medata added to the object.
                    string contentType = response.Headers["Content-Type"];
                    Console.WriteLine("Object metadata, Title: {0}", title);
                    Console.WriteLine("Content type: {0}", contentType);

                    return reader.ReadToEnd(); // Now you process the response body.
                }
            }
            catch (AmazonS3Exception e)
            {
                // If bucket or object does not exist
                Console.WriteLine("Error encountered ***. Message:'{0}' when reading object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when reading object", e.Message);
            }
            return "";
        }

        public void WriteFile(Stream data, string filePath, string fileName)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var configuration = appsettingbuilder.Build();
            string accessKey = configuration.GetSection("AWSConfigs:accessKey").Value;
            string secretKey = configuration.GetSection("AWSConfigs:secretKey").Value;
            var client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.APSoutheast1);

            var uploadRequest = new TransferUtilityUploadRequest();
            uploadRequest.CannedACL = S3CannedACL.PublicRead;
            uploadRequest.InputStream = data;
            uploadRequest.BucketName = filePath;
            uploadRequest.Key = fileName;

            using (TransferUtility fileTransferUtility = new TransferUtility(client))
            {
                fileTransferUtility.Upload(uploadRequest);
            }
        }
    }
}