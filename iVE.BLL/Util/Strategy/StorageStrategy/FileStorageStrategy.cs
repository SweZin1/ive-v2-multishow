using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using iVE.DAL.Util;
using System.Threading.Tasks;
using iVE.BLL.Interface;
using System;
using System.IO;
namespace iVE.BLL
{
    public class FileStorageStrategy : IStorageStrategy
    {
        public StorageStrategy StorageStrategy => StorageStrategy.File;

        public async Task<dynamic> UploadFile(IFormFile file, string folderName, string subFolderName, string fileName)
        {
            return await SaveFile(file, folderName, subFolderName, fileName);
        }

        public async Task<dynamic> ReadFile(string filePath, string fileName)
        {
            return await Task.FromResult<object>(null);
        }

        public void WriteFile(Stream data, string filePath, string fileName)
        {
        }

        private async Task<dynamic> SaveFile(IFormFile file, string folderName, string subFolderName, string fileName)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var configuration = appsettingbuilder.Build();
            string returnUrl = configuration.GetSection("appSettings:SingnalUrl").Value;

            string filePath = System.IO.Directory.GetCurrentDirectory() + "\\wwwroot\\Files\\";
            if (!string.IsNullOrEmpty(subFolderName))
            {
                filePath += folderName + "\\" + subFolderName + "\\";
                returnUrl = returnUrl + "/File/" + folderName + "/" + subFolderName + "/" + fileName;
            }
            else
            {
                filePath += folderName + "\\";
                returnUrl = returnUrl + "/File/" + folderName + "/" + fileName;
            }

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
                using (FileStream fileStream = System.IO.File.Create(filePath + fileName))
                {
                    await file.CopyToAsync(fileStream);
                    fileStream.Flush();
                }
            }
            else
            {
                using (FileStream fileStream = System.IO.File.Create(filePath + fileName))
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    await file.CopyToAsync(fileStream);
                    fileStream.Flush();
                }
            }
            return returnUrl;
        }
    }
}