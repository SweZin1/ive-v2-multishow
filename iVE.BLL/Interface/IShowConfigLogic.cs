namespace iVE.BLL.Interface {
    public interface IShowConfigLogic {
        dynamic GetShowConfig ();
        dynamic CreateOrUpdateShowConfig (dynamic obj);
    }
}