namespace iVE.BLL.Interface
{
    public interface IShowLogic
    {
        dynamic GetShowBySuperAdmin(string id);
        dynamic GetShowListBySuperAdmin(dynamic obj);
        dynamic DeleteShowBySuperAdmin(string id);
        dynamic AddOrUpdateShowBySuperAdmin(dynamic obj);
        dynamic UpdateShowByOrganizer(dynamic obj);
        dynamic GetShowByOrganizer();
        void ChangeStatusForShow(dynamic obj);
        dynamic GetThemeConfigFileName(string id);
        void UpdateShowConfigUrl(string id, string url);
    }
}