namespace iVE.BLL.Interface
{
    public interface IAdminLogic
    {
        dynamic GetAdminBySuperAdmin(int id);
        dynamic GetAdminListBySuperAdmin(dynamic obj, string showId);
        dynamic DeleteAdminBySuperAdmin(int id);
        dynamic AddOrUpdateAdminBySuperAdmin(dynamic obj);
        dynamic ChangeAdminPassword(dynamic obj);
        void UpdateAdminProfile(int id, string url);
    }
}