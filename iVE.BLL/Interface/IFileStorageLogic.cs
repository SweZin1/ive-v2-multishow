using System.IO;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using iVE.DAL.Models;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using iVE.DAL.Util;
namespace iVE.BLL.Interface
{
    public interface IFileStorageLogic
    {
        Task<dynamic> UploadFile(IFormFile file, string folderName, string subFolderName, string fileName, StorageStrategy ss);
        Task<dynamic> ReadFile(string filePath, string fileName, StorageStrategy ss);
        void WriteFile(Stream data, string filePath, string fileName, StorageStrategy ss);
    }
}