namespace iVE.BLL.Interface
{
    public interface IChatConfigurationLogic
    {
        dynamic GetChatConfig(int id);
        dynamic GetChatConfigList(dynamic obj);
        dynamic DeleteChatConfig(int id);
        dynamic AddOrUpdateChatConfig(dynamic obj);
        dynamic GetChatConfigs();
    }
}