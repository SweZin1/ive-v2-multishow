namespace iVE.BLL.Interface
{
    public interface IMCQScoreBoardLogic
    {
        dynamic GetAllMCQScoreData(dynamic obj,string returnUrl);
    }
}