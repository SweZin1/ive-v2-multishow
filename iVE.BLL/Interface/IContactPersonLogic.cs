namespace iVE.BLL.Interface {
    public interface IContactPersonLogic {
        dynamic AddOrUpdateContactPerson (dynamic obj, string exhId, string contactId);
        dynamic GetAllContactPersonByExhId (string exhId);
        void DeleteContactPersonByExhId (string exhId, string contactId);
        dynamic GetContactPersonById (string exhId, string contactId);
        dynamic GetContactPersonByType (string type);

        /*8-7-2020*/
        dynamic GetAllContactPersonsWithStatusByExhId (string exhId);
        void ChangeContactPersonPassword (dynamic obj);
        bool ChangeExhibitorGroupChatStatus (string exhId, int type);
        bool CheckBadgeEntitlement (string exhId);
    }
}