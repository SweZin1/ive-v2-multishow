using System.IO;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using iVE.DAL.Models;
using System.Threading.Tasks;
namespace iVE.BLL.Interface
{
    public interface IBoothLogic
    {
        dynamic GetAllBooth(string exhId);
        dynamic GetBoothById(string exhId, string boothId);
        string CreateOrUpdateBoothById(string exhId, dynamic obj);
        dynamic GetBoothbyTemplateId(string templateId);
        void UpdateBoothFileConfig(string boothTemplateId, dynamic obj);
        void UpdateTemplateImageByExhID(dynamic obj, string exhId);
        Task<byte[]> GetBoothTemplateZipFile(string exhId);
    }
}