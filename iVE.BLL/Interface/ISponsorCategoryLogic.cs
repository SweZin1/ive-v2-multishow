namespace iVE.BLL.Interface {
    public interface ISponsorCategoryLogic {
        dynamic GetSponsorCategories ();
        dynamic UpdateOrCreateSponsorCategory(string id, dynamic obj);
        dynamic GetSponsorCategory(string id);
        dynamic DeleteSponsorCategory(string id);

    }
}