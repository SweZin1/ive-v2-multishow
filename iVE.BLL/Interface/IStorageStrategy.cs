using Microsoft.AspNetCore.Http;
using iVE.DAL.Util;
using System.Threading.Tasks;
using System.IO;
namespace iVE.BLL.Interface
{
    public interface IStorageStrategy
    {
        StorageStrategy StorageStrategy { get; }

        Task<dynamic> UploadFile(IFormFile file, string folderName, string subFolderName, string fileName);
        Task<dynamic> ReadFile(string filePath, string fileName);
        void WriteFile(Stream data, string filePath, string fileName);
    }
}