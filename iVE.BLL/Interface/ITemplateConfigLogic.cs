using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
namespace iVE.BLL.Interface
{
    public interface ITemplateConfigLogic
    {
        dynamic GetTemplate(dynamic obj);
        dynamic GetTemplateConfigList(dynamic obj);
        dynamic AddOrUpdateTemplateConfig(dynamic templateData);
        dynamic ChangeStatus(int id, string status);
        void SaveUrl(int id, string url, string mediaType);
        dynamic GetTemplateConfig(int id);
        bool DeleteTemplateConfig(int id);
    }
}