using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
namespace iVE.BLL.Interface
{
    public interface IExhibitorTypeLogic
    {
        dynamic getExhibitorTypes();
        dynamic getExhibitorTypes(dynamic obj);
        dynamic getExhibitorTypeDetails(string Id);
        dynamic deleteExhibitorType(string Id);
        dynamic createorUpdateExhibitorType(string Id, dynamic obj);

    }
}