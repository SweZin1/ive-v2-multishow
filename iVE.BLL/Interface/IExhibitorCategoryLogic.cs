using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
namespace iVE.BLL.Interface
{
    public interface IExhibitorCategoryLogic
    {
        dynamic getExhibitorCategories();
        dynamic getExhibitorCategories(dynamic obj);
        dynamic getExhibitorCategoryDetails(string Id);
        dynamic deleteExhibitorCategory(string Id);
        dynamic createorUpdateExhibitorCategory(string Id, dynamic obj);
    }
}