namespace iVE.BLL.Interface
{
    public interface IAppointmentLogic
    {
        dynamic CreateTimeSlots(dynamic obj);
        dynamic GetUserSchedule(string ownerId, string userId);
        dynamic GetUserOwnSchedule(string ownerId);
        dynamic GetUserScheduleTimeSlot(string userId, string timeSlotId);
        dynamic GetOwnerTimeSlotRequest(string ownerId, string timeSlotId);
        dynamic GetOwnerTimeSlotReceivedRequestList(string ownerId, string timeSlotId, dynamic obj);
        dynamic RequestForAppointment(dynamic obj);
        dynamic RejectForRequst(dynamic obj);
        dynamic CancelForRequst(dynamic obj);
        dynamic AcceptForRequst(dynamic obj, string tokenUser);
        dynamic CancelForAppointment(dynamic obj);
        dynamic MakeBlockForTimeSlot(dynamic obj, string tokenUser);
        dynamic CanelBlockForTimeSlot(dynamic obj, string tokenUser);
        dynamic GetTimeSlotDetail(string id);
    }
}