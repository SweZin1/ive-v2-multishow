using Microsoft.AspNetCore.Http;

namespace iVE.BLL.Interface
{
    public interface IAccountLogic
    {
        dynamic GetVisitor(dynamic obj);
        dynamic GetVisitorById(string Id);

        dynamic GetVisitedExhListByVisitor(string Id);

        dynamic CreateOrUpdateVisitor(dynamic obj, string visitorId);
        void DeleteVisitorByVisitorId(string visitorId);
        dynamic GetFavouriteExhibitorsByVisitorId(string Id);
        dynamic GetFavouriteProductsByVisitorId(string Id);
        
        dynamic GetVisitedExhibitor(dynamic obj, string userId);

        void AddUsersFavouriteByVisitor(string vId, string exhId);
        void RemoveUsersFavourite(string VId, string exhId);
        dynamic GetAllExhibitorProductsByVisitorId(dynamic obj,string exhId,string vId);
        dynamic GetFavouriteExhStatusByVisitor(string vId, string exhId);
        dynamic GetVisitorByCountry();
        dynamic GetActiveLoginUsers(dynamic obj,bool isexport);
        dynamic GetDays();
        void updateProfilePicUrl(string Id, string url);
        dynamic GetSalutation();
        dynamic DownloadExcelTemplate();
        dynamic UploadExcelFile(IFormFile file);
        dynamic SaveExcelData(dynamic obj);
        dynamic getTempAccounts(dynamic obj);
        dynamic changePassword(dynamic obj);
    }
}

