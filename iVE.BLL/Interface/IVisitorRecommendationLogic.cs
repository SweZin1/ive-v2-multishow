namespace iVE.BLL.Interface
{
    public interface IVisitorRecommendationLogic
    {
        dynamic GetAllRecommendedExhibitorsByVisitor(string vId);
        string AddVisitorRecommendation(dynamic obj, string vId);
        void UpdateVisitorRecommendation(dynamic objRecList, string vId, bool resetAll);
    }
}